const webpack = require('webpack');
const webpackMerge = require('webpack-merge');
const neutrino = require('neutrino');
const react = require('@neutrinojs/react');
const path = require('path');
const { resolvePath } = require('babel-plugin-module-resolver');
const TsConfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const getReactConfig = require('@nrwl/react/plugins/babel');
const { pick, omit } = require('lodash');

module.exports = (baseConfig, nxConfig) => {
  const prevConfig = getReactConfig(baseConfig);

  const { buildOptions: origBuildOptions, options, configuration } = nxConfig;
  const { tsConfig: tsConfigPath, main: mainPath } =
    options || origBuildOptions;

  const isDevelopmentEnvironment = configuration !== 'prod';
  process.env.NODE_ENV = isDevelopmentEnvironment
    ? 'development'
    : 'production';
  const isDevServerEnvironment = !!baseConfig.devServer;

  const appPath = tsConfigPath.replace(/tsconfig(\.app)?\.json/, '');

  const { error } = require('dotenv').config({
    // hack, guess path to app .env file
    path: path.resolve(appPath, '.env')
  });
  if (error) {
    console.error(
      'Copy .env.example to .env in app directory and adjust values to configure environment'
    );
    process.exit(1);
  }

  if (!process.env.REACT_APP_RECAPTCHA_SITE_KEY) {
    console.error(
      'Environment variable REACT_APP_RECAPTCHA_SITE_KEY is required.'
    );
    process.exit(1);
  }

  const config = neutrino({
    use: [
      neutrino =>
        neutrino.config.module
          .rule('svgr')
          .test(/\.svg$/)
          .issuer({ test: /\.[tj]sx?$/ })
          .use('@svgr/webpack')
          .loader(require.resolve('@svgr/webpack'))
          .options({ svgoConfig: { plugins: { removeViewBox: false } } })
          .end(),
      react({
        html: false,
        devtool: false,
        babel: {
          plugins: [
            '@babel/plugin-proposal-optional-chaining',
            '@babel/plugin-proposal-nullish-coalescing-operator',
            'react-hot-loader/babel',
            [
              'module-resolver',
              {
                resolvePath(sourcePath, currentFile, opts) {
                  if (!/client-panel-src/.test(currentFile)) {
                    return sourcePath;
                  }
                  return resolvePath(sourcePath, currentFile, opts);
                },
                root: './node_modules/client-panel-src',
                alias: {
                  state: './node_modules/client-panel-src/state',
                  components: './node_modules/client-panel-src/components',
                  codecs: './node_modules/client-panel-src/codecs',
                  pages: './node_modules/client-panel-src/pages',
                  permission: './node_modules/client-panel-src/permission.ts',
                  validation: './node_modules/client-panel-src/validation.ts'
                }
              }
            ]
          ],
          presets: [
            '@babel/preset-typescript',
            [
              '@emotion/babel-preset-css-prop',
              {
                autoLabel: true,
                labelFormat: '[local]'
              }
            ]
          ]
        },
        hot: isDevServerEnvironment,
        style: {
          test: /\.(css|sass|scss)$/,
          modulesTest: /\.module\.(css|sass|scss)$/,
          loaders: [
            {
              loader: 'postcss-loader',
              options: {
                plugins: [require('autoprefixer')]
              }
            },
            {
              loader: 'sass-loader',
              useId: 'sass'
            }
          ]
        }
      }),
      (/** @type {{config: import('webpack-chain')}} */ neutrino) => {
        neutrino.config.module.rule('compile').test(/\.(js|jsx|ts|tsx)$/);
        neutrino.config.module.rule('compile').include.clear();
        neutrino.config.module
          .rule('compile')
          .include.add(path.dirname(mainPath));
        neutrino.config.module
          .rule('compile')
          .include.add(/node_modules[\/\\]client-panel-src/);
        neutrino.config.module.rule('compile').include.add(/libs/);
        neutrino.config.resolve.alias.set(
          '~_vars.scss',
          path.resolve(appPath, 'src/_vars.scss')
        );
        // neutrino.config.module.rule('compile').exclude.add(/node_modules/);

        neutrino.config
          .plugin('env-process')
          .use(webpack.DefinePlugin, [{ 'process.env': {} }]);
        neutrino.config
          .plugin('env-vars')
          .use(webpack.EnvironmentPlugin, [
            [
              'NODE_ENV',
              'REACT_APP_API_URL',
              'REACT_APP_AUTH_API_URL',
              'REACT_APP_PROXY_API_URL',
              'REACT_APP_RECAPTCHA_SITE_KEY',
              'PUBLIC_URL',
              'REACT_APP_GA_KEY',
            ]
          ]);
        if (isDevelopmentEnvironment) {
          neutrino.config.resolve.alias.set(
            'react-dom',
            '@hot-loader/react-dom'
          );
        }
      }
    ]
  }).webpack();

  const finalConfig = webpackMerge(
    omit(prevConfig, ['module']),
    pick(config, ['module', 'plugins', 'resolve'])
  );
  if (finalConfig.devServer) {
    finalConfig.devServer.hot = true;
  }

  return {
    ...finalConfig,
    plugins: finalConfig.plugins.filter(plugin => {
      return !['CleanWebpackPlugin'].includes(plugin.constructor.name);
    })
  };
};
