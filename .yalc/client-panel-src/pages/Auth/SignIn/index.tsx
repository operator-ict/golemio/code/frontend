import * as React from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Formik } from 'formik';
import { useTranslation, Trans } from 'react-i18next';

import * as validation from 'validation';

import { WidgetContent, FInput, FormInputGroup, Button, withRecaptcha } from 'components';

import SimpleContainer from '../components/SimpleContainer';
import { observer } from 'mobx-react-lite';
import { useAuthStateContext } from '@dataplatform/common-state/modules';

export const SignIn = withRecaptcha<RouteComponentProps>(
  observer(({ history, trySubmit }) => {
    const [hasSubmitted, setSubmit] = React.useState(false);
    const { t } = useTranslation();

    const appState = useAuthStateContext();
    const auth = appState.auth;
    const signingIn = auth.signInState.isLoading;

    const handleLogin = (data: { email: string; password: string }) =>
      trySubmit((recaptchaResponse) => {
        auth.signIn({ ...data, 'g-recaptcha-response': recaptchaResponse }).then((data: unknown) => {
          if (data) {
            history.push('/dashboard');
          }
        });
      });

    return (
      <React.Fragment>
        <SimpleContainer>
          <WidgetContent>
            <h2>{t('page::signin::title')}</h2>
            <br />
            <Formik
              initialValues={{ email: '', password: '' }}
              onSubmit={handleLogin}
              validateOnBlur={hasSubmitted}
              validateOnChange={false}
              validationSchema={validation.createSchema({
                email: validation.email,
                password: validation.password,
              })}
            >
              {({ handleSubmit }) => (
                <form
                  onSubmit={(e) => {
                    setSubmit(true);
                    handleSubmit(e);
                  }}
                >
                  <FormInputGroup>
                    <FInput
                      label={t('email')}
                      name="email"
                      placeholder={t('form::emailPlaceholder')}
                    />
                    <FInput
                      // autoComplete="new-password"
                      label={t('password')}
                      name="password"
                      type="password"
                    />
                    <Button isLoading={signingIn} primary type="submit">
                      {t('signIn')}
                    </Button>
                  </FormInputGroup>
                </form>
              )}
            </Formik>
          </WidgetContent>
        </SimpleContainer>
        <p>
          <Trans i18nKey="page::signin::footer">
            {'Don\'t have an account yet?'}
            <Link to="/auth/sign-up">Sign in!</Link>
            {' Did you '}<Link to="/auth/password-reset">forget a password?</Link>
          </Trans>
        </p>
      </React.Fragment>
    );
  }),
);
