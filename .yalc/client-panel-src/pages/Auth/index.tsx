import * as React from 'react';
import { Switch, Route, Redirect, Link } from 'react-router-dom';

import { useTranslation } from 'react-i18next';

import { PageSignUp } from './SignUp';
import { SignIn } from './SignIn';
import { PagePasswordReset } from './PasswordReset';
import { PagePasswordResetRequest } from './PasswordResetRequest';
import NotVerified from './NotVerified';
import Verify from './Verify';
import { PageAccountConfirmation } from './AccountConfirmation';

import cls from './auth.module.scss';

import { LangSwitch } from '../components/LangSwitch';
import { observer } from 'mobx-react-lite';
import { appState } from 'state';

interface AuthRoutesProps {
  enableSignUp: boolean;
  tacTo?: string;
}

export const AuthRoutes = observer((props: AuthRoutesProps) => {
  const { enableSignUp, tacTo = '/terms' } = props;
  const { t } = useTranslation();

  const user = appState.auth.user;

  if (user && user.verified) {
    return <Redirect to="/dashboard" />;
  }

  return (
    <div className={cls.pageWrapper}>
      <h1 className={cls.appName}>{t('appName')}</h1>
      <Switch>
        {enableSignUp && <Route component={PageSignUp} path="/auth/sign-up" />}
        <Route component={Verify} path="/auth/verify" />
        <Route component={SignIn} path="/auth/sign-in" />
        <Route component={PagePasswordReset} path="/auth/password-reset/:resetToken" />
        <Route component={PagePasswordResetRequest} path="/auth/password-reset" />
        <Route
          component={PageAccountConfirmation}
          path="/auth/account-confirmation/:confirmationToken"
        />
        <Route component={NotVerified} path="/auth/not-verified" />
        <Redirect to="/auth/sign-in" />
      </Switch>
      <p>
        <Link to={tacTo}>{t('linkTerms')}</Link>
      </p>
      <LangSwitch />
    </div>
  );
});
