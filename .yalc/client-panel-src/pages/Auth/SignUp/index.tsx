import * as React from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { useTranslation, Trans } from 'react-i18next';

import { WidgetContent, Widget, FormInputGroup, Button, FInput, withRecaptcha } from 'components';

import { Formik } from 'formik';
import * as validation from 'validation';

import cls from './buildings-signup.module.scss';
import { observer } from 'mobx-react-lite';
import { useAuthStateContext } from '@dataplatform/common-state/modules';

export const PageSignUp = withRecaptcha<RouteComponentProps>(
  observer(({ history, trySubmit }) => {
    const [hasSubmitted, setSubmit] = React.useState(false);
    const { t } = useTranslation();
    const appState = useAuthStateContext();

    const signingUp = appState.auth.signUpState.isLoading;

    const handleSignUp = (data: { email: string; password: string }) => {
      trySubmit((recaptchaResponse) => {
        appState.auth.signUp({ ...data, 'g-recaptcha-response': recaptchaResponse }).then((res: unknown) => {
          if (res) {
            history.push('/auth/verify');
          }
          return res;
        });
      });
    };

    return (
      <React.Fragment>
        <Widget className={cls.widgetWrapper}>
          <div className={cls.content}>
            <WidgetContent className={cls.left}>
              <h2>{t('page::signup::title')}</h2>
              <p>{t('page::signup::asterix')}</p>
              <br />
              <Formik
                initialValues={{ email: '', password: '' }}
                onSubmit={handleSignUp}
                validateOnBlur={hasSubmitted}
                validateOnChange={false}
                validationSchema={validation.createSchema({
                  email: validation.email,
                  password: validation.password,
                })}
              >
                {({ handleSubmit }) => (
                  <form
                    onSubmit={(e) => {
                      setSubmit(true);
                      handleSubmit(e);
                    }}
                  >
                    <FormInputGroup>
                      <FInput
                        label={t('email')}
                        name="email"
                        placeholder={t('form::emailPlaceholder')}
                      />
                      <FInput
                        autoComplete="new-password"
                        label={t('password')}
                        name="password"
                        type="password"
                      />
                      <br />
                      <Button className={cls.btnAction} isLoading={signingUp} success type="submit">
                        {t('page::signup::signupButton')}
                      </Button>
                    </FormInputGroup>
                  </form>
                )}
              </Formik>
            </WidgetContent>
            <div className={cls.right}>
              <h3 className={cls.headline}>{t('page::signup::appname')}</h3>
              <p className={cls.desc}>{t('page::signup::appdesc')}</p>
            </div>
          </div>
        </Widget>
        <p>
          <Trans i18nKey="auth::wantToLogIn">
            {'Want to log in? '}
            <Link to="/auth/sign-in">Go to login page.</Link>
          </Trans>
        </p>
      </React.Fragment>
    );
  }),
);
