import * as React from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Formik } from 'formik';
import { Trans, useTranslation } from 'react-i18next';
import * as validation from 'validation';

import { WidgetContent, FormInputGroup, Button, FInput } from 'components';

import SimpleContainer from '../components/SimpleContainer';
import { observer } from 'mobx-react-lite';
import { useAuthStateContext } from '@dataplatform/common-state/modules';

export const PagePasswordReset = observer(
  ({ match, history }: RouteComponentProps<{ resetToken: string }>) => {
    const { t } = useTranslation();
    const appState = useAuthStateContext();
    const [hasReseted, setHasReseted] = React.useState(false);
    const [hasSubmitted, setSubmit] = React.useState(false);

    const isLoading = appState.auth.passwordResetState.isLoading;

    const handleClickReset = (data: { password: string }) => {
      const {
        params: { resetToken },
      } = match;
      appState.auth.passwordReset(resetToken, data.password).then((res: unknown) => {
        setHasReseted(true);

        setTimeout(() => {
          history.push('/auth/sign-in');
        }, 4000);

        return res;
      });
    };

    return (
      <React.Fragment>
        <SimpleContainer>
          {hasReseted ? (
            <WidgetContent style={{ textAlign: 'center', padding: '4rem' }}>
              <h2>{t('page::passwordReset::titleSuccess')}</h2>
              <p>{t('page::passwordReset::asterixSuccess')}</p>
            </WidgetContent>
          ) : (
            <WidgetContent>
              <h2>{t('page::passwordReset::title')}</h2>
              <br />
              <Formik
                initialValues={{ password: '' }}
                onSubmit={handleClickReset}
                validateOnBlur={hasSubmitted}
                validateOnChange={false}
                validationSchema={validation.createSchema({
                  password: validation.password,
                })}
              >
                {({ handleSubmit }) => (
                  <form
                    onSubmit={(e) => {
                      setSubmit(true);
                      handleSubmit(e);
                    }}
                  >
                    <FormInputGroup>
                      <FInput label={t('newPassword')} name="password" type="password" />
                      <Button isLoading={isLoading} primary type="submit">
                        {t('page::passwordRequest::resetButton')}
                      </Button>
                    </FormInputGroup>
                  </form>
                )}
              </Formik>
            </WidgetContent>
          )}
        </SimpleContainer>

        <p>
          <Trans i18nKey="auth::wantToLogIn">
            {'Want to log in? '}
            <Link to="/auth/sign-in">Go to login page.</Link>
          </Trans>
        </p>
      </React.Fragment>
    );
  },
);
