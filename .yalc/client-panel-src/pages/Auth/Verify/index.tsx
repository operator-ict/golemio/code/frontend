import React from 'react';
import { useTranslation } from 'react-i18next';
import { WidgetContent } from 'components';
import SimpleContainer from '../components/SimpleContainer';

const PageAccountVerify = () => {
  const { t } = useTranslation();
  return (
    <React.Fragment>
      <SimpleContainer>
        <WidgetContent style={{ textAlign: 'center', padding: '4rem' }}>
          <h2>{t('page::accountVerify::title')}</h2>
          <p>{t('page::accountVerify::asterix')}</p>
          {/* <Button>Resend confirmation</Button> */}
        </WidgetContent>
      </SimpleContainer>
    </React.Fragment>
  );
};

export default PageAccountVerify;
