import * as React from 'react';
import { Switch, Route, Redirect, BrowserRouter } from 'react-router-dom';
import config from 'config';

import { AuthRoutes } from './Auth';
import { Dashboard } from './Dashboard';
import PageTermsAndConditions from './TermsAndConditions';
import { AuthStateContext } from '@dataplatform/common-state/modules';
import { appState } from 'state';

export const AppPages = () => (
  <React.Suspense fallback={<div />}>
    <AuthStateContext.Provider value={appState}>
      <BrowserRouter basename={config.basename}>
        <Switch>
          <Route component={PageTermsAndConditions} exact path="/terms" strict />
          <Route component={Dashboard} path="/dashboard" />
          <Route component={AuthRoutes} path="/auth" />
          <Redirect to="/auth" />
        </Switch>
      </BrowserRouter>
    </AuthStateContext.Provider>
  </React.Suspense>
);
