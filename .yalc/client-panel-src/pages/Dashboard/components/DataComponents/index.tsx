import React from 'react';
import { MapComponent, MapComponentMetadataIO } from './MapComponent';
import { ContainerVertical, TopBar, TopBarSection, TopBarTitle } from 'components';
import { Typography } from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import {
  ParkingsMap,
  ParkingsMapMetadataIO,
} from 'pages/Dashboard/components/DataComponents/ParkingMap';
import {
  SortedWasteMap,
  SortedWasteMapMetadataIO,
} from 'pages/Dashboard/components/DataComponents/SortedWasteMap';
import { UnknownMetadata } from 'codecs';
import * as io from 'io-ts';
import { isRight } from 'fp-ts/lib/Either';
import {
  IframeComponent,
  IframeComponentMetadataIO,
} from 'pages/Dashboard/components/DataComponents/IframeComponent';

const components: {
  [key: string]: {
    Component: React.ComponentType<{ metadata: unknown }>;
    codec: io.Mixed;
  };
} = {
  mapComponent: {
    Component: MapComponent,
    codec: MapComponentMetadataIO,
  },
  parkingsMap: {
    Component: ParkingsMap,
    codec: ParkingsMapMetadataIO,
  },
  sortedWasteMap: {
    Component: SortedWasteMap,
    codec: SortedWasteMapMetadataIO,
  },
  iframeComponent: {
    Component: IframeComponent,
    codec: IframeComponentMetadataIO,
  },
};

const ErrorComponent = ({ errorKey }: { errorKey: string }) => {
  const { t } = useTranslation();
  return (
    <ContainerVertical>
      <TopBar>
        <TopBarSection>
          <TopBarTitle>
            <Typography color="error">{t(errorKey)}</Typography>
          </TopBarTitle>
        </TopBarSection>
      </TopBar>
    </ContainerVertical>
  );
};

interface DataComponentProps {
  metadata: UnknownMetadata;
}

export const DataComponent = (props: DataComponentProps) => {
  const { metadata } = props;
  const component = components[metadata.component];

  if (!component) {
    return <ErrorComponent errorKey="invalidComponent" />;
  }

  const { Component, codec } = component;

  if (!isRight(codec.decode(metadata))) {
    return <ErrorComponent errorKey="invalidMetadata" />;
  }

  return <Component metadata={metadata}></Component>;
};
