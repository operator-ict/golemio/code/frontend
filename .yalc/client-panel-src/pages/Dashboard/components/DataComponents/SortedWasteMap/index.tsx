import React from 'react';
import { SortedWasteTopBar } from './components/TopBar';
import { SortedWasteDetail } from './components/Detail';
import { Map } from 'components/Map';
import { observer } from 'mobx-react-lite';
import { appState } from 'state';
import { MapWrapper } from 'components/MapComponents';
import { sortedWasteIcon } from 'components/MapIcons';
import { MetadataIO } from 'codecs';
import * as io from 'io-ts';

export const SortedWasteMapMetadataIO = MetadataIO(
  io.interface({
    districtsRoute: io.string,
    measurementsRoute: io.string,
  }),
);

interface SortedWasteMapProps {
  metadata: io.TypeOf<typeof SortedWasteMapMetadataIO>;
}

export const SortedWasteMap = observer((props: SortedWasteMapProps) => {
  const { sortedWaste, districts } = appState;
  const { filteredData } = sortedWaste;
  const { metadata } = props;

  React.useEffect(() => {
    sortedWaste.fetch({ params: { route: metadata.route } });
    districts.fetch({ params: { route: metadata.data.districtsRoute } });
  }, []);

  return (
    <MapWrapper
      isError={!!sortedWaste.error}
      isLoading={sortedWaste.isLoading}
      topBar={{ custom: <SortedWasteTopBar /> }}
    >
      {filteredData && (
        <Map
          data={filteredData}
          customIcons={{
            images: [['sorted-waste', sortedWasteIcon]],
            markerIcon: 'sorted-waste',
          }}
          RenderFeatureProp={({ properties }) => {
            return (
              <SortedWasteDetail
                properties={properties}
                measurementsRoute={metadata.data.measurementsRoute}
              />
            );
          }}
        />
      )}
    </MapWrapper>
  );
});
