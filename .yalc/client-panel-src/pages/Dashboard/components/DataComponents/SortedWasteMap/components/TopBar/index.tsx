import * as React from 'react';
import { useTranslation } from 'react-i18next';
import { TopBar, TopBarSection, TopBarTitle, FilterWrapper } from 'components';

import filters from 'state/filters';
import { observer } from 'mobx-react-lite';
import { appState } from 'state';
import cls from './topbar.module.scss';

export const SortedWasteTopBar = observer(() => {
  const { t } = useTranslation();
  const data = appState.sortedWaste.data;
  const filterOptions = appState.sortedWaste.filterOptions;
  const setFilterOptions = appState.sortedWaste.setFilterOptions;
  const resetFilters = appState.sortedWaste.resetFilters;
  const filteredData = appState.sortedWaste.filteredData;

  const filterDescription = filters
    .filter((filter) => filterOptions[filter.name])
    .map((filter) => filter.getLabel(filterOptions[filter.name], t))
    .filter((filter) => filter)
    .join(', ');

  const stationsCount = (filteredData && filteredData.features.length) || 0;

  return (
    <TopBar>
      <TopBarSection>
        <TopBarTitle>{t('page::sortedWaste::title')}</TopBarTitle>
      </TopBarSection>
      <TopBarSection>
        <div className={cls.filterList}>
          <span>
            {t('page::sortedWaste::filters::shownStations', { count: stationsCount })}
            {filterDescription && filterDescription.length > 0 && ` ${filterDescription}`}.
          </span>
        </div>

        <FilterWrapper resetFilters={resetFilters}>
          {filters.map((filter) => {
            const { FilterComponent } = filter;

            return (
              <FilterComponent
                key={filter.name}
                data={data}
                filterOptions={filterOptions[filter.name]}
                name={filter.name}
                setFilterOptions={setFilterOptions}
                title={t(`page::sortedWaste::filters::${filter.name}`)}
              />
            );
          })}
        </FilterWrapper>
      </TopBarSection>
    </TopBar>
  );
});
