import React, { ComponentProps } from 'react';
import classnames from 'classnames';
import moment from 'moment';
import { useTranslation } from 'react-i18next';

import { ContainerVertical } from 'components';

import { Chart } from '../Chart';

import cls from './detail.module.scss';
import { observer } from 'mobx-react-lite';
import { appState } from 'state';

import { SortedWasteProperties, SortedWasteContainer } from 'codecs';
import { ContentWrapper } from 'components/MapComponents';
import { ExpansionPanel, ExpansionPanelSummary, ExpansionPanelDetails } from '@material-ui/core';
import { ExpandMore, ErrorRounded } from '@material-ui/icons';

interface RenderContainerProps {
  containers: SortedWasteContainer[];
  measurementsRoute: string;
}

const RenderContainers = (props: RenderContainerProps) => {
  const { containers, measurementsRoute } = props;
  const { t } = useTranslation();

  type ChartProps = ComponentProps<typeof Chart>;
  const [selectedTimeRange, setTimeRange] = React.useState<ChartProps['selectedTimeRange']>(
    undefined,
  );

  const [selectedPanel, selectPanel] = React.useState<number | null>(0);

  const handleChange = (panel: number) => (_event: React.ChangeEvent<{}>, isExpanded: boolean) => {
    selectPanel(isExpanded ? panel : null);
  };

  return (
    <React.Fragment>
      {containers.map((container, index) => {
        const trashType = container.trash_type
          ? t(`page::sortedWaste::trashType::${container.trash_type.id}`)
          : t('page::sortedWaste::trashType::unknown');

        const className = index === selectedPanel ? cls.selected : null;
        const lastMeasurement =
          container.last_measurement && moment(container.last_measurement.measured_at_utc);

        if (!container.sensor_code) {
          return (
            <div key={index}>
              <h2>{trashType}</h2>
            </div>
          );
        }

        return (
          <ExpansionPanel
            expanded={selectedPanel === index}
            onChange={handleChange(index)}
            key={index}
          >
            <ExpansionPanelSummary
              classes={{
                root: classnames(cls.containerInfo, className),
                expandIcon: cls.expandIcon,
              }}
              expandIcon={<ExpandMore />}
            >
              <div>
                <h2>{trashType}</h2>
                {container.sensor_code && (
                  <span className={cls.id}>id: {container.sensor_code}</span>
                )}
                {lastMeasurement && (
                  <p>
                    {`${t('page::map::spotDetail::lastUpdate')} ${lastMeasurement.fromNow()}`}
                    {lastMeasurement < moment().subtract(1, 'day') && (
                      <ErrorRounded color="error" fontSize="small" />
                    )}
                  </p>
                )}
              </div>
              {container.last_measurement && (
                <div className={cls.right}>
                  <h2>{container.last_measurement.percent_calculated}&nbsp;%</h2>
                </div>
              )}
            </ExpansionPanelSummary>
            {container.sensor_container_id && (
              <ExpansionPanelDetails classes={{ root: cls.expansionDetails }}>
                <ContainerVertical className={cls.selected}>
                  <Chart
                    measurementsRoute={measurementsRoute}
                    setTimeRange={setTimeRange}
                    selectedTimeRange={selectedTimeRange}
                    containerData={container}
                  />
                </ContainerVertical>
              </ExpansionPanelDetails>
            )}
          </ExpansionPanel>
        );
      })}
    </React.Fragment>
  );
};

interface DetailProps {
  properties: SortedWasteProperties;
  measurementsRoute: string;
}

export const SortedWasteDetail = observer((props: DetailProps) => {
  const { properties, measurementsRoute } = props;
  const dictionary = appState.districts.dictionary;

  if (!properties) {
    return null;
  }

  const getDistrictLabel = (key: string) => dictionary[key] || key;

  return (
    <ContentWrapper>
      <h2>
        {properties.name}
        {properties.district && ` - ${getDistrictLabel(properties.district)}`}
      </h2>
      <p>{properties.station_number}</p>
      {properties.containers && (
        <div>
          <RenderContainers
            containers={properties.containers}
            measurementsRoute={measurementsRoute}
          />
        </div>
      )}
    </ContentWrapper>
  );
});
