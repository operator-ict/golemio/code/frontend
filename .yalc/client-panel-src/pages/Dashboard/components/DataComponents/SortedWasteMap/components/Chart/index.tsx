import React, { useMemo, ChangeEvent } from 'react';
import moment from 'moment';
import {
  AreaChart,
  Area,
  XAxis,
  YAxis,
  Tooltip,
  ResponsiveContainer,
  ReferenceLine,
} from 'recharts';
import io from 'io-ts';

import { useTranslation } from 'react-i18next';

import cls from './chart.module.scss';
import { observer } from 'mobx-react-lite';
import { createSortedWasteMeasurementsQueryStateObservable } from 'state/modules';
import { SortedWasteContainer } from 'codecs';
import { SortedWasteMeasurementIO } from 'codecs/SortedWasteMeasurementIO';
import { Select, MenuItem, FormControl, CircularProgress } from '@material-ui/core';

const getPastDate = (value: moment.DurationInputArg1, unit: moment.DurationInputArg2) =>
  moment()
    .subtract(value, unit)
    .toDate();

interface Option {
  key: string;
  value: Date;
}

const options = [
  {
    key: 'day',
    value: getPastDate(1, 'days'),
  },
  {
    key: '3days',
    value: getPastDate(3, 'days'),
  },
  {
    key: '7days',
    value: getPastDate(7, 'days'),
  },
  {
    key: '2weeks',
    value: getPastDate(2, 'weeks'),
  },
  {
    key: 'month',
    value: getPastDate(1, 'months'),
  },
  {
    key: '2months',
    value: getPastDate(2, 'months'),
  },
  {
    key: '3months',
    value: getPastDate(3, 'months'),
  },
];

const getTrashColor = (trashId: number | null) => {
  switch (trashId) {
    case 1:
      return '#318a2c';
    case 2:
      return '#e20612';
    case 3:
      return '#777';
    case 4:
      return '#ee8a28';
    case 5:
      return '#1d71b9';
    case 6:
      return '#fec736';
    case 7:
      return '#fefefe';
    default:
      return '#fff';
  }
};

interface ChartProps {
  containerData: SortedWasteContainer;
  selectedTimeRange?: Option;
  setTimeRange: (option: Option) => void;
  measurementsRoute: string;
}

export const Chart = observer(
  ({
    containerData,
    selectedTimeRange = options[2],
    setTimeRange,
    measurementsRoute,
  }: ChartProps) => {
    const { t } = useTranslation();
    const sortedWasteMeasurements = useMemo(
      () => createSortedWasteMeasurementsQueryStateObservable(),
      [],
    );

    const onSelectChange = (e: ChangeEvent<{ value: unknown }>) => {
      setTimeRange(options.find((item) => item.key === e.target.value) as Option);
    };

    React.useEffect(() => {
      const dateFrom = options.reduce((min, curr) => {
        return min < curr.value ? min : curr.value;
      }, new Date());

      if (containerData && containerData.sensor_container_id) {
        sortedWasteMeasurements.fetch({
          params: {
            route: measurementsRoute,
          },
          queryParams: {
            containerId: '' + containerData.sensor_container_id,
            from: dateFrom.toISOString(),
          },
        });
      }
    }, [containerData, sortedWasteMeasurements]);

    const trashType = (containerData.trash_type && containerData.trash_type.id) || null;
    const trashColor = getTrashColor(trashType);

    interface TooltipProps {
      active: boolean;
      payload: Array<{ name: string; value: number; unit: string }>;
      label: string;
    }

    const CustomTooltip = ({ active, payload, label }: TooltipProps) => {
      if (active) {
        return (
          <div className={cls.customTooltip}>
            <h4 className={cls.title}>{label}</h4>
            {payload &&
              payload.map((item) => (
                <div key={cls.name} className={cls.item}>
                  <p className={cls.name}>{t('page::sortedWaste::fullness')}:</p>
                  <p className={cls.value}>{Math.floor(item.value)}%</p>
                </div>
              ))}
          </div>
        );
      }

      return null;
    };

    type SortedWasteMeasurement = io.TypeOf<typeof SortedWasteMeasurementIO>;
    const transformData = (measureData: SortedWasteMeasurement[]) => {
      return measureData
        .filter((item) => new Date(item.measured_at_utc) >= selectedTimeRange.value)
        .reverse()
        .map((item, _i, arr) => {
          const date = moment(item.measured_at_utc).format(arr.length < 10 ? 'D.M. h:mm' : 'D.M.');

          return {
            fullness: item.percent_calculated,
            date,
          };
        });
    };

    interface ChartProps {
      data: SortedWasteMeasurement[];
    }

    const RenderChartData = (props: ChartProps) => {
      const { data } = props;
      const gradientId = `gradient-${trashType}`;

      return (
        <ResponsiveContainer height={150} width="100%">
          <AreaChart
            data={transformData(data)}
            margin={{
              top: 5,
              right: 30,
              left: 20,
              bottom: 5,
            }}
          >
            <defs>
              <linearGradient id={gradientId} x1="0" x2="0" y1="0" y2="1">
                <stop offset="5%" stopColor={trashColor} stopOpacity={0.8} />
                <stop offset="95%" stopColor={trashColor} stopOpacity={0} />
              </linearGradient>
            </defs>
            <XAxis dataKey="date" />
            <YAxis
              domain={[0, 100]}
              orientation="right"
              padding={{
                top: 0,
                bottom: 0,
              }}
              tickLine={false}
              unit="%"
              width={15}
            />
            <Tooltip
              content={({ active, payload, label }: TooltipProps) => (
                <CustomTooltip active={active} payload={payload} label={label} />
              )}
            />
            <Area
              dataKey="fullness"
              fill={`url(#${gradientId})`}
              fillOpacity={1}
              stroke={trashColor}
              type="linear"
            />
            <ReferenceLine y={100} stroke="#fff" strokeDasharray="3 3" />
          </AreaChart>
        </ResponsiveContainer>
      );
    };

    if (sortedWasteMeasurements && sortedWasteMeasurements.isLoading) {
      return (
        <div className={cls.container}>
          <div className={cls.text}>
            <CircularProgress style={{ color: getTrashColor(trashType) }} />
          </div>
        </div>
      );
    }

    if (
      !sortedWasteMeasurements ||
      !sortedWasteMeasurements.data ||
      !sortedWasteMeasurements.data.length
    ) {
      return (
        <div className={cls.container}>
          <h3 className={cls.text}>{t('page::sortedWaste::noData')}</h3>
        </div>
      );
    }

    return (
      <div className={cls.container}>
        <div className={cls.timePickerWrapper}>
          <span className={cls.text}>{t('page::sortedWaste::fullnessPast')}</span>
          <FormControl variant="filled" classes={{ root: cls.formControl }}>
            <Select
              classes={{ root: cls.timePicker }}
              onChange={onSelectChange}
              value={selectedTimeRange.key}
              MenuProps={{ disablePortal: true }}
            >
              {options.map((option) => (
                <MenuItem key={option.key} value={option.key}>
                  {t(`page::sortedWaste::timeRange::${option.key}`)}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </div>
        {!!sortedWasteMeasurements.error && (
          <pre>{JSON.stringify(sortedWasteMeasurements.error.message, null, 2)}</pre>
        )}
        <div className={cls.wrapper}>
          {sortedWasteMeasurements &&
            !sortedWasteMeasurements.isLoading &&
            sortedWasteMeasurements.data && <RenderChartData data={sortedWasteMeasurements.data} />}
        </div>
      </div>
    );
  },
);
