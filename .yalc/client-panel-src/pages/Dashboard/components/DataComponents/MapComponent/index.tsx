import React, { useMemo } from 'react';
import { MapWrapper } from 'components/MapComponents';
import { Map } from 'components/Map';
import { appState } from 'state';
import { observer } from 'mobx-react-lite';
import * as io from 'io-ts';
import { MetadataIO } from 'codecs';

export const MapComponentMetadataIO = MetadataIO(io.unknown);

interface MapComponentProps {
  metadata: io.TypeOf<typeof MapComponentMetadataIO>;
}

export const MapComponent = observer((props: MapComponentProps) => {
  const { universalGeojsonQueryStateObservableFactory, settings } = appState;
  const { metadata } = props;

  const universalGeojson = useMemo(() => universalGeojsonQueryStateObservableFactory(), []);

  React.useEffect(() => {
    universalGeojson.fetch({ params: { route: metadata.route } });
  }, [universalGeojson]);

  return (
    <MapWrapper
      isError={!!universalGeojson.error}
      isLoading={universalGeojson.isLoading}
      topBar={{ title: metadata.description[settings.lng] }}
    >
      {universalGeojson.data && <Map data={universalGeojson.data} />}
    </MapWrapper>
  );
});
