import React, { useMemo } from 'react';
import { ParkingCollection, ParkingPropertiesIO, MetadataIO } from 'codecs';
import { ParkingDetail } from './components/ParkingDetail';
import { parkingIcon, parkingPaidIcon, parkingRideIcon } from 'components/MapIcons';
import { MapWrapper } from 'components/MapComponents';
import { Map } from 'components/Map';
import { appState } from 'state';
import { observer } from 'mobx-react-lite';
import * as io from 'io-ts';

const getParkingIcon = (parkingTypeId: number | undefined) => {
  switch (parkingTypeId) {
    case 1:
      return 'parking-ride-marker';
    case 2:
      return 'parking-paid-marker';
    default:
      return 'parking-marker';
  }
};

const transformData = (data: ParkingCollection) => {
  return {
    ...data,
    features: data.features.map((feature) => {
      return {
        ...feature,
        properties: {
          ...feature.properties,
          icon: getParkingIcon(
            feature.properties.parking_type && feature.properties.parking_type.id,
          ),
        },
      };
    }),
  };
};

export const ParkingsMapMetadataIO = MetadataIO(io.unknown);

interface ParkingsMapProps {
  metadata: io.TypeOf<typeof ParkingsMapMetadataIO>;
}

export const ParkingsMap = observer((props: ParkingsMapProps) => {
  const { metadata } = props;
  const { universalGeojsonQueryStateObservableFactory, settings } = appState;

  const universalGeojson = useMemo(
    () => universalGeojsonQueryStateObservableFactory(ParkingPropertiesIO),
    [],
  );

  React.useEffect(() => {
    universalGeojson.fetch({ params: { route: metadata.route } });
  }, [universalGeojson]);

  return (
    <MapWrapper
      isError={!!universalGeojson.error}
      isLoading={universalGeojson.isLoading}
      topBar={{ title: metadata.description[settings.lng] }}
    >
      {universalGeojson.data && (
        <Map
          data={transformData(universalGeojson.data)}
          RenderFeatureProp={({ properties }) => {
            return <ParkingDetail properties={properties} />;
          }}
          customIcons={{
            images: [
              ['parking-marker', parkingIcon],
              ['parking-ride-marker', parkingRideIcon],
              ['parking-paid-marker', parkingPaidIcon],
            ],
            markerIcon: '{icon}',
            clusterIcon: 'parking-marker',
          }}
        />
      )}
    </MapWrapper>
  );
});
