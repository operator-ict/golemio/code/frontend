import React from 'react';
import moment from 'moment';
import { useTranslation } from 'react-i18next';
import { ContentTitle } from 'components';
import { ParkingProperties } from 'codecs';

import { ContentWrapper, SectionInfoContainer, SectionInfo } from 'components/MapComponents';
import { ChartOccupancyWeekly } from '../ChartOccupancyWeekly';

interface ParkingDetailProps {
  properties: ParkingProperties;
}

export const ParkingDetail = (props: ParkingDetailProps) => {
  const { properties } = props;
  const { t } = useTranslation();
  return (
    <>
      <ContentWrapper>
        <h2>{properties.name}</h2>
        {properties.address && <p>{properties.address.address_formatted}</p>}
        <SectionInfoContainer>
          <SectionInfo>
            <ContentTitle>{t('page::parkings::spotDetail::capacity')}</ContentTitle>
            <p>{properties.total_num_of_places}</p>
          </SectionInfo>
          <SectionInfo>
            <ContentTitle>{t('page::parkings::spotDetail::occupancy')}</ContentTitle>
            <p>{properties.num_of_taken_places}</p>
          </SectionInfo>
          <SectionInfo>
            <ContentTitle>{t('page::parkings::spotDetail::freeSpots')}</ContentTitle>
            <p>{properties.num_of_free_places}</p>
          </SectionInfo>
          <SectionInfo>
            <ContentTitle>{t('page::parkings::spotDetail::lastUpdate')}</ContentTitle>
            <p>{moment(properties.last_updated).fromNow()}</p>
          </SectionInfo>
        </SectionInfoContainer>
      </ContentWrapper>
      <ChartOccupancyWeekly properties={properties} />
    </>
  );
};
