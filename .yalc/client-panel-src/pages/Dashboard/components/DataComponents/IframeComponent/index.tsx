import React from 'react';
import { appState } from 'state';
import { observer } from 'mobx-react-lite';
import * as io from 'io-ts';
import { MetadataIO } from 'codecs';
import { ContainerVertical, TopBar, TopBarSection, TopBarTitle, ScrollArea } from 'components';
import { useTranslation } from 'react-i18next';
import { Button } from '@material-ui/core';

export const IframeComponentMetadataIO = MetadataIO(
  io.interface({
    url: io.string,
  }),
);

interface IframeComponentProps {
  metadata: io.TypeOf<typeof IframeComponentMetadataIO>;
}

export const IframeComponent = observer((props: IframeComponentProps) => {
  const { settings } = appState;
  const { metadata } = props;
  const { t } = useTranslation();

  return (
    <ContainerVertical>
      <TopBar>
        <TopBarSection>
          <TopBarTitle>{metadata.description[settings.lng]}</TopBarTitle>
        </TopBarSection>
        <TopBarSection>
          <Button target="_blank" variant="contained" color="primary" href={metadata.data.url}>
            {t('openInNewWindow')}
          </Button>
        </TopBarSection>
      </TopBar>
      <div style={{ height: '100%' }}>
        <iframe
          frameBorder="0"
          allowFullScreen={true}
          src={metadata.data.url}
          style={{ height: '100%', width: '100%' }}
        ></iframe>
      </div>
    </ContainerVertical>
  );
});
