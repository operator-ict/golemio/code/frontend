import React, { Fragment, ReactNode, useState } from 'react';
import { ListItem, ListItemText, List, Collapse } from '@material-ui/core';
import { ExpandLess, ExpandMore } from '@material-ui/icons';

interface NavCategoryProps {
  title: string;
  children: ReactNode;
}

export const NavCategory = (props: NavCategoryProps) => {
  const { children, title, ...rest } = props;
  const [open, setOpen] = useState(true);

  return (
    <Fragment {...rest}>
      <ListItem button onClick={() => setOpen(!open)}>
        <ListItemText>
          <b>{title}</b>
        </ListItemText>
        {open ? <ExpandLess /> : <ExpandMore />}
      </ListItem>
      <Collapse in={open} timeout="auto">
        <List component="nav" disablePadding style={{ marginLeft: 15 }}>
          {children}
        </List>
      </Collapse>
    </Fragment>
  );
};
