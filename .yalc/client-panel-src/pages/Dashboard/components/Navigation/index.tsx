import * as React from 'react';

import { useTranslation } from 'react-i18next';
import { appState } from 'state';
import { Icon } from 'components';
import cls from './navigation.module.scss';
import { observer } from 'mobx-react-lite';
import { GroupIO } from 'codecs';
import * as io from 'io-ts';
import { List, ListItem, ListItemText, Divider, Paper } from '@material-ui/core';
import { ListItemLink } from './ListItemLink';
import { NavCategory } from './NavCategory';
import { css } from '@emotion/core';
import { COLOR_BACKGROUND_DARK } from 'apps/client-panel/client-panel/src/theme';

type Group = io.TypeOf<typeof GroupIO>;

export const Navigation = observer(() => {
  const { t } = useTranslation();

  const metadata = appState.metadata;
  const lng = appState.settings.lng;
  const { user, signOut } = appState.auth;

  React.useEffect(() => {
    metadata.fetch({ params: { userId: user.id } });
  }, []);

  const renderGroups = React.useCallback(
    (group: Group) => (
      <NavCategory title={group.title[lng]} key={group.order.toString()}>
        {group.subGroups.map(renderGroups)}
        {group.metadata.map((link) => (
          <ListItemLink key={link.client_route} to={`/dashboard${link.client_route}`}>
            {link.title[lng]}
          </ListItemLink>
        ))}
      </NavCategory>
    ),
    [lng],
  );

  return (
    <Paper
      style={{ overflowY: 'auto' }}
      css={css`
        display: flex;
        flex-direction: column;
        height: 100%;
        border-radius: 0;
        background: ${COLOR_BACKGROUND_DARK};
        color: #fff;
      `}
    >
      <List
        component="nav"
        disablePadding
        css={css`
          overflow: auto;
          flex: 1;
        `}
      >
        <ListItemLink to="/dashboard">{t('menu::Dashboard')}</ListItemLink>
        {metadata.data?.map(renderGroups)}
        <NavCategory title={t('menu::User')}>
          {user?.admin && (
            <ListItemLink to="/dashboard/admin">
              <ListItemText>{t('menu::administration')}</ListItemText>
            </ListItemLink>
          )}
          <ListItem>
            {t('menu::lang')}:{' '}
            <span className={cls.langLink} onClick={() => appState.settings.setLng('en')}>
              EN
            </span>
            {' / '}
            <span className={cls.langLink} onClick={() => appState.settings.setLng('cs')}>
              CS
            </span>
          </ListItem>
          <ListItem button component="button" onClick={signOut}>
            <Icon>input</Icon> {t('menu::signOut')}
          </ListItem>
        </NavCategory>
        <Divider />
      </List>
      <div className={cls.aboutAppContainer}>
        <div className={cls.helpdeskTitle}>{t('helpdeskTitle')}</div>
        <div className={cls.helpdeskLink}>
          <a href={t('helpdeskLinkURL')} rel="noopener noreferrer" target="_blank">
            {t('helpdeskLinkText')}
          </a>
        </div>
      </div>
      <div className={cls.aboutAppContainer}>
        <div className={cls.copyright}>&copy; Golemio</div>
        <div className={cls.copyAbout}>{t('appDesc')}</div>
      </div>
    </Paper>
  );
});
