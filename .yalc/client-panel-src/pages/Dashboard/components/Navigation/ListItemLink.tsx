import React from 'react';
import { ListItem, useTheme, makeStyles, Theme, createStyles } from '@material-ui/core';
import { ListItemProps } from '@material-ui/core/ListItem';
import { NavLink } from 'react-router-dom';
import { css } from '@emotion/core';
import { COLOR_BACKGROUND_DARK_HIGHLIGHT } from 'apps/client-panel/client-panel/src/theme';

type ListItemLinkProps = ListItemProps<
  typeof NavLink,
  { button?: true; exact?: true; strict?: true }
>;

export const ListItemLink = (props: ListItemLinkProps) => {
  const theme = useTheme();

  return (
    <ListItem
      button
      exact
      strict
      component={NavLink}
      activeClassName="active"
      css={css`
        &.active {
          background: ${COLOR_BACKGROUND_DARK_HIGHLIGHT};
        }
      `}
      {...props}
    />
  );
};
