import * as React from 'react';

import { ContainerVertical, ScrollArea, WidgetContainer } from 'components';

import { WidgetProfile } from './components/WidgetProfile';
import TopBarHome from './components/TopBarHome';

export const Home = () => {
  return (
    <ContainerVertical>
      <TopBarHome />
      <ScrollArea>
        <WidgetContainer limitWidth>
          <WidgetProfile />
        </WidgetContainer>
      </ScrollArea>
    </ContainerVertical>
  );
};
