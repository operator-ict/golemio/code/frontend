import * as React from 'react';
import { useTranslation } from 'react-i18next';

import { Widget } from 'components';
import { appState } from 'state';
import { observer } from 'mobx-react-lite';

export const WidgetProfile = observer(() => {
  const { t } = useTranslation();

  const user = appState.auth.user;

  if (!user) {
    return null;
  }

  let fullname;
  if (user.name && user.surname) {
    fullname = `${user.name} ${user.surname}`;
  } else if (user.name) {
    fullname = user.name;
  } else if (user.surname) {
    fullname = user.surname;
  }

  return (
    <Widget padding>
      <h2>{t('widget::profile::title')}</h2>
      {fullname && (
        <p>
          {t('widget::profile::name')}: {fullname}
        </p>
      )}
      <p>
        {t('widget::profile::email')}: {user.email}
      </p>
    </Widget>
  );
});
