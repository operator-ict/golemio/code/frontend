import React from 'react';
import { useTranslation } from 'react-i18next';

import { TopBar, TopBarSection, TopBarTitle } from 'components';

export default () => {
  const { t } = useTranslation();
  return (
    <TopBar>
      <TopBarSection>
        <TopBarTitle>{t('menu::Dashboard')}</TopBarTitle>
      </TopBarSection>
    </TopBar>
  );
};
