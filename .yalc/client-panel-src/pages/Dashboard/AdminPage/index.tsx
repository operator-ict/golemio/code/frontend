import React from 'react';
import { ContainerVertical, TopBar, TopBarSection, TopBarTitle } from 'components';
import { Typography } from '@material-ui/core';

export const AdminPage = () => {
  return (
    <ContainerVertical>
      <TopBar>
        <TopBarSection>
          <TopBarTitle>
            <Typography color="error">TODO</Typography>
          </TopBarTitle>
        </TopBarSection>
      </TopBar>
    </ContainerVertical>
  );
};
