import React, { useCallback, useMemo } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import * as io from 'io-ts';

import { Home } from './Home';

import { Navigation } from './components/Navigation';
import { observer } from 'mobx-react-lite';
import { appState } from 'state';
import { GroupIO, UnknownMetadata } from 'codecs';
import { AdminPage } from 'pages/Dashboard/AdminPage';
import { DataComponent } from 'pages/Dashboard/components/DataComponents';
import { AppLayout } from 'components/AppLayout';

type Group = io.TypeOf<typeof GroupIO>;

export const Dashboard = observer(() => {
  const user = appState.auth.user;
  const groups = appState.metadata.data;

  const flattenGroups = useCallback((groups: Group[]) => {
    const metadata: UnknownMetadata[] = [];
    groups?.map((group) => {
      metadata.push(...flattenGroups(group.subGroups));
      metadata.push(...group.metadata);
    });
    return metadata;
  }, []);

  const metadataArray = useMemo(() => {
    return flattenGroups(groups);
  }, [groups]);

  if (!user) {
    return <Redirect to="/auth" />;
  }

  return (
    <AppLayout
      navigation={<Navigation />}
      content={
        <Switch>
          <Route component={Home} exact path="/dashboard" strict />
          <Route component={AdminPage} exact path="/dashboard/admin" strict />
          {metadataArray.map((metadata) => (
            <Route
              key={metadata.client_route}
              component={() => <DataComponent metadata={metadata}></DataComponent>}
              exact
              strict
              path={`/dashboard${metadata.client_route}`}
            />
          ))}
          <Redirect to="/dashboard" />
        </Switch>
      }
    />
  );
});
