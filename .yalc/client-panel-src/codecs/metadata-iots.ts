import * as io from 'io-ts';

export const MetadataIO = <T extends unknown>(dataType: io.Type<T>) =>
  io.intersection([
    io.interface({
      type: io.string,
      component: io.string,
      route: io.string,
      client_route: io.string,
      group: io.string,
      title: io.interface({
        cs: io.string,
        en: io.string,
      }),
      description: io.interface({
        cs: io.string,
        en: io.string,
      }),
      data: dataType,
    }),
    io.partial({
      fa_icon: io.string,
    }),
  ]);

const UnknownMetadataIO = MetadataIO(io.unknown);

export type UnknownMetadata = io.TypeOf<typeof UnknownMetadataIO>;

interface Group {
  order: Number;
  title: {
    cs: String;
    en: String;
  };
  metadata: Array<io.TypeOf<ReturnType<typeof MetadataIO>>>;
  subGroups: Array<Group>;
}

export const GroupIO: io.Type<Group> = io.recursion('GroupIO', () =>
  io.interface({
    order: io.number,
    title: io.interface({
      cs: io.string,
      en: io.string,
    }),
    metadata: io.array(UnknownMetadataIO),
    subGroups: io.array(GroupIO),
  }),
);
