import { createQueryStateObservable, fetch } from '@dataplatform/common-state';
import { GroupIO } from 'codecs';
import * as io from 'io-ts';
import { autorun, runInAction } from 'mobx';
import { auth } from '@dataplatform/common-state/modules';
import { SyncTrunk } from 'mobx-sync';

export const metadata = createQueryStateObservable({
  dataCodec: io.array(GroupIO),
  paramsCodec: io.interface({ userId: io.string }),
  fetch: ({ params: { userId } }) =>
    fetch(`${process.env.REACT_APP_API_URL}/user/${userId}/metadata`),
});

const trunk = new SyncTrunk(metadata, {
  storageKey: 'state/modules/metadata',
  storage: localStorage,
});
trunk.init();

autorun(() => {
  if (!auth.user) {
    runInAction(() => {
      metadata.data = null;
      metadata.error = null;
    });
  }
});

autorun(() => {
  if (metadata.error) {
    auth.signOut();
  }
});
