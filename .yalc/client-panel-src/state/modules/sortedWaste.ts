import { createQueryStateObservable, fetch } from '@dataplatform/common-state';
import { SortedWasteIO, SortedWasteCollection, SortedWasteFeature } from 'codecs';
import { extendObservable, observable, action, IKeyValueMap } from 'mobx';
import filters from '../filters';
import { autorun } from 'mobx';
import { notifications, NotificationType } from '@dataplatform/common-state/modules';
import * as io from 'io-ts';

const sortedWasteQueryStateObservable = createQueryStateObservable({
  dataCodec: SortedWasteIO,
  paramsCodec: io.interface({ route: io.string }),
  fetch: ({ params: { route }, queryString }) =>
    fetch(`${process.env.REACT_APP_PROXY_API_URL}${route}${queryString}`),
});

export const sortedWaste = extendObservable(
  sortedWasteQueryStateObservable,
  {
    filterOptionsMap: observable.map<string, unknown>(),
    get filterOptions() {
      return this.filterOptionsMap.toJSON();
    },
    setFilterOptions(name: string, value: any) {
      const originalValue = this.filterOptionsMap.get(name);
      this.filterOptionsMap.set(name, { ...originalValue, ...value });
    },
    resetFilters() {
      this.filterOptionsMap.clear();
    },
    get districtList() {
      if (!sortedWasteQueryStateObservable.data) {
        return [];
      }
      return sortedWasteQueryStateObservable.data.features
        .reduce((districts, item) => {
          const { district } = item.properties;
          if (district && !districts.includes(district)) {
            districts.push(district);
          }
          return districts;
        }, [] as string[])
        .sort();
    },
    get filteredData() {
      return getFilteredData(sortedWasteQueryStateObservable.data, this.filterOptions);
    },
  },
  { setFilterOptions: action.bound, resetFilters: action.bound },
);

const getFilterFunction: (
  filterOptions: IKeyValueMap,
) => (item: SortedWasteFeature) => SortedWasteFeature = (filterOptions) => {
  const noNull = <T>(item: T | null): item is T => !!item;
  const activeFilters = filters
    .map((filter) => {
      const options = filterOptions[filter.name];
      if (!options) {
        return null;
      }

      return filter.getFilterFunction(options);
    })
    .filter(noNull);

  return (item) => {
    let filteredItem = item;

    for (const activeFilter of activeFilters) {
      filteredItem = activeFilter(filteredItem);
    }

    return filteredItem;
  };
};

const getFilteredData = (data: SortedWasteCollection | null, filterOptions: IKeyValueMap) => {
  if (!data || !data.features) {
    return data;
  }

  const filterFunction = getFilterFunction(filterOptions);

  return {
    ...data,
    features: data.features
      .map(filterFunction)
      .filter((item) => item.properties.containers && item.properties.containers.length > 0),
  };
};

autorun(() => {
  const error = sortedWaste.error;
  if (error) {
    notifications.addNotification({ title: error.message, type: NotificationType.error });
  }
});
