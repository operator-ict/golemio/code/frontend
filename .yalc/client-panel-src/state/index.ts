import { configure, observable } from 'mobx';
import { notifications, auth, settings } from '@dataplatform/common-state/modules';
import {
  districts,
  sortedWaste,
  vehiclePositions,
  metadata,
  universalGeojsonQueryStateObservableFactory,
} from 'state/modules';

configure({ enforceActions: 'always' });

export const appState = observable({
  sortedWaste,
  districts,
  settings,
  notifications,
  auth,
  vehiclePositions,
  metadata,
  universalGeojsonQueryStateObservableFactory,
});
