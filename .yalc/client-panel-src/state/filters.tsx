import React, { ChangeEvent, ComponentType } from 'react';
import { FilterSelect, FilterMultiSelect, FilterRange } from 'components';
import { useTranslation } from 'react-i18next';
import { Select, MenuItem, Tooltip } from '@material-ui/core';
import { appState } from 'state';
import { observer } from 'mobx-react-lite';
import { withStyles } from '@material-ui/styles';
import { SortedWasteFeature } from 'codecs';
import i18next from 'i18next';

const CustomTooltip = withStyles({
  tooltip: {
    fontSize: 15,
  },
})(Tooltip);

type FilterFunction = ((item: SortedWasteFeature) => SortedWasteFeature) | null;

interface Props<OptionsType> {
  key: string;
  data: unknown;
  filterOptions: OptionsType;
  name: string;
  setFilterOptions: (name: string, value: unknown) => void;
  title: string;
}

interface Filter<OptionsType> {
  name: string;
  FilterComponent: ComponentType<Props<OptionsType>>;
  getFilterFunction: (options: OptionsType) => FilterFunction;
  getLabel: (options: OptionsType, t: i18next.TFunction) => string | null;
}

interface ContainersFilterOptions {
  selected: 'sensor_only' | 'all';
}

export const containersFilter: Filter<ContainersFilterOptions> = {
  name: 'containers',
  FilterComponent: (props) => {
    const { t } = useTranslation();
    const { setFilterOptions, ...rest } = props;

    const handleFilterChange = (selected: string) => {
      if (selected === 'all') {
        setFilterOptions('fullness', { min: 0, max: 100, enabled: false });
      }

      if (selected === 'sensor_only') {
        setFilterOptions('fullness', { enabled: true });
      }

      setFilterOptions('containers', { selected });
    };

    return (
      <FilterSelect
        defaultValue="all"
        optionList={[
          {
            label: t('page::sortedWaste::filters::all'),
            value: 'all',
          },
          {
            label: t('page::sortedWaste::filters::sensorOnly'),
            value: 'sensor_only',
          },
        ]}
        {...rest}
        onChange={handleFilterChange}
      />
    );
  },
  getFilterFunction: (options) => {
    if (options.selected === 'sensor_only') {
      return (item) => {
        const containers = item.properties.containers || [];
        return {
          ...item,
          properties: {
            ...item.properties,
            containers: containers.filter((container) => container.sensor_container_id),
          },
        };
      };
    }

    return null;
  },
  getLabel: (options, t) => {
    if (options.selected === 'sensor_only') {
      return t('page::sortedWaste::filters::withSensor').toLowerCase();
    }

    return null;
  },
};

const typeFilter: Filter<{ selected: string[] }> = {
  name: 'wasteTypes',
  FilterComponent: ({ ...rest }) => {
    const { t } = useTranslation();

    return (
      <FilterMultiSelect
        defaultValues={[]}
        optionList={[
          {
            label: t('page::sortedWaste::filters::all'),
            value: 'ALL',
          },
          ...[1, 2, 3, 4, 5, 6, 7, 8].map((item) => ({
            label: t(`page::sortedWaste::trashType::${item}`),
            value: String(item),
          })),
        ]}
        {...rest}
      />
    );
  },
  getFilterFunction: (options) => {
    const { selected } = options;

    if (selected.length > 0) {
      return (item) => {
        const containers = item.properties.containers || [];

        const filteredContainers = containers.filter(
          (container) => container.trash_type && selected.includes(String(container.trash_type.id)),
        );

        return {
          ...item,
          properties: {
            ...item.properties,
            containers: filteredContainers,
          },
        };
      };
    }

    return null;
  },
  getLabel: (options, t) => {
    if (options.selected.length > 0) {
      const types = options.selected
        .map((option) => t(`page::sortedWaste::trashType::${option}`))
        .join(', ');

      return t('page::sortedWaste::filters::containing', { types }).toLowerCase();
    }

    return null;
  },
};

const fullnessFilter: Filter<{ min: number; max: number; enabled: boolean }> = {
  name: 'fullness',
  FilterComponent: ({ filterOptions, ...rest }) => {
    const { t } = useTranslation();

    interface WrapperProps {
      children: React.ReactNode;
    }

    const enabled = filterOptions ? filterOptions.enabled : false;

    const Wrapper = !enabled
      ? ({ children }: WrapperProps) => (
          <CustomTooltip title={t('page::sortedWaste::filters::forSensorOnly')}>
            <div>{children}</div>
          </CustomTooltip>
        )
      : ({ children }: WrapperProps) => <>{children}</>;

    return (
      <Wrapper>
        <FilterRange
          defaultValues={[0, 100]}
          max={100}
          min={0}
          unit="%"
          filterOptions={filterOptions}
          disabled={!enabled}
          {...rest}
        />
      </Wrapper>
    );
  },
  getFilterFunction: (options) => {
    const { min, max } = options;

    if (min > 0 || max < 100) {
      return (item) => {
        const containers =
          item.properties.containers &&
          item.properties.containers.filter((container) => {
            const lm = container.last_measurement;
            const calc = lm && lm.percent_calculated;
            return calc && calc >= min && calc <= max;
          });

        return {
          ...item,
          properties: {
            ...item.properties,
            containers,
          },
        };
      };
    }

    return null;
  },
  getLabel: (options, t) => {
    if ((options.min > 0 || options.max < 100) && options.min === options.max) {
      return `${t('page::sortedWaste::filters::percentFull', options)}`;
    }

    if (options.min > 0 && options.max < 100) {
      return `${t('page::sortedWaste::filters::between', options)}`;
    }

    if (options.min > 0) {
      return `${t('page::sortedWaste::filters::min', options)}`;
    }

    if (options.max < 100) {
      return `${t('page::sortedWaste::filters::max', options)}`;
    }

    return null;
  },
};

const areaFilter: Filter<{ value: string; label: string }> = {
  name: 'area',
  FilterComponent: observer(({ title, setFilterOptions, name, filterOptions }) => {
    const { t } = useTranslation();

    const districtList = appState.sortedWaste.districtList;
    const dictionary = appState.districts.dictionary;

    const handleChange = (e: ChangeEvent<{ value: unknown }>) => {
      const value = e.target.value as string;
      setFilterOptions(name, { value, label: dictionary[value] });
    };

    const value = (filterOptions && filterOptions.value) || 'all';

    return (
      <>
        <h3>{title}</h3>
        <Select
          MenuProps={{ className: 'ignoreClickAway' }}
          value={value}
          onChange={handleChange}
          placeholder={t('page::sortedWaste::filters::chooseDistrict')}
          style={{ width: '100%', maxWidth: '100%' }}
        >
          {
            <MenuItem key="all" value="all">
              {t('page::sortedWaste::filters::all')}
            </MenuItem>
          }
          {districtList.map((district) => {
            return (
              <MenuItem value={district} key={district}>
                {dictionary[district]}
              </MenuItem>
            );
          })}
        </Select>
      </>
    );
  }),
  getFilterFunction: (options) => {
    const { value: district } = options;
    if (district && district !== 'all') {
      return (item) => {
        const containers = district === item.properties.district ? item.properties.containers : [];

        return {
          ...item,
          properties: {
            ...item.properties,
            containers,
          },
        };
      };
    }

    return null;
  },
  getLabel: (options, t) => {
    if (!options || !options.value || options.value === 'all') {
      return null;
    }

    return `${t('page::sortedWaste::filters::inArea', { area: options.label })}`;
  },
};

export default [containersFilter, typeFilter, fullnessFilter, areaFilter];
