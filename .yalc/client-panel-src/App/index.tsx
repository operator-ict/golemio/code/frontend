import React from 'react';
import { hot } from 'react-hot-loader/root';

import { AppPages as Pages } from '../pages';
import { Lang } from '@dataplatform/common-components/components';
import { SnackContainer } from '../components/SnackContainer';

import './App.module.scss';
import { StylesProvider, ThemeProvider } from '@material-ui/styles';
import { createMuiTheme } from '@material-ui/core';
import { grey } from '@material-ui/core/colors';

const theme = createMuiTheme({
  palette: {
    primary: { main: '#212121' },
    secondary: grey,
  },
});

export const App = () => {
  return (
    <StylesProvider injectFirst>
      <ThemeProvider theme={theme}>
        <Lang>
          <SnackContainer />
          <Pages />
        </Lang>
      </ThemeProvider>
    </StylesProvider>
  );
};

export default hot(App);
