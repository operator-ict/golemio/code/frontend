import defaultB64 from './defaultIcon.png';
import defaultClusterB64 from './defaultClusterIcon.png';
import parkingB64 from './parking.png';
import parkingPaidB64 from './parkingPaid.png';
import parkingRideB64 from './parkingRide.png';
import sortedWasteB64 from './sortedWaste.png';

export const getImageFromBase64 = (src: string): HTMLImageElement => {
  const img = new Image();
  img.src = src;
  return img;
};

export const defaultIcon = getImageFromBase64(defaultB64);
export const defaultClusterIcon = getImageFromBase64(defaultClusterB64);
export const parkingIcon = getImageFromBase64(parkingB64);
export const parkingPaidIcon = getImageFromBase64(parkingPaidB64);
export const parkingRideIcon = getImageFromBase64(parkingRideB64);
export const sortedWasteIcon = getImageFromBase64(sortedWasteB64);
