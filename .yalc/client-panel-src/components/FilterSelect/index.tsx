import React, { ReactElement } from 'react';
import { Radio, FormControlLabel } from '@material-ui/core';
import ContainerVertical from '../ContainerVertical';
import cls from './filterSelect.module.scss';

interface Option {
  value: string;
  label: string;
}

interface FilterSelectProps {
  onChange: (selected: string) => void;
  optionList: Option[];
  name: string;
  defaultValue: string;
  filterOptions?: { selected: string };
  title: string;
}

const FilterSelect: (props: FilterSelectProps) => ReactElement = (props) => {
  const { onChange, optionList, name, defaultValue, filterOptions, title } = props;
  const handleChange = (e: React.ChangeEvent<HTMLInputElement>, checked: boolean) => {
    if (checked) {
      onChange(e.target.value);
    }
  };

  const selectedOption = (filterOptions && filterOptions.selected) || defaultValue;

  return (
    <ContainerVertical>
      <h3>{title}</h3>
      {optionList.map((option) => {
        const id = `id-${name}-${option.value}`;

        return (
          <FormControlLabel
            key={id}
            classes={{ root: cls.label }}
            control={
              <Radio
                classes={{ root: cls.radio }}
                key={id}
                checked={selectedOption === option.value}
                onChange={handleChange}
                value={option.value}
                color="secondary"
              />
            }
            label={option.label}
          />
        );
      })}
    </ContainerVertical>
  );
};

export default FilterSelect;
