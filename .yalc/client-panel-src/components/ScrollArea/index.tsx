import React, { ReactNode } from 'react';
import classnames from 'classnames';

import cls from './scroll.module.scss';

interface ScrollAreaProps {
  children?: ReactNode;
  className?: string;
  [prop: string]: unknown;
}

const ScrollArea = (props: ScrollAreaProps) => {
  const { className = null, children = null, ...rest } = props;

  return (
    <div className={classnames(cls.wrapper, className)} {...rest}>
      {children}
    </div>
  );
};

export default ScrollArea;
