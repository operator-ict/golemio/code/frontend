import React, { FC, useMemo, useCallback } from 'react';
import * as io from 'io-ts';
import { FeatureCollectionIO, PropertiesIO } from 'codecs';
import ReactMapboxGl, { Source, Layer, ZoomControl } from 'react-mapbox-gl';
import { defaultIcon, defaultClusterIcon } from 'components/MapIcons';
import { DetailWrapper, RenderJson, MapImagesType, ClusterLayers } from 'components/MapComponents';
import { MapLayerMouseEvent } from 'mapbox-gl';

const DEFAULT_CENTER: [number, number] = [14.4378, 50.0755];
const DEFAULT_ZOOM: [number] = [10];
const MAX_BOUNDS = [
  [13.38938, 49.46719],
  [15.662555, 50.57593],
];

const Mapbox = ReactMapboxGl({
  accessToken:
    'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4M29iazA2Z2gycXA4N2pmbDZmangifQ.-g_vE53SD2WrJ6tFX7QHmA',
  minZoom: 9,
  dragRotate: false,
});

const DataIO = FeatureCollectionIO(PropertiesIO);
type FeatureCollection = io.TypeOf<typeof DataIO>;

interface RenderFeatureProps<FeatureProperties> {
  properties: FeatureProperties;
}

interface CustomIcons {
  images: MapImagesType;
  markerIcon: string;
  clusterIcon?: string;
}

interface MapProps<Data extends FeatureCollection> {
  data: Data;
  RenderFeatureProp?: FC<RenderFeatureProps<Data['features'][0]['properties']>>;
  mapOptions?: Partial<React.ComponentProps<typeof Mapbox>>;
  children?: React.ReactNode;
  customIcons?: CustomIcons;
}

const defaultImages: MapImagesType = [
  ['default-icon', defaultIcon],
  ['default-cluster-icon', defaultClusterIcon],
];

export const Map = <Data extends FeatureCollection>(props: MapProps<Data>) => {
  const { data, mapOptions, children, customIcons, RenderFeatureProp } = props;
  const { images, markerIcon, clusterIcon } = customIcons || {
    images: defaultImages,
    markerIcon: 'default-icon',
    clusterIcon: 'default-cluster-icon',
  };

  const RenderFeature = useMemo(() => RenderFeatureProp || RenderJson, [RenderFeatureProp]);

  const [selectedFeature, selectFeature] = React.useState<
    RenderFeatureProps<Data['features'][0]['properties']> | null | undefined
  >(null);

  const geoJsonSource = useMemo(
    () => ({
      type: 'geojson',
      data: {
        ...data,
        features: data.features.map((item, featureIndex) => {
          // set feature index to recognize which object marker was clicked on
          return { ...item, properties: { ...item.properties, featureIndex } };
        }),
      },
      cluster: true,
      clusterMaxZoom: 18,
      clusterRadius: 50,
    }),
    [data],
  );

  const onMouseEnter = useCallback((e: MapLayerMouseEvent) => {
    e.target.getCanvas().style.cursor = 'pointer';
  }, []);

  const onMouseLeave = useCallback((e: MapLayerMouseEvent) => {
    e.target.getCanvas().style.cursor = '';
  }, []);

  const onFeatureClick = useCallback(
    (e: MapLayerMouseEvent) => {
      // stop clickAwayListener from closing detail instantly
      e.originalEvent.preventDefault();

      const feature = e.features && e.features[0];
      const index = feature ? feature.properties && feature.properties.featureIndex : null;
      if (index !== null || index !== undefined) {
        selectFeature(data.features[index]);
      }
    },
    [data],
  );

  return (
    <>
      <Mapbox
        // eslint-disable-next-line
        style="mapbox://styles/mapbox/light-v8"
        containerStyle={{ width: '100%', height: '100%' }}
        center={DEFAULT_CENTER}
        zoom={DEFAULT_ZOOM}
        maxBounds={MAX_BOUNDS}
        {...mapOptions}
      >
        <ZoomControl />
        <Source id="data" geoJsonSource={geoJsonSource} />
        <Layer
          sourceId="data"
          type="symbol"
          filter={['!', ['has', 'point_count']]}
          images={images}
          layout={{
            'icon-image': markerIcon,
            'text-offset': [0, 2],
            'text-field': '{name}',
            'text-letter-spacing': 0.05,
            'text-font': ['Roboto Regular', 'Arial Unicode MS Bold'],
            'text-size': 12,
            'text-anchor': 'top',
            'icon-allow-overlap': true,
            'text-allow-overlap': true,
          }}
          onMouseEnter={onMouseEnter}
          onMouseLeave={onMouseLeave}
          onClick={onFeatureClick}
        />
        <ClusterLayers images={images} image={clusterIcon || markerIcon} sourceId="data" />
        <>{children}</>
      </Mapbox>
      {selectedFeature && RenderFeature && (
        <DetailWrapper hideDetail={() => selectFeature(null)}>
          <RenderFeature properties={selectedFeature.properties} />
        </DetailWrapper>
      )}
    </>
  );
};
