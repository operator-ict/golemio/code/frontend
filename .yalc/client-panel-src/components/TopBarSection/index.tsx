import React from 'react';
import classnames from 'classnames';

import cls from './top-bar-section.module.scss';

interface TopBarSectionProps {
  children?: React.ReactNode;
  className?: string;
  Component?: React.ReactType;
  [prop: string]: unknown;
}

const TopBarSection = (props: TopBarSectionProps) => {
  const { children = null, className = null, Component = 'div', ...rest } = props;

  return (
    <Component className={classnames(cls.wrapper, className)} {...rest}>
      {children}
    </Component>
  );
};

export default TopBarSection;
