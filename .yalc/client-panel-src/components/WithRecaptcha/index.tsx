import React, { useState, useCallback, ComponentType } from 'react';
import ReCAPTCHA from 'react-google-recaptcha';

const sitekey = process.env.REACT_APP_RECAPTCHA_SITE_KEY;
if (!sitekey) {
  console.error('Recaptcha site key not set');
}

type OnSuccess = (response: string) => void;

interface ComponentProps {
  trySubmit: (onSuccess: OnSuccess) => void;
}

type RecaptchaResponse = string | null;

const withRecaptcha = <T extends {}>(Component: ComponentType<ComponentProps & T>) => {
  return (props: T) => {
    const recaptchaRef = React.useRef<ReCAPTCHA>(null);
    const [onSuccess, setOnSuccess] = useState<OnSuccess | null>();

    const trySubmit = (onSuccess: OnSuccess) => {
      if (recaptchaRef.current) {
        setOnSuccess(() => onSuccess);
        recaptchaRef.current.execute();
      }
    };

    const onChange = useCallback(
      (res: RecaptchaResponse) => {
        if (onSuccess && res) {
          onSuccess(res);

          if (recaptchaRef.current) {
            recaptchaRef.current.reset();
          }
        }
      },
      [onSuccess],
    );

    return (
      <>
        {sitekey && (
          <ReCAPTCHA ref={recaptchaRef} size="invisible" sitekey={sitekey} onChange={onChange} />
        )}
        <Component trySubmit={trySubmit} {...props} />
      </>
    );
  };
};

export default withRecaptcha;
