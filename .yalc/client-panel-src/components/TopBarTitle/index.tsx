import React, { ReactNode, ElementType } from 'react';
import classnames from 'classnames';
import cls from './top-bar-title.module.scss';

interface TopBarTitleProps {
  children?: ReactNode;
  className?: string;
  Component?: ElementType;
  large?: boolean;
  [prop: string]: unknown;
}

const TopBarTitle = (props: TopBarTitleProps) => {
  const { children = null, className = null, Component = 'h2', large = false, ...rest } = props;

  return (
    <Component
      className={classnames(cls.wrapper, className, {
        [cls.large]: large,
      })}
      {...rest}
    >
      {children}
    </Component>
  );
};

export default TopBarTitle;
