import React, { useContext, useCallback } from 'react';
import classnames from 'classnames';

import cls from './top-bar.module.scss';
import { makeStyles, AppBar, Toolbar, Typography, IconButton, Divider } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import { AppLayoutContext } from 'components/AppLayout';
import { observer } from 'mobx-react-lite';
import { css } from '@emotion/core';

interface TopBarProps {
  center?: boolean;
  className?: string;
  children?: React.ReactNode;
  [prop: string]: unknown;
}

const TopBar = observer((props: TopBarProps) => {
  const { children = null, className = null, center = false, ...rest } = props;
  const classes = useStyles({});

  const appLayputContext = useContext(AppLayoutContext);

  const handleDrawerToggle = useCallback(() => {
    appLayputContext.setMobileOpen(!appLayputContext.mobileOpen);
  }, [appLayputContext]);

  return (
    <div
      css={css`
        border-bottom: 1px solid rgba(0, 0, 0, 0.12);
		height: 65px;
      `}
      className={classnames(cls.wrapper, className, {
        [cls.center]: center,
      })}
      {...rest}
    >
      <IconButton
        color="inherit"
        aria-label="open drawer"
        edge="start"
        onClick={handleDrawerToggle}
        className={classes.menuButton}
      >
        <MenuIcon />
      </IconButton>
      <div
        css={css`
          display: flex;
          align-items: center;
          justify-content: space-between;
          flex: 1;
        `}
      >
        {children}
      </div>
    </div>
  );
});

export default TopBar;

const useStyles = makeStyles((theme) => ({
  menuButton: {
    [theme.breakpoints.down('xs')]: {
      marginLeft: theme.spacing(2),
    },
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
}));
