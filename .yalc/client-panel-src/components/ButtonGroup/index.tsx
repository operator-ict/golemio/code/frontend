import classnames from 'classnames';
import React, { ElementType, ReactNode } from 'react';
import cls from './btn-group.module.scss';

interface ButtonGroupProps {
  className?: string;
  Component?: ElementType;
  children?: ReactNode;
  [prop: string]: unknown;
}

const ButtonGroup = (props: ButtonGroupProps) => {
  const { className = null, Component = 'div', children = null, ...rest } = props;

  return (
    <Component className={classnames(cls.wrapper, className)} {...rest}>
      {children}
    </Component>
  );
};

export default ButtonGroup;
