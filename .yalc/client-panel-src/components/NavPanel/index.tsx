import React, { ReactNode } from 'react';
import classnames from 'classnames';
import cls from './nav-panel.module.scss';

interface NavPanelProps {
  children?: ReactNode;
  className?: string;
  positionRight?: boolean;
  [prop: string]: unknown;
}

const NavPanel = (props: NavPanelProps) => {
  const { className = null, positionRight = false, children = null, ...rest } = props;

  return (
    <div
      className={classnames(cls.wrapper, className, {
        [cls.positionRight]: positionRight,
      })}
      {...rest}
    >
      {children}
    </div>
  );
};

export default NavPanel;
