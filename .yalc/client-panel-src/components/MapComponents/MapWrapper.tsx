import React, { ReactNode } from 'react';
import {
  ContainerVertical,
  ScrollArea,
  ContentLoader,
  TopBar,
  TopBarSection,
  TopBarTitle,
} from 'components';
import { useTranslation } from 'react-i18next';
import cls from './detail.module.scss';

interface TopBarTitle {
  title: string;
}

interface TopBarNode {
  custom: React.ReactNode;
}

interface WrapperProps {
  isLoading: boolean;
  isError: boolean;
  children: ReactNode;
  topBar: TopBarTitle | TopBarNode;
}

export const MapWrapper = (props: WrapperProps) => {
  const { isLoading, children, topBar, isError } = props;
  const { t } = useTranslation();
  return (
    <ContainerVertical style={{ position: 'relative' }}>
      {(topBar as TopBarNode).custom || (
        <TopBar>
          <TopBarSection>
            <TopBarTitle>{(topBar as TopBarTitle).title}</TopBarTitle>
          </TopBarSection>
        </TopBar>
      )}
      {isLoading && <ContentLoader>{t('page::global::loadingText')}</ContentLoader>}
      {!isLoading && isError && <div className={cls.error}>{t('page::global::loadingError')}</div>}
      {!isLoading && !isError && <ScrollArea>{children}</ScrollArea>}
    </ContainerVertical>
  );
};
