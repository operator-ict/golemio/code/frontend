import React, { ReactNode, ElementType } from 'react';
import classnames from 'classnames';
import cls from './container-v.module.scss';

interface ContainerVerticalProps {
  className?: string;
  children?: ReactNode;
  Component?: ElementType;
  [prop: string]: unknown;
}

const ContainerVertical = (props: ContainerVerticalProps) => {
  const { className = null, Component = 'div', children = null, ...rest } = props;

  return (
    <Component className={classnames(cls.container, className)} {...rest}>
      {children}
    </Component>
  );
};

export default ContainerVertical;
