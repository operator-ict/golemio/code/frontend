import React, { ChangeEvent } from 'react';
import ContainerVertical from '../ContainerVertical';
import cls from './multiselect.module.scss';
import { FormControlLabel, Checkbox, FormGroup, Radio } from '@material-ui/core';

interface FilterMultiSelectProps {
  setFilterOptions: (name: string, options: { selected: string[] }) => void;
  optionList: Array<{ value: string; label: string; count?: number }>;
  name: string;
  defaultValues: string[];
  filterOptions?: { selected: string[] };
  title: string;
}

const FilterMultiSelect = (props: FilterMultiSelectProps) => {
  const { setFilterOptions, optionList, name, defaultValues, filterOptions, title } = props;
  const selectedOptions: string[] = (filterOptions && filterOptions.selected) || defaultValues;

  const handleChange = (e: ChangeEvent<HTMLInputElement>, checked: boolean) => {
    if (checked) {
      const selected = e.target.value === 'ALL' ? [] : [...selectedOptions, e.target.value];
      setFilterOptions(name, { selected });
    } else {
      const selected = selectedOptions.filter((item) => item !== e.target.value);
      setFilterOptions(name, { selected });
    }
  };

  return (
    <ContainerVertical>
      <h3>{title}</h3>
      {optionList.map((option) => {
        const id = `id-${name}-${option.value}`;
        const label =
          option.count !== undefined ? (
            <>
              {option.label}
              &nbsp;(
              <span className={cls.count}>{option.count}</span>)
            </>
          ) : (
            option.label
          );

        const InputElement = option.value === 'ALL' ? Radio : Checkbox;

        return (
          <FormGroup row key={id}>
            <FormControlLabel
              classes={{ root: cls.label }}
              control={
                <InputElement
                  classes={{ root: cls.input }}
                  checked={
                    (option.value === 'ALL' && selectedOptions.length === 0) ||
                    selectedOptions.includes(option.value)
                  }
                  onChange={handleChange}
                  value={option.value}
                  color="secondary"
                  disabled={option.count === 0}
                />
              }
              label={label}
            />
          </FormGroup>
        );
      })}
    </ContainerVertical>
  );
};

export default FilterMultiSelect;
