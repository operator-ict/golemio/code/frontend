import React, { ChangeEvent, useCallback, useMemo } from 'react';
import ContainerVertical from '../ContainerVertical';
import cls from './range.module.scss';
import { Slider } from '@material-ui/core';

interface FilterRangeProps {
  setFilterOptions: (name: string, options: { min: number; max: number }) => void;
  name: string;
  defaultValues: number[];
  filterOptions: { min: number; max: number };
  title: string;
  min: number;
  max: number;
  unit: string;
  disabled: boolean;
}

const FilterRange = (props: FilterRangeProps) => {
  const {
    setFilterOptions,
    name,
    defaultValues,
    filterOptions,
    title,
    min,
    max,
    unit,
    disabled,
  } = props;

  const handleChange = useCallback(
    (_e: ChangeEvent<{}>, newValue: number | number[]) => {
      if (Array.isArray(newValue)) {
        setFilterOptions(name, { min: newValue[0], max: newValue[1] });
      } else {
        setFilterOptions(name, { min: newValue, max: newValue });
      }
    },
    [name, setFilterOptions],
  );

  const selectedValues = useMemo(
    () =>
      filterOptions && filterOptions.min !== undefined && filterOptions.max !== undefined
        ? [filterOptions.min, filterOptions.max]
        : defaultValues,
    [filterOptions, defaultValues],
  );

  const labelFormat = useCallback((value: number) => `${value}${unit}`, [unit]);
  const marks = useMemo(
    () => [
      { value: min, label: `${min}${unit}` },
      { value: max, label: `${max}${unit}` },
    ],
    [min, max, unit],
  );

  return (
    <ContainerVertical>
      <h3>{title}</h3>
      <div className={cls.sliderWrapper}>
        <Slider
          defaultValue={selectedValues}
          disabled={disabled}
          max={max}
          min={min}
          marks={marks}
          onChangeCommitted={handleChange}
          valueLabelDisplay="auto"
          valueLabelFormat={labelFormat}
        />
      </div>
    </ContainerVertical>
  );
};

export default FilterRange;
