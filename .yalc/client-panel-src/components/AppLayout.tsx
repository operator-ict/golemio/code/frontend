import React, { ReactNode, createContext, useContext, useCallback } from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import {
  Divider,
  Drawer,
  Hidden,
  AppBar,
  Typography,
  Toolbar,
  CssBaseline,
  IconButton,
} from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import { css } from '@emotion/core';
import { observable, action } from 'mobx';
import { observer } from 'mobx-react-lite';
import { COLOR_BACKGROUND_DARK } from 'apps/client-panel/client-panel/src/theme';

interface AppLayoutProps {
  navigation: ReactNode;
  content: ReactNode;
}
export const AppLayout = observer((props: AppLayoutProps) => {
  const { t } = useTranslation();
  const { navigation, content } = props;
  const classes = useStyles({});
  const theme = useTheme();

  const appLayputContext = useContext(AppLayoutContext);

  const handleDrawerToggle = useCallback(() => {
    appLayputContext.setMobileOpen(!appLayputContext.mobileOpen);
  }, [appLayputContext]);

  const drawer = (
    <div
      css={css`
        display: flex;
        flex-direction: column;
        height: 100%;
        background: ${COLOR_BACKGROUND_DARK};
      `}
    >
      <div
        className={classes.toolbar}
        css={css`
          flex: 1;
          flex-grow: 0;
          flex-shrink: 0;
        `}
      >
        <Toolbar>
          <Typography
            variant="h6"
            noWrap
            css={css`
              color: #fff;
            `}
          >
            {t('appName')}
          </Typography>
        </Toolbar>
      </div>
      <Divider />
      <div
        css={css`
          flex: 1;
          overflow: auto;
          color: #fff;
        `}
      >
        {navigation}
      </div>
    </div>
  );
  return (
    <div className={classes.root}>
      <nav className={classes.drawer}>
        <Hidden smUp implementation="css">
          <Drawer
            container={document.getElementById('root')}
            variant="temporary"
            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
            open={appLayputContext.mobileOpen}
            onClose={handleDrawerToggle}
            ModalProps={{
              keepMounted: true,
            }}
            classes={{
              paper: classes.drawerPaper,
            }}
            css={css`
              width: ${drawerWidth}px;
              height: 100%;
            `}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            css={css`
              width: ${drawerWidth}px;
              height: 100%;
            `}
            variant="permanent"
            open
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
      <main className={classes.content}>{content}</main>
    </div>
  );
});

const drawerWidth = 340;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    minHeight: '100%',
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: `${drawerWidth}px`,
      flexShrink: 0,
    },
  },
  appBar: {
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: `${drawerWidth}px`,
    },
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: `${drawerWidth}px`,
  },
  content: {
    flexGrow: 1,
  },
}));

export const AppLayoutContext = createContext(
  observable(
    {
      mobileOpen: false,
      setMobileOpen(mobileOpen: boolean) {
        this.mobileOpen = mobileOpen;
      },
    },
    {
      setMobileOpen: action.bound,
    },
  ),
);
