import React, { ElementType, ReactNode } from 'react';
import classnames from 'classnames';
import cls from './nav-link.module.scss';

interface NavLinkProps {
  className?: string;
  children?: ReactNode;
  rightEl?: ReactNode;
  highlighted?: boolean;
  icon?: ReactNode;
  Component?: ElementType;
  [prop: string]: unknown;
}

const NavLink = (props: NavLinkProps) => {
  const {
    className = null,
    rightEl = null,
    highlighted = false,
    icon = null,
    children = null,
    Component = 'a',
    ...rest
  } = props;

  return (
    <Component
      className={classnames(cls.wrapper, className, {
        [cls.highlighted]: highlighted,
      })}
      {...rest}
    >
      <span className={cls.text}>
        {icon && <span className={cls.icon}>{icon}</span>}
        {children}
      </span>
      {rightEl && <span className={cls.rightEl}>{rightEl}</span>}
    </Component>
  );
};

export default NavLink;
