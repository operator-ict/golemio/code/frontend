import React, { ReactNode, ElementType } from 'react';
import classnames from 'classnames';
import cls from './btn.module.scss';
import LoaderDots from '../LoaderDots';

interface ButtonProps {
  children?: ReactNode;
  Component?: ElementType;
  className?: string;
  contentClassName?: string;
  href?: string;
  type?: string;

  // appearence

  xs?: boolean;
  lg?: boolean;

  primary?: boolean;
  error?: boolean;
  success?: boolean;
  transparent?: boolean;

  clear?: boolean;
  isLoading?: boolean;
  // icon properties
  icon?: ReactNode;
  iconRight?: boolean;
  iconOnly?: boolean;
  noBorder?: boolean;
  dark?: boolean;
  isExpanded?: boolean;
  [prop: string]: unknown;
}

const Button = (props: ButtonProps) => {
  const {
    Component = 'button',
    primary = false,
    error = false,
    success = false,
    dark = false,
    transparent = false,
    children = null,
    className = null,
    contentClassName = null,
    xs = false,
    lg = false,
    clear = false,
    isLoading = false,
    type = 'button',
    icon = null,
    iconRight = false,
    iconOnly = false,
    noBorder = false,
    isExpanded = false,
    ...rest
  } = props;

  const classes = classnames(
    cls.base,
    {
      [cls.primary]: primary,
      [cls.error]: error,
      [cls.success]: success,
      [cls.dark]: dark,
      [cls.xs]: xs,
      [cls.lg]: lg,
      [cls.clear]: clear,
      [cls.isLoading]: isLoading,
      [cls.hasIcon]: icon,
      [cls.iconRight]: iconRight,
      [cls.iconOnly]: iconOnly,
      [cls.transparent]: transparent,
      [cls.noBorder]: noBorder,
      [cls.isExpanded]: isExpanded,
    },
    className,
  );

  // put props together so we don't have to repeat it
  const btnProps = {
    className: classes,
    ...rest,
  };

  return (
    <Component {...btnProps} type={Component === 'button' ? type : null}>
      {icon && <span className={cls.iconWrapper}>{icon}</span>}
      {isLoading && <LoaderDots className={cls.loader} />}
      <span className={classnames(cls.content, contentClassName)}>{children}</span>
    </Component>
  );
};

export default Button;
