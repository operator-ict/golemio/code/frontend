import React, { ReactNode } from 'react';
import classnames from 'classnames';
import cls from './nav-section-title.module.scss';

interface NavSectionTitleProps {
  children?: ReactNode;
  className?: string;
  [prop: string]: unknown;
}

const SectionName = (props: NavSectionTitleProps) => {
  const { className = null, children = null, ...rest } = props;

  return (
    <span className={classnames(cls.wrapper, className)} {...rest}>
      {children}
    </span>
  );
};

export default SectionName;
