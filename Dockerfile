FROM bitnami/node:16.13.0-prod
ARG PROJECT_NAME_FOLDER=dist
WORKDIR /app
RUN yarn global add serve

COPY ./$PROJECT_NAME_FOLDER/ .

# Create a non-root user
RUN useradd -r -u 1001 -g root nonroot && \
    chown -R nonroot /app
USER nonroot

EXPOSE 3000
CMD ["serve","-s", "-l", "3000"]
