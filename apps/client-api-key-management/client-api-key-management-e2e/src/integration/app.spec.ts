import { getGreeting } from '../support/app.po';

describe('client-api-key-management-client-api-key-management', () => {
  beforeEach(() => cy.visit('/'));

  it('should display welcome message', () => {
    getGreeting().contains(
      'Welcome to client-api-key-management-client-api-key-management!'
    );
  });
});
