import React from 'react';
import { hot } from 'react-hot-loader/root';
import config from 'config';

import { Lang, SnackContainer, TypedRoute } from '@dataplatform/common-components/components';

import { Switch, Redirect, BrowserRouter } from 'react-router-dom';

import { AuthRoutes } from 'client-panel-src/pages/Auth';
import { AuthStateContext } from '@dataplatform/common-state/modules';

import { HomePage } from 'pages/HomePage';
import { TermsAndConditionsPage } from 'pages/TermsAndConditionsPage';

import { appState } from 'state';
import { ROUTE_TAC, ROUTE_DASHBOARD, ROUTE_AUTH } from 'routes';

import CssBaseline from '@material-ui/core/CssBaseline';
import { ThemeProvider } from '@material-ui/styles';
import { theme } from '@dataplatform/common-components/styles/theme';
import { NotificationsStateProvider } from '@dataplatform/common-components/components/NotificationsStateProvider';

export const App = hot(() => {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Lang>
        <NotificationsStateProvider state={appState.notifications}>
          <SnackContainer />
          <AuthStateContext.Provider value={appState}>
            <BrowserRouter basename={config.basename}>
              <Switch>
                <TypedRoute
                  path={ROUTE_AUTH.template()}
                  route={ROUTE_AUTH}
                  render={() => <AuthRoutes enableSignUp={true} tacTo={ROUTE_TAC.create({})} />}
                />
                <TypedRoute
                  path={ROUTE_DASHBOARD.template()}
                  route={ROUTE_DASHBOARD}
                  render={() => <HomePage />}
                />
                <TypedRoute
                  path={ROUTE_TAC.template()}
                  route={ROUTE_TAC}
                  render={() => <TermsAndConditionsPage />}
                />
                <Redirect to={ROUTE_DASHBOARD.template()} />
              </Switch>
            </BrowserRouter>
          </AuthStateContext.Provider>
        </NotificationsStateProvider>
      </Lang>
    </ThemeProvider>
  );
});
