import * as io from 'io-ts';
import { createQueryStateObservable } from '@dataplatform/common-state';
// import { extendObservable, computed } from 'mobx';
import { fetch } from '@dataplatform/common-state';
import { ApiKeysResponseIO } from 'codecs/ApiKeysResponseIO';

const apiKeysQueryStateObservable = createQueryStateObservable({
  dataCodec: ApiKeysResponseIO,
  transform: (a) => a,
  fetch: () => fetch(`${process.env.REACT_APP_AUTH_API_URL}/api-keys`),
});

// export const apiKeys = extendObservable(apiKeysQueryStateObservable, {}, {});
export const apiKeys = apiKeysQueryStateObservable;

export const deleteApiKeyQueryStateObservableFactory = () =>
  createQueryStateObservable({
    paramsCodec: io.interface({ id: io.union([io.string, io.number]) }),
    fetch: ({ params }) => {
      if (params) {
        const { id } = params;
        return fetch(`${process.env.REACT_APP_AUTH_API_URL}/api-key/${id}`, {
          method: 'delete',
        });
      }
      throw new Error();
    },
  });

export const createApiKeyQueryStateObservable = createQueryStateObservable({
  dataCodec: io.any,
  fetch: () => {
    return fetch(`${process.env.REACT_APP_AUTH_API_URL}/api-key`, {
      method: 'POST',
    });
  },
});
