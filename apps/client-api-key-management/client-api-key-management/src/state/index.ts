import { observable } from 'mobx';
import { apiKeys } from 'state/modules/apiKeys';
import { auth, notifications, settings } from '@dataplatform/common-state/modules';

export const appState = observable({
  auth,
  apiKeys,
  notifications,
  settings,
});
