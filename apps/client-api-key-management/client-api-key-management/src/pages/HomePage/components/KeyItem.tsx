import React, { ReactNode } from 'react';
import { css } from '@emotion/core';
import { Box, Typography } from '@material-ui/core';

interface KeyItemProps {
  title: ReactNode;
  content: ReactNode;
}
export const KeyItem = ({ title, content }: KeyItemProps) => {
  return (
    <Box py={2}>
      <Box
        css={css`
          text-transform: uppercase;
        `}
      >
        <Typography component="div" color="textSecondary" variant="subtitle2">
          {title}
        </Typography>
      </Box>
      <div
        css={css`
          white-space: pre-wrap;
          word-break: break-all;
        `}
      >
        <code>{content}</code>
      </div>
    </Box>
  );
};
