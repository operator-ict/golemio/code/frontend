import * as React from 'react';
import moment from 'moment';
import * as io from 'io-ts';
import { useTranslation } from 'react-i18next';

import { ApiKeyIO } from 'codecs';
import { KeyItem } from 'pages/HomePage/components/KeyItem';
import { Divider } from '@material-ui/core';

interface DeletedKeyItemProps {
  item: io.TypeOf<typeof ApiKeyIO>;
}

export const DeletedKeyItem = ({ item }: DeletedKeyItemProps) => {
  const { t } = useTranslation();
  return (
    <>
      <Divider />
      <KeyItem
        title={
          <>
            {!!item.deleted_at && (
              <div>
                {t('deleted_at')}: {moment(item.deleted_at).format('LLLL')}
              </div>
            )}
          </>
        }
        content={item.code}
      />
    </>
  );
};
