import * as React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

import { ActiveKeyItem } from './components/ActiveKeyItem';
import { DeletedKeyItem } from './components/DeletedKeyItem';

import { appState } from 'state';
import { observer } from 'mobx-react-lite';
import { createApiKeyQueryStateObservable } from 'state/modules/apiKeys';
import { useState } from 'react';
import { NotificationType, withAuthenticatedUser } from '@dataplatform/common-state/modules';
import {
  Container,
  Button,
  Box,
  CircularProgress,
  Card,
  CardContent,
  Link,
  Typography,
  Divider,
} from '@material-ui/core';
import { AppLayout } from 'components/AppLayout';
import { ROUTE_TAC } from 'apps/client-api-key-management/client-api-key-management/src/routes';

export const HomePage = withAuthenticatedUser()(
  observer(() => {
    const { t } = useTranslation();
    React.useEffect(() => {
      appState.apiKeys.fetch({});
    }, []);

    const [creating, setCreating] = useState(false);

    const apiKeysState = appState.apiKeys;

    if (!apiKeysState.data) {
      return <div>loading</div>;
    }

    /*if (apiKeysState.error) {
	  return <pre>{JSON.stringify(apiKeysState.error, null, 2)}</pre>;
	}*/

    //   apiKeys

    const deletedKeys = apiKeysState.data.filter((item) => item.deleted_at);
    const activeKeys = apiKeysState.data.filter((item) => !item.deleted_at);

    return (
      <AppLayout>
        <Container maxWidth="md">
          <Box py={4}>
            <Card>
              <CardContent>
                <Box display="flex" justifyContent="space-between" pb={2} alignItems="center">
                  <div>
                    <Typography component="h2" variant="h6">
                      {t('page::dashboard::home::title')}
                    </Typography>
                  </div>
                  <div>
                    <Button
                      variant="contained"
                      disabled={activeKeys.length > 0}
                      onClick={async () => {
                        setCreating(true);
                        try {
                          await createApiKeyQueryStateObservable.fetch({});
                        } catch (error) {
                          appState.notifications.addNotification({
                            title: 'snacks::keyCreateError',
                            type: NotificationType.error,
                          });
                          setCreating(false);
                          return;
                        }
                        await apiKeysState.fetch({});
                        appState.notifications.addNotification({
                          title: 'snacks::keyCreated',
                          type: NotificationType.success,
                        });
                        setCreating(false);
                      }}
                      color="primary"
                    >
                      {creating && (
                        <>
                          <CircularProgress color="secondary" size={20} />
                          &nbsp;&nbsp;
                        </>
                      )}
                      {t('create_new')}
                    </Button>
                  </div>
                </Box>
                <Divider />
                {activeKeys.length > 0 ? (
                  activeKeys.map((item) => <ActiveKeyItem key={item.id} item={item} />)
                ) : (
                  <p>{t('page::dashboard::home::noApiKeys')}</p>
                )}
              </CardContent>
            </Card>
            <Card style={{ marginTop: '1.5rem' }}>
              <CardContent>
                <Box pb={3}>
                  <Typography component="h2" variant="h6">
                    {t('page::dashboard::home::deletedApiKeys')}
                  </Typography>
                </Box>
                {deletedKeys.length > 0 ? (
                  deletedKeys.map((item) => <DeletedKeyItem key={item.id} item={item} />)
                ) : (
                  <p>{t('page::dashboard::home::noDeletedApiKeys')}</p>
                )}
              </CardContent>
            </Card>
            <Box pt={4} display="flex" justifyContent="center">
              <Link component={RouterLink} to={ROUTE_TAC.create({})}>
                {t('linkTerms')}
              </Link>
            </Box>
          </Box>
        </Container>
      </AppLayout>
    );
  }),
);
