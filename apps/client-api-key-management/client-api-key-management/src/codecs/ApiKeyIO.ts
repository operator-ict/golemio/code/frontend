import * as io from 'io-ts';
import { maybe } from '@dataplatform/common-state';

export const ApiKeyIO = io.interface({
  id: io.union([io.string, io.number]),
  code: io.string,
  user_id: io.number,
  created_at: io.string,
  updated_at: io.string,
  deleted_at: maybe(io.string),
});
