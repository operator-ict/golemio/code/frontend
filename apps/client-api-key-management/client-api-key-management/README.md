# API Key Management for Golemio

API Key Management for the Golemio Data Platform System.

Developed by http://operatorict.cz

## Local installation

### Prerequisites

- node.js
- npm
- yarn

### Installation

1. Install all prerequisites
2. Install all dependencies using command: `yarn install` from the monorepo's root directory.
3. Create `.env` file in `apps/client-api-key-management/src`

## Usage

## NPM Scripts

from the monorepo's root directory.


Local start

For client-api-key-management `npm run-script start-local-client-api-key-management`


Local build

For client-api-key-management `npm run-script build-local-client-api-key-management`


Dev start

For client-api-key-management `npm run-script start-dev-client-api-key-management`


Dev build

For client-api-key-management `npm run-script build-dev-client-api-key-management`


Prod start

For client-api-key-management `npm run-script start-prod-client-api-key-management`


Prod build

For client-api-key-management `npm run-script build-prod-client-api-key-management`

Local tests

For client-api-key-management `e2e-local-client-api-key-management`
For client-api-key-management `e2e-local-watch-client-api-key-management`

Dev tests
For client-api-key-management `e2e-dev-client-api-key-management`

Prod tests
For client-api-key-management `e2e-prod-client-api-key-management`

## Contribution guidelines

Please read [CONTRIBUTING.md](CONTRIBUTING.md).

## Problems?

Contact benak@operatorict.cz or vycpalek@operatorict.cz
