import { getGreeting } from '../support/app.po';

describe('client-panel-client-panel', () => {
  beforeEach(() => cy.visit('/'));

  it('should display welcome message', () => {
    getGreeting().contains('Welcome to client-panel-client-panel!');
  });
});
