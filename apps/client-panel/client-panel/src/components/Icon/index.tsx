/**
 * This is a component for icons using material ui icons, see the list of icons her:
 * https://material.io/tools/icons/?style=baseline
 *
 * Use it as
 * <Icon>face</Icon>
 *
 * Icon font is loaded in public/index.html via CDN
 */

import createSimpleComponent from '../createSimpleComponent';

import cls from './mui-icon.module.scss';

export default createSimpleComponent({
  displayName: 'Icon',
  className: cls.mi,
  Component: 'i',
});
