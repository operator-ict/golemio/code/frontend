// for components that just need styling
import React, { ReactNode, ElementType } from 'react';
import classnames from 'classnames';

interface CreateSimpleComponentProps {
  displayName: string;
  className?: string;
  /**
   * React Element or String
   */
  Component?: ElementType;
  /**
   * pass a function to build custom className from props
   */
  classNameFromProps?: (props: { [prop: string]: unknown }) => string;
  /**
   * List of props explicitly not passed down to the element
   */
  excludeProps?: string[];
}

export interface SimpleComponentProps {
  className?: string;
  Component?: ElementType;
  children: ReactNode;
  [prop: string]: unknown;
}

export const createSimpleComponent = <ExtraProps extends { [prop: string]: unknown }>(
  props: CreateSimpleComponentProps,
) => {
  const {
    displayName,
    className: cl,
    Component = 'div',
    classNameFromProps = () => '',
    excludeProps = [],
  } = props;

  const SimpleComponent = (props: SimpleComponentProps & ExtraProps) => {
    const { className = null, Component: PComponent = Component, children = null, ...rest } = props;
    const otherProps = Object.keys(rest).filter((propKey) => excludeProps.indexOf(propKey) > -1);

    return (
      <PComponent className={classnames(cl, className, classNameFromProps(props))} {...otherProps}>
        {children}
      </PComponent>
    );
  };

  SimpleComponent.displayName = displayName;

  return SimpleComponent;
};

export default createSimpleComponent;
