/**
 * This component is jsut a container with 15px padding :)
 */

import classnames from 'classnames';
import createSimpleComponent from '../createSimpleComponent';

import cls from './widget-container.module.scss';

export default createSimpleComponent<{ limitWidth?: boolean }>({
  displayName: 'WidgetContainer',
  className: cls.wrapper,
  Component: 'div',
  classNameFromProps: (props) =>
    classnames({
      [cls.limitWidth]: props.limitWidth,
    }),
});
