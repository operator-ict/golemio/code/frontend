import React from 'react';
import classnames from 'classnames';

import { Snack } from '../Snack';

import cls from './snack-container.module.scss';
import { appState } from 'state';
import { observer } from 'mobx-react-lite';

interface SnackContainerProps {
  className?: string;
}

export const SnackContainer = observer(({ className }: SnackContainerProps) => {
  const snacks = appState.notifications.notifications;

  return (
    <div className={classnames(cls.box, className)}>
      {snacks.map((data) => (
        <Snack key={data.id} data={data} />
      ))}
    </div>
  );
});
