import React, { FC, ReactNode, Component } from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
  CircularProgress,
} from '@material-ui/core';
import { useTranslation } from 'react-i18next';

export const ConfirmDialog: FC<{
  title: string;
  isOpen: boolean;
  onClose: () => void;
  isLoading: boolean;
  onConfirm: () => void;
  confirmIcon: ReactNode;
  confirmText: string;
}> = ({ title, isOpen, children, onClose, isLoading, onConfirm, confirmIcon, confirmText }) => {
  const { t } = useTranslation();

  return (
    <Dialog open={isOpen}>
      <DialogTitle>{title}</DialogTitle>
      <DialogContent>{children}</DialogContent>
      <DialogActions>
        <Button variant="contained" onClick={onClose} disabled={isLoading}>
          {t('cancel')}
        </Button>
        <Button
          disabled={isLoading}
          startIcon={isLoading ? <CircularProgress color="inherit" size={20} /> : confirmIcon}
          variant="contained"
          color="primary"
          onClick={onConfirm}
        >
          {confirmText}
        </Button>
      </DialogActions>
    </Dialog>
  );
};
