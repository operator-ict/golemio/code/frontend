import React, { FC } from 'react';
import ReactMarkdown from 'react-markdown';
import gfm from 'remark-gfm';
import {
  Table,
  TableRow,
  TableHead,
  TableBody,
  TableCell,
  TableContainer,
  Paper,
} from '@material-ui/core';

export const MarkdownPreview: FC<{ source: string }> = ({ source }) => {
  return (
    <ReactMarkdown
      plugins={[gfm]}
      renderers={{
        table: ({ children }) => (
          <TableContainer component={Paper}>
            <Table>{children}</Table>
          </TableContainer>
        ),
        tableRow: ({ children }) => <TableRow>{children}</TableRow>,
        tableHead: ({ children }) => <TableHead>{children}</TableHead>,
        tableBody: ({ children }) => <TableBody>{children}</TableBody>,
        tableCell: ({ children, align }) => (
          <TableCell align={align || undefined}>{children}</TableCell>
        ),
      }}
      source={source}
    />
  );
};
