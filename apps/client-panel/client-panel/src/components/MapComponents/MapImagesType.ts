export type MapImageType = [string, HTMLImageElement];
export type MapImagesType = MapImageType[] | MapImageType;
