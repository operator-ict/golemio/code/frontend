import React from 'react';
import ReactJson from 'react-json-view';
import { ContentWrapper } from './ContentWrapper';
import { useTranslation } from 'react-i18next';

interface RenderProps {
  properties: object | null;
}

export const RenderJson = (props: RenderProps) => {
  const { properties } = props;
  const { t } = useTranslation();

  if (!properties) {
    return (
      <ContentWrapper>
        <h2>{t('page::sortedWaste::noData')}</h2>
      </ContentWrapper>
    );
  }

  return (
    <ContentWrapper>
      <ReactJson
        src={properties}
        name="Properties"
        indentWidth={2}
        collapsed={1}
        groupArraysAfterLength={100}
        displayDataTypes={false}
        sortKeys={true}
        enableClipboard={false}
        theme="brewer"
      />
    </ContentWrapper>
  );
};
