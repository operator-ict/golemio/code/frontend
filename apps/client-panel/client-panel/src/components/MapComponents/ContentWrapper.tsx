import React from 'react';
import cls from './detail.module.scss';

interface ContentWrapperProps {
  children: React.ReactNode;
}

export const ContentWrapper = (props: ContentWrapperProps) => {
  const { children } = props;

  return <div className={cls.content}>{children}</div>;
};
