import React, { ReactNode } from 'react';
import { ContainerVertical, ContentLoader } from 'components';
import { useTranslation } from 'react-i18next';
import cls from './detail.module.scss';

interface WrapperProps {
  isLoading: boolean;
  isError: boolean;
  children: ReactNode;
}

export const MapWrapper = (props: WrapperProps) => {
  const { isLoading, children, isError } = props;
  const { t } = useTranslation();
  return (
    <ContainerVertical style={{ position: 'relative' }}>
      {isLoading && <ContentLoader>{t('page::global::loadingText')}</ContentLoader>}
      {!isLoading && isError && <div className={cls.error}>{t('page::global::loadingError')}</div>}
      {!isLoading && !isError && children}
    </ContainerVertical>
  );
};
