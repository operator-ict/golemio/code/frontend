import React from 'react';
import cls from './detail.module.scss';
import { ClickAwayListener } from '@material-ui/core';

const LEFT_BUTTON = 0;

interface DetailProps {
  children: React.ReactNode;
  hideDetail: () => void;
}

export const DetailWrapper = (props: DetailProps) => {
  const { children, hideDetail } = props;

  return (
    <ClickAwayListener
      mouseEvent="onClick"
      onClickAway={(e) => {
        if (e.button === LEFT_BUTTON) {
          hideDetail();
        }
      }}
    >
      <div className={cls.wrapper}>
        {children}
        <span className={cls.close} onClick={hideDetail} role="button" tabIndex={0}>
          &times;
        </span>
      </div>
    </ClickAwayListener>
  );
};
