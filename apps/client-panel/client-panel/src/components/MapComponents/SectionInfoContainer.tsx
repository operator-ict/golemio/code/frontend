import React from 'react';
import cls from './detail.module.scss';

interface SectionInfoContainerProps {
  children: React.ReactNode;
}

export const SectionInfoContainer = (props: SectionInfoContainerProps) => {
  const { children } = props;
  return <div className={cls.sectionInfoContainer}>{children}</div>;
};
