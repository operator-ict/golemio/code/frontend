import React, { FC, useMemo } from 'react';
import { makeStyles, Typography } from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import analysesImage from './analyses.png';
import applicationsImage from './applications.png';
import dashboardsImage from './dashboards.png';
import dataExportImage from './dataExport.png';
import Carousel from 'react-material-ui-carousel';

const DELAY = 8000;

const useStyles = makeStyles(() => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    minHeight: 500,
    '& > .CarouselItem, & > .CarouselItem > div, & > .CarouselItem > div > div': {
      display: 'flex',
      flex: '1 0 auto',
    },
  },
  image: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    maxWidth: 400,
  },
  card: {
    padding: 20,
    backgroundColor: '#fffd',
    minHeight: 130,
  },
  cardTitle: {
    marginBottom: 10,
  },
  indicatorContainer: {
    position: 'absolute',
    bottom: 8,
  },
}));

export interface CardData {
  title?: string;
  description?: string;
  image: string;
}

const useDefaultCards = () => {
  const { t } = useTranslation();
  const defaultCards: CardData[] = [
    {
      title: t('dashboards'),
      description: t('dashboardsDescription'),
      image: dashboardsImage,
    },
    {
      title: t('dataExport'),
      description: t('dataExportDescription'),
      image: dataExportImage,
    },
    {
      title: t('applications'),
      description: t('applicationsDescription'),
      image: applicationsImage,
    },
    {
      title: t('analyses'),
      description: t('analysesDescription'),
      image: analysesImage,
    },
  ];

  return { defaultCards };
};

export const AuthCarousel: FC<{ cards?: CardData[] }> = ({ cards }) => {
  const { defaultCards } = useDefaultCards();
  const usedCards = useMemo(() => cards || defaultCards, [cards, defaultCards]);
  const classes = useStyles({});

  return (
    <Carousel
      animation="slide"
      interval={DELAY}
      className={classes.container}
      indicatorContainerProps={{ className: classes.indicatorContainer, style: {} }}
      timeout={{ appear: 0, enter: 500, exit: 500 }}
    >
      {usedCards.map((usedCard) => (
        <div
          className={classes.image}
          css={{ backgroundImage: `url(${usedCard.image})` }}
          key={usedCard.image}
        >
          {usedCard.title && usedCard.description && (
            <div className={classes.card}>
              <Typography variant="h2" className={classes.cardTitle}>
                {usedCard.title}
              </Typography>
              <Typography variant="body2" color="textSecondary">
                {usedCard.description}
              </Typography>
            </div>
          )}
        </div>
      ))}
    </Carousel>
  );
};
