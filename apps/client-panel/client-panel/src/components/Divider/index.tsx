import React from 'react';
import classnames from 'classnames';

import cls from './divider.module.scss';

interface DividerProps {
  className?: string;
  direction?: 'vertical' | 'horizontal';
  margin?: boolean;
  vertical?: boolean;
  [prop: string]: unknown;
}

const Divider = (props: DividerProps) => {
  const {
    className = null,
    direction = 'horizontal',
    margin = false,
    vertical = false,
    ...rest
  } = props;

  return (
    <div
      className={classnames(
        {
          [cls.vertical]: vertical || direction === 'vertical',
          [cls.horizontal]: !(vertical || direction === 'vertical'),
          [cls.margin]: margin,
        },
        className,
      )}
      {...rest}
    />
  );
};

export default Divider;
