import React, { FC, useState, useRef } from 'react';
import { Button, makeStyles, Typography, Menu, MenuItem, Toolbar } from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import GolemioLogo from './GolemioWhite.svg';
import { appState } from 'state';
import { observer } from 'mobx-react-lite';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const useStyles = makeStyles((theme) => ({
  container: {
    backgroundColor: theme.palette.primary.main,
    display: 'flex',
    justifyContent: 'center',
  },
  header: {
    display: 'flex',
    justifyContent: 'space-between',
    padding: 16,
    color: theme.palette.common.white,
    flexBasis: '100%',
    alignItems: 'center',
    flexWrap: 'wrap',
  },
  homeLink: {
    textTransform: 'uppercase',
    color: 'inherit',
    textDecoration: 'none',
  },
  logo: {
    height: 40,
  },
  itemWrapper: {
    flex: '1 0 0',
    '&:last-child': {
      textAlign: 'right',
    },
  },
  logout: {
    display: 'flex',
    alignItems: 'center',
  },
}));

export const Header: FC = observer(() => {
  const { t } = useTranslation();
  const classes = useStyles({});
  const { user, signOut } = appState.auth;
  const [isLogoutMenuOpen, setLogoutMenuOpen] = useState(false);
  const logoutMenuAnchor = useRef(null);

  return (
    <Toolbar className={classes.container}>
      <div className={classes.header}>
        <div className={classes.itemWrapper}>
          <a href="https://golemio.cz/" target="_blank" rel="noreferrer noopener">
            <img alt="Golemio logo" className={classes.logo} src={GolemioLogo} />
          </a>
        </div>
        <div>
          <Typography className={classes.homeLink} variant="h1">
            <Link className={classes.homeLink} to={user ? '/dashboard' : '/'}>
              {t('appName')}
            </Link>
          </Typography>
        </div>
        <div className={classes.itemWrapper}>
          {!!user && (
            <>
              <Button
                type="button"
                variant="text"
                color="inherit"
                onClick={() => setLogoutMenuOpen(true)}
              >
                <Typography color="inherit" className={classes.logout}>
                  {user.fullname || user.email} <ExpandMoreIcon ref={logoutMenuAnchor} />
                </Typography>
              </Button>
              <Menu
                anchorEl={logoutMenuAnchor.current}
                open={isLogoutMenuOpen}
                onClose={() => setLogoutMenuOpen(false)}
              >
                <MenuItem onClick={signOut}>{t('menu::signOut')}</MenuItem>
              </Menu>
            </>
          )}
        </div>
      </div>
    </Toolbar>
  );
});
