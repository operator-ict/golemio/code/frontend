import React, { FC } from 'react';
import { Box, FormGroup, FormControlLabel, Checkbox, Tooltip, IconButton } from '@material-ui/core';
import { Delete } from '@material-ui/icons';
import { useTranslation } from 'react-i18next';
import { observer } from 'mobx-react-lite';
import { appState } from 'state';
import { Tag } from 'codecs/tag-iots';

export const TopicItem: FC<{ tag: Tag; onDelete?: (tag: Tag) => void }> = observer(
  ({ tag, onDelete }) => {
    const { t } = useTranslation();
    const { selectedTags, toggleTagSelection } = appState.dashboardFilters;
    const { user } = appState.auth;

    return (
      <Box display="flex" alignItems="center" justifyContent="space-between" key={tag._id}>
        <FormGroup>
          <FormControlLabel
            control={
              <Checkbox
                checked={selectedTags.includes(tag._id)}
                onChange={() => toggleTagSelection(tag._id)}
                color="primary"
              />
            }
            label={tag.title}
          />
        </FormGroup>
        {!!onDelete && !!user?.admin && (
          <Tooltip title={t('deleteTag')}>
            <IconButton onClick={() => onDelete(tag)}>
              <Delete />
            </IconButton>
          </Tooltip>
        )}
      </Box>
    );
  },
);
