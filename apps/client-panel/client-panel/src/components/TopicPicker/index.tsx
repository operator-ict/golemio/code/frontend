import React, { FC, useState, useCallback, useMemo, useEffect } from 'react';
import {
  Button,
  makeStyles,
  Popover,
  Box,
  Divider,
  Badge,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  CircularProgress,
} from '@material-ui/core';
import { useTranslation, Trans } from 'react-i18next';
import filterIcon from './filterIcon.svg';
import { observer } from 'mobx-react-lite';
import { appState } from 'state';
import { Delete } from '@material-ui/icons';
import { TopicItem } from 'components/TopicPicker/TopicItem';
import { Tag } from 'codecs/tag-iots';
import { ConfirmDialog } from 'components/ConfirmDialog';

const useStyles = makeStyles((theme) => ({
  icon: {
    marginRight: 8,
  },
  selected: {
    backgroundColor: theme.palette.action.selected,
  },
  root: {
    height: 'calc(100% - 184px)',
    left: '0 !important', // override inline styles
  },
}));

export const TopicPicker: FC<{ anchor: HTMLElement | null }> = observer(({ anchor }) => {
  const { t } = useTranslation();
  const classes = useStyles({});
  const [isOpen, setOpen] = useState(false);
  const { data, selectedTags } = appState.dashboardFilters;
  const {
    data: adminTags,
    fetch: fetchAdminTags,
    isLoading: isLoadingAdminTags,
  } = appState.adminTags;
  const { fetch: deleteTag, isLoading: isDeleteLoading } = appState.adminTags.deleteTag;
  const { admin } = appState.auth.user;
  const { data: allMetadata } = appState.metadata.allMetadata;
  const [isDeleteConfirmationOpen, setDeleteConfirmationOpen] = useState(false);
  const [tagToDelete, setTagToDelete] = useState<Tag | null>(null);

  useEffect(() => {
    if (!admin || adminTags || isLoadingAdminTags) {
      return;
    }

    fetchAdminTags({});
  }, []);

  const tagToDeleteUsedCount = useMemo(() => {
    if (!tagToDelete || !allMetadata) {
      return null;
    }

    const metadataWithTag = allMetadata.filter(
      (metadata) => !!metadata.tags.find((tag) => tag._id === tagToDelete._id),
    );

    return metadataWithTag.length;
  }, [tagToDelete, allMetadata]);

  const askForDeleteConfirmation = useCallback((tag: Tag) => {
    setTagToDelete(tag);
    setDeleteConfirmationOpen(true);
  }, []);

  const closeConfirmation = useCallback(() => {
    setDeleteConfirmationOpen(false);
  }, []);

  const otherTags = useMemo(() => {
    if (!admin || !adminTags) {
      return [];
    }

    return adminTags.filter((adminTag) => !data?.find((tag) => tag._id === adminTag._id));
  }, [admin, adminTags, data]);

  return (
    <>
      <Badge
        invisible={!selectedTags.length}
        badgeContent={selectedTags.length || ''}
        color="primary"
      >
        <Button
          type="button"
          onClick={() => setOpen(true)}
          className={isOpen ? classes.selected : undefined}
          disabled={!data?.length}
        >
          <img src={filterIcon} className={classes.icon} />
          {t('topics')}
        </Button>
      </Badge>
      {anchor && (
        <Popover
          classes={{ paper: classes.root }}
          anchorEl={anchor}
          anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
          transformOrigin={{ horizontal: 'left', vertical: 'top' }}
          open={isOpen}
          onClose={() => setOpen(false)}
        >
          <Box p={2}>
            <Divider />
            <Box p={4}>
              {data?.map((tag) => (
                <TopicItem key={tag._id} tag={tag} onDelete={askForDeleteConfirmation} />
              ))}
              {!!otherTags?.length && (
                <>
                  <Divider />
                  {otherTags.map((tag) => (
                    <TopicItem key={tag._id} tag={tag} onDelete={askForDeleteConfirmation} />
                  ))}
                </>
              )}
              {!!tagToDelete && (
                <ConfirmDialog
                  title={t('deleteConfirmation')}
                  isOpen={isDeleteConfirmationOpen}
                  onClose={closeConfirmation}
                  isLoading={isDeleteLoading}
                  onConfirm={() => {
                    deleteTag({ params: { tagId: tagToDelete._id } }).then(closeConfirmation);
                  }}
                  confirmIcon={<Delete />}
                  confirmText={t('deleteTag')}
                >
                  <Trans
                    i18nKey="deleteTagConfirmationQuestion"
                    components={[<strong />]}
                    values={{
                      name: tagToDelete.title,
                      usedCount: tagToDeleteUsedCount ?? '?',
                    }}
                  />
                </ConfirmDialog>
              )}
            </Box>
            <Divider />
          </Box>
        </Popover>
      )}
    </>
  );
});
