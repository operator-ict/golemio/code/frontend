import React, { ReactElement } from 'react';
import { Radio, FormControlLabel, Typography } from '@material-ui/core';
import ContainerVertical from '../ContainerVertical';
import cls from './filterSelect.module.scss';

interface FilterSelectProps<OptionValue> {
  onChange: (selected: string) => void;
  optionList: {
    value: OptionValue;
    label: string;
  }[];
  value: OptionValue;
  title: string;
}

const FilterSelect: <T extends string>(props: FilterSelectProps<T>) => ReactElement = (props) => {
  const { onChange, optionList, value, title } = props;
  const handleChange = (e: React.ChangeEvent<HTMLInputElement>, checked: boolean) => {
    if (checked) {
      onChange(e.target.value);
    }
  };

  return (
    <ContainerVertical>
      <Typography variant="h4" component="h2">
        {title}
      </Typography>
      {optionList.map((option, index) => (
        <FormControlLabel
          key={index}
          classes={{ root: cls.label }}
          control={
            <Radio
              classes={{ root: cls.radio }}
              checked={value === option.value}
              onChange={handleChange}
              value={option.value}
              color="secondary"
            />
          }
          label={option.label}
        />
      ))}
    </ContainerVertical>
  );
};

export default FilterSelect;
