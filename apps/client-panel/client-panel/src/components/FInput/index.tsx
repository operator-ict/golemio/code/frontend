// for use with Formik <Field component={FInput} name="input" />
import React, { ReactNode } from 'react';
import classnames from 'classnames';
import { Field } from 'formik';
import { useTranslation } from 'react-i18next';
import ContentTitle from '../ContentTitle';
import cls from './input.module.scss';

interface InputProps {
  wrapperProps?: { className?: string; [prop: string]: unknown };
  label?: ReactNode;
  className?: string;
  icon?: ReactNode;
  iconPosition?: 'left' | 'right';
  clear?: boolean;
  errorMessage?: string;
  name: string;
  [prop: string]: any;
}

const BaseInput = (props: InputProps) => {
  const {
    className = null,
    wrapperProps = {},
    icon = null,
    label = null,
    clear = false,
    iconPosition = 'left',
    errorMessage = null,
    field, // formik
    form, // formik
    ...rest
  } = props;

  const errMsg = form.errors[field.name] || errorMessage;
  const { t } = useTranslation();

  return (
    <div
      {...wrapperProps}
      className={classnames(wrapperProps && wrapperProps.className, cls.formGroup, {
        [cls.clear]: clear,
      })}
    >
      {label && <ContentTitle>{label}</ContentTitle>}
      <div className={classnames(cls.inputWrapper)}>
        {icon && (
          <span
            className={classnames(cls.iconWrapper, {
              [cls.iconWrapperRight]: iconPosition === 'right',
            })}
          >
            {icon}
          </span>
        )}
        <input
          className={classnames(className, cls.input, {
            [cls.errorHighlight]: errMsg,
          })}
          name={field.name}
          onBlur={field.onBlur}
          onChange={field.onChange}
          type="text"
          value={field.value}
          {...rest}
        />
      </div>
      {errMsg && <p className={cls.errorMessage}>{t(errMsg)}</p>}
    </div>
  );
};

const FInput = (props: InputProps) => <Field component={BaseInput} {...props} />;

export default FInput;
