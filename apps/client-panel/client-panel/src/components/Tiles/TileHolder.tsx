import React, { FC } from 'react';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  container: {
    maxWidth: theme.breakpoints.values.md,
    margin: 'auto',
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center',
    width: '100%',
  },
}));

export const TileHolder: FC = ({ children }) => {
  const classes = useStyles({});

  return <div className={classes.container}>{children}</div>;
};
