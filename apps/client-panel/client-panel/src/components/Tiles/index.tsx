import React, { FC, useMemo, useEffect, useCallback, useState } from 'react';
import { observer } from 'mobx-react-lite';
import { appState } from 'state';
import {
  Box,
  makeStyles,
  CircularProgress,
  Typography,
  Select,
  MenuItem,
  Divider,
  Button,
  IconButton,
  Tooltip,
} from '@material-ui/core';
import { useTranslation, Trans } from 'react-i18next';
import { Tile } from 'components/Tiles/Tile';
import { UnknownMetadata, MetadataType } from 'codecs';
import { Link } from 'react-router-dom';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import { TileCategoryTitle } from 'components/Tiles/TileCategoryTitle';
import { TileContentWrapper } from 'components/Tiles/TileContentWrapper';
import { TileHolder } from 'components/Tiles/TileHolder';
import { getName } from 'utils/getName';
import { getNormalizedText } from 'utils/getNormalizedText';
import { Clear, Delete } from '@material-ui/icons';
import { ConfirmDialog } from 'components/ConfirmDialog';

const useStyles = makeStyles((theme) => ({
  loaderWrapper: {
    width: '100%',
    textAlign: 'center',
  },
  searchResultLabel: {
    fontWeight: 'normal',
    marginRight: theme.spacing(1),
  },
}));

const filterMetadata = (
  data: UnknownMetadata[],
  category: MetadataType,
  selectedTags: string[],
  search: string,
) => {
  if (!category && selectedTags.length === 0 && !search) {
    return data;
  }

  const searchWords = getNormalizedText(search)
    .split(/\s/)
    .filter((word) => !!word);

  return data?.filter((item) => {
    if (category && category !== item.type) {
      return false;
    }

    if (selectedTags.length !== 0) {
      const itemTagIds = item.tags.map((tag) => tag._id);
      const hasSelectedTag = selectedTags.find((tag) => itemTagIds.includes(tag));

      if (!hasSelectedTag) {
        return false;
      }
    }

    if (search) {
      const tagTitles = item.tags.map((tag) => getNormalizedText(tag.title));
      for (const word of searchWords) {
        if (
          !getNormalizedText(item.description).includes(word) &&
          !getNormalizedText(item.title).includes(word) &&
          !tagTitles.find((title) => title.includes(word))
        ) {
          return false;
        }
      }
    }

    return true;
  });
};

export const Tiles: FC = observer(() => {
  const { selectedTags, category, search, setSearch } = appState.dashboardFilters;
  const { admin, id: userId } = appState.auth.user;
  const { t } = useTranslation();
  const classes = useStyles({});
  const [tileToDelete, setTileToDelete] = useState<UnknownMetadata | null>(null);
  const [isDeleteConfirmationOpen, setDeleteConfirmationOpen] = useState(false);
  const {
    isLoading: isDeleteTileLoading,
    fetch: deleteMetadata,
  } = appState.metadata.deleteMetadata;

  const {
    data: users,
    isLoading: areUsersLoading,
    fetch: fetchUsers,
    selectedUser,
    selectUser,
  } = appState.users;

  const {
    data: allMetadata,
    fetch: fetchAllMetadata,
    isLoading: isAllMetadataLoading,
  } = appState.metadata.allMetadata;

  const { selectedUserMetadata, isSelectedUserMetadataLoading } = appState.metadata;

  const { fetch: fetchUserData, favouriteTiles } = appState.userData;

  const filteredMetadata = useMemo(() => {
    return filterMetadata(selectedUserMetadata, category, selectedTags, search);
  }, [selectedUserMetadata, category, selectedTags, search]);

  const filteredFavouriteTiles = useMemo(() => {
    const favouriteMetadata = favouriteTiles
      .map((favTileId) =>
        (admin ? allMetadata : selectedUserMetadata)?.find(
          (metadata) => metadata._id === favTileId,
        ),
      )
      .filter((tile) => !!tile);

    return filterMetadata(favouriteMetadata, category, selectedTags, search);
  }, [favouriteTiles, category, selectedTags, admin, allMetadata, selectedUserMetadata, search]);

  const filteredAllMetadata = useMemo(() => {
    const remaining =
      allMetadata?.filter(
        (metadata) =>
          !selectedUserMetadata?.find(
            (userMetadata) => userMetadata.client_route === metadata.client_route,
          ),
      ) || [];
    return filterMetadata(remaining, category, selectedTags, search);
  }, [selectedUserMetadata, category, selectedTags, search, allMetadata]);

  const clearSearch = useCallback(() => {
    setSearch('');
  }, [setSearch]);

  useEffect(() => {
    if (!areUsersLoading && admin) {
      fetchUsers({});
    }

    if (!isAllMetadataLoading && admin) {
      fetchAllMetadata({});
    }

    fetchUserData({});
  }, []);

  const showConfirmDeleteDialog = useCallback((tile: UnknownMetadata) => {
    setTileToDelete(tile);
    setDeleteConfirmationOpen(true);
  }, []);

  const closeDeleteConfirmation = useCallback(() => {
    setDeleteConfirmationOpen(false);
  }, []);

  const sortedUsers = useMemo(() => {
    return users
      ?.map((user) => ({
        id: user.id,
        name: getName(user.name, user.surname, user.email),
      }))
      .sort((a, b) => a.name.localeCompare(b.name));
  }, [users]);

  const showFavourites = favouriteTiles.length > 0 && (!admin || selectedUser?.id === userId);

  return (
    <TileContentWrapper>
      {admin && (
        <>
          <Box textAlign="center">
            <Button
              startIcon={<AddCircleIcon />}
              variant="contained"
              color="primary"
              component={Link}
              to="/dashboard/new"
            >
              {t('addNewTile')}
            </Button>
          </Box>
          <Box display="flex" alignItems="center" justifyContent="center">
            <Typography>{t('tilesShownForUser')}&nbsp;</Typography>
            {!areUsersLoading && sortedUsers && (
              <Select
                value={selectedUser?.id ?? userId}
                onChange={({ target }) => selectUser(target.value as number)}
              >
                {sortedUsers.map((user) => (
                  <MenuItem value={user.id} key={user.id}>
                    {user.name}
                  </MenuItem>
                ))}
              </Select>
            )}
            {areUsersLoading && <CircularProgress />}
          </Box>
        </>
      )}
      {admin && selectedUser && (
        <Typography align="center" variant="h2">
          {t('usersTiles', {
            name: getName(selectedUser.name, selectedUser.surname, selectedUser.email),
          })}
        </Typography>
      )}
      {isSelectedUserMetadataLoading && (
        <div className={classes.loaderWrapper}>
          <CircularProgress />
        </div>
      )}
      {!!search && (
        <Box
          display="flex"
          justifyContent="center"
          alignItems="center"
          flexWrap="wrap"
          textAlign="center"
        >
          <Typography variant="h2" className={classes.searchResultLabel}>
            {t('searchResults')}:
          </Typography>
          <Typography variant="h2">{search}</Typography>
          <Tooltip title={t('cancelSearch')}>
            <IconButton size="small" onClick={clearSearch}>
              <Clear color="primary" />
            </IconButton>
          </Tooltip>
        </Box>
      )}
      {!!selectedUserMetadata && !isSelectedUserMetadataLoading && (
        <>
          {showFavourites && (
            <>
              <TileCategoryTitle>{t('favourites')}</TileCategoryTitle>
              <TileHolder>
                {filteredFavouriteTiles.map((tile) => (
                  <Tile
                    key={tile.client_route}
                    tile={tile}
                    showConfirmDeleteDialog={showConfirmDeleteDialog}
                  />
                ))}
                {!filteredFavouriteTiles.length && (
                  <Typography>{t('nothingPassingFilters')}</Typography>
                )}
              </TileHolder>
              <Divider />
              <TileCategoryTitle>{t('other')}</TileCategoryTitle>
            </>
          )}
          <TileHolder>
            {filteredMetadata.length === 0 && <Typography>{t('nothingPassingFilters')}</Typography>}
            {filteredMetadata
              .filter((item) => !(favouriteTiles || []).includes(item._id))
              .map((item) => (
                <Tile
                  key={item.client_route}
                  tile={item}
                  showConfirmDeleteDialog={showConfirmDeleteDialog}
                />
              ))}
          </TileHolder>
        </>
      )}
      {admin && allMetadata && (
        <>
          <Divider />
          <Typography align="center" variant="h2">
            {t('otherTiles')}
          </Typography>
          <TileHolder>
            {filteredAllMetadata.length === 0 && (
              <Typography>{t('nothingPassingFilters')}</Typography>
            )}
            {filteredAllMetadata
              .filter((item) => !(favouriteTiles || []).includes(item._id))
              .map((item) => (
                <Tile
                  key={item.client_route}
                  tile={item}
                  showConfirmDeleteDialog={showConfirmDeleteDialog}
                />
              ))}
          </TileHolder>
        </>
      )}
      {admin && !!tileToDelete && (
        <ConfirmDialog
          title={t('deleteConfirmation')}
          isOpen={isDeleteConfirmationOpen}
          onClose={closeDeleteConfirmation}
          isLoading={isDeleteTileLoading}
          onConfirm={() => {
            deleteMetadata({ params: { metadataId: tileToDelete._id } }).then(
              closeDeleteConfirmation,
            );
          }}
          confirmIcon={<Delete />}
          confirmText={t('deleteTile')}
        >
          <Trans
            i18nKey="deleteTileConfirmationQuestion"
            components={[<strong />]}
            values={{
              name: tileToDelete.title,
            }}
          />
        </ConfirmDialog>
      )}
    </TileContentWrapper>
  );
});
