import React, { FC } from 'react';
import { makeStyles, Typography } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  categoryTitle: {
    width: theme.breakpoints.values.md,
    maxWidth: '100%',
    margin: 'auto',
  },
}));

export const TileCategoryTitle: FC = ({ children }) => {
  const classes = useStyles({});

  return (
    <div className={classes.categoryTitle}>
      <Typography variant="h3" component="h2">
        {children}
      </Typography>
    </div>
  );
};
