import React, { FC } from 'react';
import { makeStyles } from '@material-ui/core';
import ContainerVertical from 'components/ContainerVertical';
import ScrollArea from 'components/ScrollArea';
import { WidgetContainer } from 'components';

const useStyles = makeStyles(() => ({
  centered: {
    alignItems: 'center',
  },
  stretched: {
    alignItems: 'stretch',
  },
  fullWidth: {
    width: '100%',
  },
}));

export const TileContentWrapper: FC = ({ children }) => {
  const classes = useStyles({});

  return (
    <ContainerVertical className={classes.centered}>
      <ScrollArea className={classes.fullWidth}>
        <WidgetContainer className={classes.stretched}>{children}</WidgetContainer>
      </ScrollArea>
    </ContainerVertical>
  );
};
