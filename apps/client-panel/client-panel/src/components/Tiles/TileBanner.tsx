import React, { FC } from 'react';
import { makeStyles, Theme, CardMedia } from '@material-ui/core';
import { MetadataType } from 'codecs';
import { getMetadataTypeColor } from 'utils/metadataTypeColors';

const useStyles = makeStyles<Theme, { type: MetadataType }>(() => ({
  banner: {
    width: '100%',
    height: 74,
    backgroundColor: ({ type }) => getMetadataTypeColor(type, true),
  },
}));

export const TileBanner: FC<{ type: MetadataType; thumbnailUrl?: string }> = ({
  type,
  thumbnailUrl,
}) => {
  const classes = useStyles({ type });

  if (!thumbnailUrl) {
    return <div className={classes.banner} />;
  }

  return (
    <CardMedia
      image={`${process.env.REACT_APP_API_URL}${thumbnailUrl}`}
      className={classes.banner}
    />
  );
};
