import React, { ElementType, ReactNode } from 'react';
import classnames from 'classnames';

import cls from './container-h.module.scss';

interface ContainerHorizontalProps {
  className?: string;
  children?: ReactNode;
  Component?: ElementType;
  [prop: string]: unknown;
}

const ContainerHorizontal = (props: ContainerHorizontalProps) => {
  const { className = null, Component = 'div', children = null, ...rest } = props;

  return (
    <Component className={classnames(cls.wrapper, className)} {...rest}>
      {children}
    </Component>
  );
};

export default ContainerHorizontal;
