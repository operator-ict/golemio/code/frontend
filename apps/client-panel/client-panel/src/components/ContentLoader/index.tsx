import React, { ReactNode, ReactType } from 'react';
import classnames from 'classnames';
import cls from './content-loader.module.scss';

interface ContentLoaderProps {
  className?: string;
  Component?: ReactType;
  children?: ReactNode;
  asOverlay?: boolean;
  [prop: string]: unknown;
}

const ContentLoader = (props: ContentLoaderProps) => {
  const {
    className = null,
    Component = 'div',
    children = null,
    asOverlay = false,
    ...rest
  } = props;

  return (
    <Component
      className={classnames(cls.loader, className, {
        [cls.asOverlay]: asOverlay,
      })}
      {...rest}
    >
      <div className={cls.spinner} />
      {children && <div className={cls.content}>{children}</div>}
    </Component>
  );
};

export default ContentLoader;
