import React, { FC, ReactNode } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Header } from 'components/Header';

export const AppLayout: FC<{ footer?: ReactNode; header?: ReactNode }> = ({
  children,
  footer,
  header,
}) => {
  const classes = useStyles({});

  return (
    <div className={classes.root}>
      {header || <Header />}
      <main className={classes.content}>{children}</main>
      {footer}
    </div>
  );
};

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    minHeight: '100vh',
    flexDirection: 'column',
  },
  content: {
    flexGrow: 1,
    display: 'flex',
    flexDirection: 'column',
  },
}));
