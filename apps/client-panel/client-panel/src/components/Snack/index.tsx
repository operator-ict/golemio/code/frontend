import React, { useCallback } from 'react';
import classnames from 'classnames';
import { Notification } from 'state/modules/notifications';
import { useTranslation } from 'react-i18next';

import cls from './snack.module.scss';
import { appState } from 'state';

interface AlertBoxProps {
  data: Notification;
}

export const Snack = ({ data }: AlertBoxProps) => {
  const { t } = useTranslation(undefined, { useSuspense: false });

  const handleNotificationHoverStart = useCallback((event) => {
    appState.notifications.stopNotificationDecay(event.currentTarget.dataset.nid);
  }, []);
  const handleNotificationHoverEnd = useCallback((event) => {
    appState.notifications.startNotificationDecay(event.currentTarget.dataset.nid);
  }, []);

  return (
    <div
      data-nid={data.id}
      className={classnames(cls.box, { [cls[data.type || 'success']]: data.type })}
      onMouseEnter={handleNotificationHoverStart}
      onMouseLeave={handleNotificationHoverEnd}
    >
      {typeof data.title === 'string' ? <p className={cls.p}>{t(data.title)}</p> : data.title}
    </div>
  );
};
