import React from 'react';
import { useTranslation } from 'react-i18next';
import cls from './filter.module.scss';
import { Button, Popper, ClickAwayListener } from '@material-ui/core';

interface FilterWrapperProps {
  children: React.ReactNode;
  resetFilters: () => void;
}

const FilterWrapper = ({ children, resetFilters }: FilterWrapperProps) => {
  const { t } = useTranslation();
  const [anchorEl, setAnchorEl] = React.useState<HTMLElement | null>(null);

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };

  const handleClickAway = (e: React.MouseEvent<Document, MouseEvent>) => {
    const node = e.target as Node;
    const ignoredNodes = document.getElementsByClassName('ignoreClickAway');

    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < ignoredNodes.length; i++) {
      const ignoredNode = ignoredNodes[i];

      if (ignoredNode.contains(node)) {
        return;
      }
    }

    setAnchorEl(null);
  };

  const open = !!anchorEl;

  return (
    <>
      <Button
        className="ignoreClickAway"
        onClick={handleClick}
        variant={open ? 'outlined' : 'contained'}
        color="secondary"
      >
        {t('page::global::filters')}
      </Button>
      <Popper style={{ zIndex: 100 }} placement="bottom-end" anchorEl={anchorEl} open={open}>
        <ClickAwayListener onClickAway={handleClickAway} mouseEvent="onMouseDown">
          <div className={cls.filters}>
            {children}
            {resetFilters && (
              <Button className={cls.reset} onClick={resetFilters} variant="text" size="small">
                {t('page::global::resetFilters')}
              </Button>
            )}
          </div>
        </ClickAwayListener>
      </Popper>
    </>
  );
};

export default FilterWrapper;
