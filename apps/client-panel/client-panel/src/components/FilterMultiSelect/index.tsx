import React, { ChangeEvent } from 'react';
import ContainerVertical from '../ContainerVertical';
import cls from './multiselect.module.scss';
import { FormControlLabel, Checkbox, FormGroup, Radio, Typography } from '@material-ui/core';

interface FilterMultiSelectProps {
  onChange: (values: string[]) => void;
  optionList: Array<{ value: string; label: string; count?: number; type?: 'radio' | 'checkbox' }>;
  values: string[];
  title: string;
}

const FilterMultiSelect = (props: FilterMultiSelectProps) => {
  const { optionList, values, title, onChange } = props;

  const handleChange = (e: ChangeEvent<HTMLInputElement>, checked: boolean) => {
    if (checked) {
      const selected = e.target.type === 'radio' ? [] : [...values, e.target.value];
      onChange(selected);
    } else {
      const selected = values.filter((item) => item !== e.target.value);
      onChange(selected);
    }
  };

  return (
    <ContainerVertical>
      <Typography variant="h4" component="h2">
        {title}
      </Typography>
      {optionList.map((option) => {
        const id = `id-${name}-${option.value}`;
        const label =
          option.count !== undefined ? (
            <>
              {option.label}
              &nbsp;(
              <span className={cls.count}>{option.count}</span>)
            </>
          ) : (
            option.label
          );

        const InputElement = option.type === 'radio' ? Radio : Checkbox;

        return (
          <FormGroup row key={id}>
            <FormControlLabel
              classes={{ root: cls.label }}
              control={
                <InputElement
                  classes={{ root: cls.input }}
                  checked={values.includes(option.value) || (!values.length && option.value === '')}
                  onChange={handleChange}
                  value={option.value}
                  color="secondary"
                  disabled={option.count === 0}
                />
              }
              label={label}
            />
          </FormGroup>
        );
      })}
    </ContainerVertical>
  );
};

export default FilterMultiSelect;
