import React, { FC, useState, useCallback, FormEvent, ChangeEvent, useRef, useEffect } from 'react';
import { Input, InputAdornment, IconButton } from '@material-ui/core';
import { Search } from '@material-ui/icons';
import { useTranslation } from 'react-i18next';
import { observer } from 'mobx-react-lite';
import { appState } from 'state';

export const SearchInput: FC = observer(() => {
  const { t } = useTranslation();
  const { search, setSearch } = appState.dashboardFilters;
  const [searchValue, setSearchValue] = useState(search);
  const inputRef = useRef<HTMLInputElement | null>(null);

  const handleChange = useCallback(
    (e: ChangeEvent<HTMLInputElement>) => setSearchValue(e.currentTarget.value),
    [],
  );

  useEffect(() => setSearchValue(search), [search]);

  const handleSearch = useCallback(
    (e: FormEvent) => {
      e.preventDefault();
      if (!searchValue) {
        inputRef.current?.focus();
        return;
      }

      setSearch(searchValue);
    },
    [searchValue, setSearch],
  );

  return (
    <form onSubmit={handleSearch}>
      <Input
        onChange={handleChange}
        placeholder={t('search')}
        inputProps={{
          ref: inputRef,
        }}
        value={searchValue}
        endAdornment={
          <InputAdornment position="end">
            <IconButton type="submit">
              <Search />
            </IconButton>
          </InputAdornment>
        }
      />
    </form>
  );
});
