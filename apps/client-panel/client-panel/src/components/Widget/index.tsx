import React, { ReactNode, ElementType } from 'react';
import classnames from 'classnames';
import cls from './widget.module.scss';

interface WidgetProps {
  className?: string;
  children?: ReactNode;
  Component?: ElementType;
  padding?: boolean;
  margin?: boolean;
  [prop: string]: unknown;
}

const Widget = (props: WidgetProps) => {
  const {
    children = null,
    className = null,
    Component = 'div',
    padding = false,
    margin = false,
    ...rest
  } = props;

  return (
    <Component
      className={classnames(cls.wrapper, className, {
        [cls.padding]: padding,
        [cls.margin]: margin,
      })}
      {...rest}
    >
      {children}
    </Component>
  );
};

export default Widget;
