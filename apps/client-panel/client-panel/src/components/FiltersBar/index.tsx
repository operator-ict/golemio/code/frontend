import React, { FC, useState } from 'react';
import { makeStyles, Paper, Button } from '@material-ui/core';
import { TopicPicker } from 'components/TopicPicker';
import { useMetadataTypeData } from 'hooks/metadataTypeData';
import { observer } from 'mobx-react-lite';
import { appState } from 'state';
import classNames from 'classnames';
import { SearchInput } from 'components/SearchInput';

const useStyles = makeStyles((theme) => ({
  container: {
    padding: 20,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexWrap: 'wrap',
  },
  categoryLink: {
    color: theme.palette.secondary.main,
    display: 'flex',
    alignItems: 'center',
    '& > *:first-child': {
      marginRight: 4,
    },
    '& svg': {
      fill: theme.palette.secondary.main,
    },
  },
  active: {
    backgroundColor: theme.palette.action.selected,
    color: theme.palette.primary.main,
    '& svg': {
      fill: theme.palette.primary.main,
    },
  },
  linkWrapper: {
    display: 'flex',
    justifyContent: 'space-between',
    flex: '1 1 auto',
    maxWidth: 800,
    flexWrap: 'wrap',
    '& > *': {
      margin: 8,
    },
  },
  itemWrapper: {
    flex: '1 0 auto',
    '&:last-child': {
      textAlign: 'right',
    },
  },
}));

export const FiltersBar: FC = observer(() => {
  const classes = useStyles({});
  const { category, setCategory } = appState.dashboardFilters;
  const { metadataTypeData } = useMetadataTypeData();
  const [anchor, setAnchor] = useState<HTMLElement>(null);

  return (
    <Paper
      elevation={3}
      square={true}
      className={classes.container}
      ref={(c: HTMLElement) => setAnchor(c)}
    >
      <div className={classes.itemWrapper}>
        <TopicPicker anchor={anchor} />
      </div>
      <div className={classes.linkWrapper}>
        {metadataTypeData.map((type) => (
          <Button
            key={type.type?.toString() || 'all'}
            onClick={() => setCategory(type.type)}
            className={classNames(classes.categoryLink, {
              [classes.active]: type.type === category,
            })}
          >
            <type.Icon css={{ height: 20 }} />
            {type.title}
          </Button>
        ))}
      </div>
      <div className={classes.itemWrapper}>
        <SearchInput />
      </div>
    </Paper>
  );
});
