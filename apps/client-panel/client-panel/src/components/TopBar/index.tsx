import React from 'react';
import classnames from 'classnames';

import cls from './top-bar.module.scss';
import { observer } from 'mobx-react-lite';
import { css } from '@emotion/core';

interface TopBarProps {
  center?: boolean;
  className?: string;
  children?: React.ReactNode;
  [prop: string]: unknown;
}

const TopBar = observer((props: TopBarProps) => {
  const { children = null, className = null, center = false, ...rest } = props;

  return (
    <div
      css={css`
        border-bottom: 1px solid rgba(0, 0, 0, 0.12);
        height: 65px;
      `}
      className={classnames(cls.wrapper, className, {
        [cls.center]: center,
      })}
      {...rest}
    >
      <div
        css={css`
          display: flex;
          align-items: center;
          justify-content: space-between;
          flex: 1;
        `}
      >
        {children}
      </div>
    </div>
  );
});

export default TopBar;
