import React, { FC } from 'react';
import { Typography, Paper, makeStyles } from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import OICTLogo from './OICT.png';

const useStyles = makeStyles(() => ({
  wrapper: {
    padding: 16,
    display: 'flex',
    alignItems: 'center',
  },
  itemWrapper: {
    flex: '1 0 0',
    '&:last-child': {
      textAlign: 'right',
    },
  },
}));

export const Footer: FC = () => {
  const { t } = useTranslation();
  const classes = useStyles({});

  return (
    <Paper elevation={3} className={classes.wrapper}>
      <div className={classes.itemWrapper}>
        <Typography variant="body2">{t('golemioDescription')}</Typography>
      </div>
      <a href="https://operatorict.cz/" target="_blank" rel="noreferrer noopener">
        <img alt="OICT logo" src={OICTLogo} height={60} />
      </a>
      <div className={classes.itemWrapper} />
    </Paper>
  );
};
