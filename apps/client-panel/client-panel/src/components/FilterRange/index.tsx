import React, { ChangeEvent, useCallback, useMemo, useState, useEffect } from 'react';
import ContainerVertical from '../ContainerVertical';
import cls from './range.module.scss';
import { Slider, Typography } from '@material-ui/core';

interface FilterRangeProps {
  onChange: (min: number, max: number) => void;
  values: number[];
  title: string;
  min: number;
  max: number;
  unit: string;
  disabled: boolean;
}

const FilterRange = (props: FilterRangeProps) => {
  const { onChange, values, title, min, max, unit, disabled } = props;
  const [currentValues, setCurrentValues] = useState(values);

  useEffect(() => {
    setCurrentValues(values);
  }, [values]);

  const handleChange = useCallback(
    (_e: ChangeEvent<{}>, newValue: number | number[]) => {
      if (Array.isArray(newValue)) {
        onChange(newValue[0], newValue[1]);
      } else {
        onChange(newValue, newValue);
      }
    },
    [onChange],
  );

  const labelFormat = useCallback((value: number) => `${value}${unit}`, [unit]);
  const marks = useMemo(
    () => [
      { value: min, label: `${min}${unit}` },
      { value: max, label: `${max}${unit}` },
    ],
    [min, max, unit],
  );

  return (
    <ContainerVertical>
      <Typography variant="h4" component="h2">
        {title}
      </Typography>
      <div className={cls.sliderWrapper}>
        <Slider
          value={currentValues}
          disabled={disabled}
          max={max}
          min={min}
          marks={marks}
          onChangeCommitted={handleChange}
          valueLabelDisplay="auto"
          valueLabelFormat={labelFormat}
          onChange={(_e, f) => {
            setCurrentValues(f as number[]);
          }}
        />
      </div>
    </ContainerVertical>
  );
};

export default FilterRange;
