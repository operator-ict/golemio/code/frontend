import React, { ReactNode } from 'react';
import classnames from 'classnames';
import cls from './widget-table.module.scss';

interface WidgetTableProps {
  children?: ReactNode;
  className?: string;
  [prop: string]: unknown;
}

const WidgetTable = (props: WidgetTableProps) => {
  const { children = null, className = null, ...rest } = props;

  return (
    <table className={classnames(cls.wrapper, className)} {...rest}>
      {children}
    </table>
  );
};

export default WidgetTable;
