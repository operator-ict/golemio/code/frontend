export const getName = (name: string | undefined, surname: string | undefined, email: string) => {
  return [name, surname].filter((str) => !!str).join(' ') || email;
};
