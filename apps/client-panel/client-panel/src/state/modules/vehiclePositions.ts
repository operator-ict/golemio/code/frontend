import { createQueryStateObservable, fetch } from '@dataplatform/common-state';
import { notifications, NotificationType } from '@dataplatform/common-state/modules';
import * as io from 'io-ts';
import { autorun } from 'mobx';

export const vehiclePositions = createQueryStateObservable({
  dataCodec: io.any,
  paramsCodec: io.interface({ route: io.string }),
  fetch: ({ params: { route }, queryString }) =>
    fetch(`${process.env.REACT_APP_PROXY_API_URL}${route}${queryString}`),
});

autorun(() => {
  const error = vehiclePositions.error;
  if (error) {
    notifications.addNotification({ title: error.message, type: NotificationType.error });
  }
});
