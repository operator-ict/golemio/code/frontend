import { createQueryStateObservable, fetch } from '@dataplatform/common-state';
import * as io from 'io-ts';
import { SortedWasteMeasurementIO } from 'codecs/SortedWasteMeasurementIO';

export const createSortedWasteMeasurementsQueryStateObservable = () =>
  createQueryStateObservable({
    paramsCodec: io.interface({ route: io.string }),
    dataCodec: io.array(SortedWasteMeasurementIO),
    queryParamsCodec: io.interface({
      containerId: io.string,
      from: io.string,
    }),
    fetch: ({ params: { route }, queryString }) =>
      fetch(`${process.env.REACT_APP_PROXY_API_URL}${route}${queryString}`),
  });
