import { createQueryStateObservable, fetch } from '@dataplatform/common-state';
import { extendObservable, action, autorun } from 'mobx';
import * as io from 'io-ts';
import { UsersIO, UserIO } from 'codecs/users-iots';
import { auth } from '@dataplatform/common-state/modules';

type User = io.TypeOf<typeof UserIO>;

const usersQueryState = createQueryStateObservable({
  dataCodec: UsersIO,
  fetch: ({ queryString }) => fetch(`${process.env.REACT_APP_AUTH_API_URL}/users${queryString}`),
});

export const users = extendObservable(
  usersQueryState,
  {
    selectedUser: null as User | null,
    selectUser(userId: number) {
      this.selectedUser = this.data?.find((user) => user.id === userId) || null;
    },
  },
  { selectUser: action.bound },
);

autorun(() => {
  const { user } = auth;
  const { selectedUser, data } = users;

  if (!selectedUser && data && user) {
    users.selectUser(user.id);
  }
});
