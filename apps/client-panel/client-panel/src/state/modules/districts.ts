import { createQueryStateObservable, fetch } from '@dataplatform/common-state';
import { DistrictsIO } from 'codecs/DistrictsIO';
import { extendObservable, computed } from 'mobx';
import * as io from 'io-ts';

const districtsQueryStateObservable = createQueryStateObservable({
  dataCodec: DistrictsIO,
  paramsCodec: io.interface({ route: io.string }),
  fetch: ({ params: { route } }) => fetch(`${process.env.REACT_APP_PROXY_API_URL}${route}`),
});

export const districts = extendObservable(
  districtsQueryStateObservable,
  {
    get dictionary() {
      if (!districtsQueryStateObservable.data) {
        return {};
      }
      return districtsQueryStateObservable.data.features.reduce<{ [key: string]: string }>(
        (acc, item) => {
          const { name } = item.properties;
          const { slug } = item.properties;
          acc[slug] = name;
          return acc;
        },
        {},
      );
    },
  },
  {
    dictionary: computed.struct,
  },
);
