import { createQueryStateObservable, fetch } from '@dataplatform/common-state';
import { notifications, NotificationType } from '@dataplatform/common-state/modules';
import * as io from 'io-ts';
import { autorun } from 'mobx';
import { FeatureCollectionIO, PropertiesIO } from 'codecs';

export const universalGeojsonQueryStateObservableFactory = (propertiesCodec?: io.Mixed) => {
  const state = createQueryStateObservable({
    dataCodec: FeatureCollectionIO(propertiesCodec ?? PropertiesIO),
    paramsCodec: io.interface({ route: io.string }),
    fetch: ({ params: { route }, queryString }) =>
      fetch(`${process.env.REACT_APP_PROXY_API_URL}${route}${queryString}`),
  });

  autorun(() => {
    const error = state.error;
    if (error) {
      notifications.addNotification({ title: error.message, type: NotificationType.error });
    }
  });

  return state;
};
