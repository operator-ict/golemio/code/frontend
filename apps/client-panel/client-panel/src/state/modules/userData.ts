import { createQueryStateObservable, fetch } from '@dataplatform/common-state';
import { userDataIO } from 'codecs/userData-iots';
import { extendObservable, computed, autorun, action } from 'mobx';

const userDataQueryState = createQueryStateObservable({
  dataCodec: userDataIO,
  fetch: () => fetch(`${process.env.REACT_APP_API_URL}/user`),
});

export const userData = extendObservable(
  userDataQueryState,
  {
    updateUserData: createQueryStateObservable({
      bodyCodec: userDataIO,
      fetch: ({ body }) => fetch(`${process.env.REACT_APP_API_URL}/user`, { body, method: 'PUT' }),
    }),
    get favouriteTiles() {
      return userDataQueryState.data?.favouriteTiles || [];
    },
    addToFavourites(id: string) {
      if (this.error || this.updateUserData.isLoading || this.data?.favouriteTiles?.includes(id)) {
        return;
      }

      const newFavouriteTiles = [...(this.data?.favouriteTiles || []), id];

      this.updateUserData.fetch({
        body: {
          ...this.data,
          favouriteTiles: newFavouriteTiles,
        },
      });
    },
    removeFromFavourites(id: string) {
      if (this.error || this.updateUserData.isLoading || !this.data?.favouriteTiles?.includes(id)) {
        return;
      }

      this.updateUserData.fetch({
        body: {
          ...this.data,
          favouriteTiles: this.data.favouriteTiles.filter((tileId) => tileId !== id),
        },
      });
    },
  },
  {
    favouriteTiles: computed.struct,
    addToFavourites: action.bound,
    removeFromFavourites: action.bound,
  },
);

let firstRun = true;
autorun(() => {
  const { isLoading, error } = userData.updateUserData;

  if (firstRun) {
    firstRun = false;
    return;
  }

  if (!isLoading && !error) {
    userData.fetch({});
  }
});
