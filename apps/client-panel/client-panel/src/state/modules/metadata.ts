import { createQueryStateObservable, fetch } from '@dataplatform/common-state';
import { UnknownMetadataIO, MetadataBodyIO } from 'codecs';
import * as io from 'io-ts';
import { autorun, runInAction, extendObservable } from 'mobx';
import { auth, notifications, NotificationType } from '@dataplatform/common-state/modules';
import { users } from 'state/modules/users';

const currentUserMetadata = createQueryStateObservable({
  dataCodec: io.array(UnknownMetadataIO),
  paramsCodec: io.interface({ userId: io.number }),
  fetch: ({ params: { userId } }) =>
    fetch(`${process.env.REACT_APP_API_URL}/user/${userId}/metadata`),
});

export const metadata = extendObservable(currentUserMetadata, {
  allMetadata: createQueryStateObservable({
    dataCodec: io.array(UnknownMetadataIO),
    fetch: () => fetch(`${process.env.REACT_APP_API_URL}/metadata`),
  }),
  // currentUserMetadata cannot be reused for this, it has to be kept intact for routing
  otherUserMetadata: createQueryStateObservable({
    dataCodec: io.array(UnknownMetadataIO),
    paramsCodec: io.interface({ userId: io.number }),
    fetch: ({ params: { userId } }) =>
      fetch(`${process.env.REACT_APP_API_URL}/user/${userId}/metadata`),
  }),
  updateMetadata: createQueryStateObservable({
    bodyCodec: MetadataBodyIO(io.unknown),
    paramsCodec: io.partial({ id: io.string }),
    fetch: ({ body, params: { id } }) =>
      fetch(`${process.env.REACT_APP_API_URL}/metadata${id !== undefined ? '/' + id : ''}`, {
        method: id !== undefined ? 'PUT' : 'POST',
        body,
      }).then((response) => {
        if (response.ok) {
          notifications.addNotification({
            title: 'metadataSaveSuccessful',
          });
        } else {
          notifications.addNotification({ title: 'errorDuringSave', type: NotificationType.error });
        }

        return response;
      }),
  }),
  updateThumbnail: createQueryStateObservable({
    bodyCodec: io.unknown, // file
    paramsCodec: io.interface({ metadataId: io.string }),
    fetch: ({ body, params: { metadataId } }) =>
      fetch(`${process.env.REACT_APP_API_URL}/metadata/${metadataId}/thumbnail`, {
        body,
        method: 'PUT',
      }),
  }),
  deleteThumbnail: createQueryStateObservable({
    paramsCodec: io.interface({ metadataId: io.string }),
    fetch: ({ params: { metadataId } }) =>
      fetch(`${process.env.REACT_APP_API_URL}/metadata/${metadataId}/thumbnail`, {
        method: 'DELETE',
      }),
  }),
  updateAttachment: createQueryStateObservable({
    bodyCodec: io.unknown, // file
    paramsCodec: io.interface({ metadataId: io.string }),
    fetch: ({ body, params: { metadataId } }) =>
      fetch(`${process.env.REACT_APP_API_URL}/metadata/${metadataId}/attachment`, {
        body,
        method: 'PUT',
      }),
  }),
  attachment: createQueryStateObservable({
    shouldGetBlob: true,
    dataCodec: io.unknown,
    paramsCodec: io.interface({ metadataId: io.string }),
    fetch: ({ params: { metadataId } }) =>
      fetch(`${process.env.REACT_APP_API_URL}/metadata/${metadataId}/attachment`),
  }),
  deleteMetadata: createQueryStateObservable({
    paramsCodec: io.interface({ metadataId: io.string }),
    fetch: ({ params: { metadataId } }) =>
      fetch(`${process.env.REACT_APP_API_URL}/metadata/${metadataId}`, { method: 'DELETE' }).then(
        (response) => {
          if (!response.ok) {
            notifications.addNotification({
              title: 'errorDuringDelete',
              type: NotificationType.error,
            });
            return response;
          }

          metadata.fetch({ params: { userId: auth.user.id } });
          metadata.allMetadata.fetch({});

          if (users.selectedUser) {
            metadata.otherUserMetadata.fetch({ params: { userId: users.selectedUser.id } });
          }

          notifications.addNotification({
            title: 'metadataDeleteSuccessful',
          });

          return response;
        },
      ),
  }),
  getSelectedUserValue(key: 'data' | 'isLoading' | 'metadata') {
    const { selectedUser } = users;
    const { user } = auth;
    const isAdmin = user?.admin;
    const userId = user?.id;

    if (!isAdmin || !selectedUser || selectedUser.id === userId) {
      return currentUserMetadata[key];
    } else {
      return this.otherUserMetadata[key];
    }
  },
  refresh() {
    metadata.fetch({ params: { userId: auth.user.id } });
    metadata.allMetadata.fetch({});

    if (users.selectedUser) {
      metadata.otherUserMetadata.fetch({ params: { userId: users.selectedUser.id } });
    }
  },
  get selectedUserMetadata() {
    return this.getSelectedUserValue('data');
  },
  get isSelectedUserMetadataLoading() {
    return this.getSelectedUserValue('isLoading');
  },
  get selectedUserMetadataError() {
    return this.getSelectedUserValue('error');
  },
});

autorun(() => {
  if (!auth.user) {
    runInAction(() => {
      metadata.data = null;
      metadata.error = null;
    });
  }
});

autorun(() => {
  if (metadata.error) {
    auth.signOut();
  }
});

autorun(() => {
  const { isLoading, error } = metadata.updateThumbnail;

  if (!isLoading && error) {
    notifications.addNotification({
      title: 'thumbnailSaveError',
      type: NotificationType.error,
    });
  }
});

autorun(() => {
  const { selectedUser } = users;
  const { user } = auth;

  if (selectedUser && selectedUser.id !== user?.id) {
    metadata.otherUserMetadata.fetch({ params: { userId: selectedUser.id } });
  }
});
