import { action, extendObservable } from 'mobx';
import { createQueryStateObservable, fetch } from '@dataplatform/common-state';
import * as io from 'io-ts';
import { MetadataType } from 'codecs';
import { TagIO } from 'codecs/tag-iots';

const tagQueryStateObservable = createQueryStateObservable({
  dataCodec: io.array(TagIO),
  paramsCodec: io.interface({ userId: io.number }),
  fetch: ({ params: { userId } }) => fetch(`${process.env.REACT_APP_API_URL}/user/${userId}/tag`),
});

export const dashboardFilters = extendObservable(
  tagQueryStateObservable,
  {
    selectedTags: [] as string[],
    toggleTagSelection(tagId: string) {
      this.search = '';
      const tagIndex = this.selectedTags.indexOf(tagId);
      if (tagIndex !== -1) {
        this.selectedTags = [
          ...this.selectedTags.slice(0, tagIndex),
          ...this.selectedTags.slice(tagIndex + 1),
        ];
      } else {
        this.selectedTags = [...this.selectedTags, tagId];
      }
    },
    category: null as MetadataType,
    setCategory(category: MetadataType) {
      this.category = category;
    },
    search: '',
    setSearch(search: string) {
      this.selectedTags = [];
      this.category = null;
      this.search = search;
    },
  },
  { toggleTagSelection: action.bound, setCategory: action.bound, setSearch: action.bound },
);
