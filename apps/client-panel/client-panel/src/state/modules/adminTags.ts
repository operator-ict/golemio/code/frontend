import { createQueryStateObservable, fetch } from '@dataplatform/common-state';
import * as io from 'io-ts';
import { TagIO } from 'codecs/tag-iots';
import { extendObservable } from 'mobx';
import { metadata } from 'state/modules/metadata';
import { users } from 'state/modules/users';
import { auth } from 'libs/common-state/src/modules/auth';
import { dashboardFilters } from 'state/modules/dashboardFilters';

const adminTagsQueryState = createQueryStateObservable({
  dataCodec: io.array(TagIO),
  fetch: () => fetch(`${process.env.REACT_APP_API_URL}/tag`),
});

export const adminTags = extendObservable(adminTagsQueryState, {
  deleteTag: createQueryStateObservable({
    paramsCodec: io.interface({ tagId: io.string }),
    fetch: ({ params: { tagId } }) =>
      fetch(`${process.env.REACT_APP_API_URL}/tag/${tagId}`, { method: 'DELETE' }).then(
        (response) => {
          if (response.ok) {
            metadata.fetch({ params: { userId: auth.user.id } });
            metadata.allMetadata.fetch({});
            dashboardFilters.fetch({ params: { userId: users.selectedUser?.id ?? auth.user.id } });
            adminTags.fetch({});

            if (users.selectedUser && metadata.otherUserMetadata.data) {
              metadata.otherUserMetadata.fetch({ params: { userId: users.selectedUser.id } });
            }
          }

          return response;
        },
      ),
  }),
});

export const createTagStateObservableFactory = () => {
  const state = createQueryStateObservable({
    dataCodec: TagIO,
    bodyCodec: io.interface({ title: io.string }),
    fetch: ({ body }) =>
      fetch(`${process.env.REACT_APP_API_URL}/tag`, { method: 'POST', body }).then((response) => {
        if (response.ok) {
          adminTags.fetch({});
        }

        return response;
      }),
  });

  return state;
};
