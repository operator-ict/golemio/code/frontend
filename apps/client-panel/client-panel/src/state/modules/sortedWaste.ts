import { createQueryStateObservable, fetch } from '@dataplatform/common-state';
// import { SortedWasteIO } from 'codecs';
import { extendObservable, autorun } from 'mobx';
import { notifications, NotificationType } from '@dataplatform/common-state/modules';
import * as io from 'io-ts';

const sortedWasteQueryStateObservable = createQueryStateObservable({
  dataCodec: io.any, // SortedWasteIO,
  paramsCodec: io.interface({ route: io.string }),
  fetch: ({ params: { route }, queryString }) =>
    fetch(`${process.env.REACT_APP_PROXY_API_URL}${route}${queryString}`),
});

export const sortedWaste = extendObservable(sortedWasteQueryStateObservable, {
  get districtList() {
    if (!sortedWasteQueryStateObservable.data) {
      return [];
    }
    return sortedWasteQueryStateObservable.data.features
      .reduce((districts, item) => {
        const { district } = item.properties;
        if (district && !districts.includes(district)) {
          districts.push(district);
        }
        return districts;
      }, [] as string[])
      .sort();
  },
});

autorun(() => {
  const error = sortedWaste.error;
  if (error) {
    notifications.addNotification({ title: error.message, type: NotificationType.error });
  }
});
