import { createQueryStateObservable, fetch } from '@dataplatform/common-state';
import * as io from 'io-ts';
import { bodyCodec, metaCodec } from 'codecs/DataExportIO';

const replaceUrlParameter = (baseUrl: string, path: string, value: string) => {
  return baseUrl + path.replace(/(:[^\/]*)/, value);
};

export const createDataExportStateObservable = (route: string) => {
  const baseUrl = process.env.REACT_APP_PROXY_API_URL;

  return {
    data: createQueryStateObservable({
      dataCodec: io.unknown,
      bodyCodec,
      shouldGetBlob: true,
      fetch: ({ body }) =>
        fetch(replaceUrlParameter(baseUrl, route, 'data'), { method: 'POST', body }),
    }),
    exportMetadata: createQueryStateObservable({
      dataCodec: metaCodec,
      fetch: () => fetch(replaceUrlParameter(baseUrl, route, 'meta')),
    }),
    preview: createQueryStateObservable({
      dataCodec: io.string,
      bodyCodec,
      fetch: ({ body }) =>
        fetch(replaceUrlParameter(baseUrl, route, 'preview'), { method: 'POST', body }),
    }),
  };
};
