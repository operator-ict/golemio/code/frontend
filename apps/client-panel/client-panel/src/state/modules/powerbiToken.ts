import { createQueryStateObservable, fetch } from '@dataplatform/common-state';
import * as io from 'io-ts';
import { PowerBITokenIO } from 'codecs/powerBIToken-iots';

export const createPowerBiTokenState = () =>
  createQueryStateObservable({
    paramsCodec: io.interface({ metadataId: io.string }),
    dataCodec: PowerBITokenIO,
    fetch: ({ params: { metadataId } }) =>
      fetch(`${process.env.REACT_APP_API_URL}/metadata/${metadataId}/powerbi-embed-token`),
  });
