import { createQueryStateObservable, fetch } from '@dataplatform/common-state';
import * as io from 'io-ts';
import { notifications, NotificationType } from '@dataplatform/common-state/modules';
import { extendObservable, runInAction } from 'mobx';

export const createAccessRequestState = () => {
  const state = extendObservable(
    createQueryStateObservable({
      fetch: ({ body }) =>
        fetch(`${process.env.REACT_APP_API_URL}/accessrequest`, {
          body,
          method: 'POST',
        }).then((response) => {
          if (response.ok) {
            onSuccess();
          } else {
            notifications.addNotification({
              title: 'accessRequestError',
              type: NotificationType.error,
            });
          }

          return response;
        }),
      bodyCodec: io.interface({
        name: io.string,
        surname: io.string,
        organization: io.string,
        email: io.string,
        'g-recaptcha-response': io.string,
      }),
    }),
    {
      wasSuccessful: false,
    },
  );

  const onSuccess = () => {
    runInAction(() => {
      state.wasSuccessful = true;
    });
  };

  return state;
};
