import { configure, observable } from 'mobx';
import { notifications, auth, settings } from '@dataplatform/common-state/modules';
import {
  districts,
  sortedWaste,
  vehiclePositions,
  metadata,
  universalGeojsonQueryStateObservableFactory,
  dashboardFilters,
  users,
  adminTags,
  createPowerBiTokenState,
  userData,
} from 'state/modules';

configure({ enforceActions: 'always' });

export const appState = observable({
  sortedWaste,
  districts,
  settings,
  notifications,
  auth,
  vehiclePositions,
  metadata,
  dashboardFilters,
  universalGeojsonQueryStateObservableFactory,
  users,
  adminTags,
  createPowerBiTokenState,
  userData,
});
