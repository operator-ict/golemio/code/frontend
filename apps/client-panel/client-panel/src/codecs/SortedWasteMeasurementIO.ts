import * as io from 'io-ts';
import { maybe } from '@dataplatform/common-state';

export const SortedWasteMeasurementIO = io.interface({
  battery_status: maybe(io.number),
  firealarm: maybe(io.number),
  id: io.string,
  measured_at_utc: io.string,
  percent_calculated: maybe(io.number),
  prediction_utc: maybe(io.string),
  temperature: maybe(io.number),
  updated_at: maybe(io.number),
  upturned: maybe(io.number),
});
