import * as io from 'io-ts';
import { FeatureCollectionIO } from 'codecs';

const DistrictPropertiesIO = io.interface({
  id: io.number,
  name: io.string,
  slug: io.string,
  updated_at: io.string,
});

export const DistrictsIO = FeatureCollectionIO(DistrictPropertiesIO);
