import * as io from 'io-ts';
import { maybe } from '@dataplatform/common-state';

export const UserIO = io.interface({
  created_at: io.string,
  email: io.string,
  id: io.number,
  invitation_accepted_at: maybe(io.string),
  name: maybe(io.string),
  roleIds: io.array(io.number),
  surname: maybe(io.string),
  updated_at: io.string,
});

export const UsersIO = io.array(UserIO);
