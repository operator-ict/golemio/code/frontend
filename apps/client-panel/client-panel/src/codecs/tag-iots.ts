import * as io from 'io-ts';

export const TagIO = io.interface({
  title: io.string,
  _id: io.string,
});

export type Tag = io.TypeOf<typeof TagIO>;
