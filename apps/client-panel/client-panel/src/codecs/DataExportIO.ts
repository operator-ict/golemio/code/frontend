import * as io from 'io-ts';
import { MetadataIO } from 'codecs/metadata-iots';
import { maybe } from '@dataplatform/common-state';

export const builderQueryCodec = io.unknown;

export const directionCodec = io.string; // io.union([io.literal('asc'), io.literal('desc')]);

export const bodyCodec = io.intersection([
  io.interface({
    columns: io.array(io.string),
    builderQuery: builderQueryCodec,
  }),
  io.partial({
    groupBy: io.array(io.string),
    order: io.array(io.interface({ direction: directionCodec, column: io.string })),
    offset: io.number,
    limit: io.number,
  }),
]);

export const metaCodec = io.array(
  io.intersection([
    io.interface({
      label: io.string,
      name: io.string,
      valueEditorType: io.union([
        io.literal('text'),
        io.literal('select'),
        io.literal('checkbox'),
        io.literal('radio'),
      ]),
    }),
    io.partial({
      inputType: io.string, // text | number | ...
    }),
  ]),
);

export type DataExportRequestBody = io.TypeOf<typeof bodyCodec>;

export const DataExportMetadataIO = MetadataIO(
  maybe(
    io.partial({
      columnDescription: io.string,
      limit: io.interface({
        column: io.string,
        value: io.number,
        unit: io.union([
          io.literal('days'),
          io.literal('weeks'),
          io.literal('months'),
          io.literal('years'),
        ]),
      }),
    }),
  ),
);

export type DataExportMetadata = io.TypeOf<typeof DataExportMetadataIO>;
