import * as io from 'io-ts';
import { FeatureCollectionIO, FeatureIO } from 'codecs/geojson-iots';
import { maybe } from '@dataplatform/common-state';

const SortedWasteCleaningFrequencyIO = io.partial(
  {
    id: io.number,
    duration: io.string,
    frequency: io.number,
  },
  'SortedWasteCleaningFrequency',
);

const SortedWasteCompanyIO = io.partial(
  {
    email: io.string,
    name: io.string,
    phone: io.string,
    web: io.string,
  },
  'SortedWasteCompany',
);

const SortedWasteTrashTypeIO = io.interface(
  {
    description: io.string,
    id: io.number,
  },
  'SortedWasteTrashType',
);

const SortedWasteLastMeasurementIO = io.interface(
  {
    measured_at_utc: io.string,
    percent_calculated: io.number,
    prediction_utc: io.string,
  },
  'SortedWasteLastMeasurement',
);

const SortedWasteLastPickIO = io.interface(
  {
    pick_at_utc: io.string,
  },
  'SortedWasteLastPick',
);

const SortedWasteContainerIO = io.partial({
  cleaning_frequency: SortedWasteCleaningFrequencyIO,
  company: SortedWasteCompanyIO,
  description: maybe(io.string),
  sensor_code: io.string,
  sensor_supplier: io.string,
  container_type: maybe(io.string),
  trash_type: SortedWasteTrashTypeIO,
  last_measurement: maybe(SortedWasteLastMeasurementIO),
  last_pick: maybe(SortedWasteLastPickIO),
  knsko_id: io.string,
  container_id: io.string,
});

const SortedWasteAccessibilityIO = io.interface({
  description: io.string,
  id: io.number,
});

export const SortedWastePropertiesIO = io.intersection([
  io.interface({
    id: io.number,
    name: io.string,
    updated_at: io.number,
  }),
  io.partial({
    accessibility: SortedWasteAccessibilityIO,
    containers: io.array(SortedWasteContainerIO),
    station_number: io.string,
    district: maybe(io.string),
  }),
]);

export const SortedWasteIO = FeatureCollectionIO(SortedWastePropertiesIO);
export const SortedWasteFeatureIO = FeatureIO(SortedWastePropertiesIO);

export type SortedWasteFeature = io.TypeOf<typeof SortedWasteFeatureIO>;
export type SortedWasteCollection = io.TypeOf<typeof SortedWasteIO>;
export type SortedWasteProperties = io.TypeOf<typeof SortedWastePropertiesIO>;
export type SortedWasteContainer = io.TypeOf<typeof SortedWasteContainerIO>;
