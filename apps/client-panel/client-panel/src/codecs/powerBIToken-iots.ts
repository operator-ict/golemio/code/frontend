import * as io from 'io-ts';

export const PowerBITokenIO = io.interface({
  accessToken: io.string,
  embedUrl: io.array(
    io.interface({
      reportId: io.string,
      reportName: io.string,
      embedUrl: io.string,
    }),
  ),
  expiry: io.string,
  status: io.number,
});
