import * as io from 'io-ts';
import { FeatureCollectionIO } from 'codecs/geojson-iots';
import { AddressIO } from 'codecs/shared-iots';
import { maybe } from '@dataplatform/common-state';

/*interface HourBrand {
  readonly Hour: unique symbol;
}

const HourIO = io.brand(
  io.string,
  (s): s is io.Branded<string, HourBrand> => /([01][0-9]|2[0-3])/.test(s),
  'Hour',
);

interface WeekDayBrand {
  readonly WeekDay: unique symbol;
}

const WeekDayIO = io.brand(
  io.string,
  (s): s is io.Branded<string, WeekDayBrand> => /([0-6])/.test(s),
  'WeekDay',
);*/

// [00 - 23]: io.number
// const ParkingOccupancyDayIO = io.record(HourIO, maybe(io.number));
const ParkingOccupancyDayIO = io.record(io.string, maybe(io.number));

// [0 - 6]: ParkingOccupancyDayIO,
// const ParkingOccupancyIO = io.record(WeekDayIO, ParkingOccupancyDayIO);
const ParkingOccupancyIO = io.record(io.string, ParkingOccupancyDayIO);

const ParkingTypesIO = io.intersection([
  io.interface({
    id: io.number,
  }),
  io.partial({
    description: io.string,
  }),
]);

export const ParkingPropertiesIO = io.intersection([
  io.interface({
    id: io.number,
    name: io.string,
    num_of_free_places: io.number,
    num_of_taken_places: io.number,
    updated_at: io.number,
    total_num_of_places: io.number,
  }),
  io.partial({
    parking_type: ParkingTypesIO,
    average_occupancy: ParkingOccupancyIO,
    district: io.string,
    address: AddressIO,
    last_updated: io.number,
  }),
]);

export const ParkingIO = FeatureCollectionIO(ParkingPropertiesIO);

export type ParkingCollection = io.TypeOf<typeof ParkingIO>;
export type ParkingProperties = io.TypeOf<typeof ParkingPropertiesIO>;
