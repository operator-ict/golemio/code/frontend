import * as io from 'io-ts';

const MetadataTypeIO = io.union([
  io.literal('dashboard'),
  io.literal('application'),
  io.literal('export'),
  io.literal('analysis'),
]);

export type MetadataType = io.TypeOf<typeof MetadataTypeIO>;

const MetadataCommonIO = <T extends unknown>(dataType: io.Type<T>) =>
  io.interface({
    type: MetadataTypeIO,
    component: io.string,
    route: io.string,
    client_route: io.string,
    title: io.string,
    description: io.string,
    data: dataType,
  });

export const MetadataBodyIO = <T extends unknown>(dataType: io.Type<T>) =>
  io.intersection([MetadataCommonIO(dataType), io.interface({ tags: io.array(io.string) })]);

export const MetadataIO = <T extends unknown>(dataType: io.Type<T>) =>
  io.intersection([
    MetadataCommonIO(dataType),
    io.interface({
      _id: io.string,
      tags: io.array(
        io.interface({
          _id: io.string,
          title: io.string,
        }),
      ),
    }),
    io.partial({
      fa_icon: io.string,
      thumbnail: io.interface({
        content_type: io.string,
        url: io.string,
      }),
      attachment: io.interface({
        id: io.string,
        title: io.string,
        name: io.string,
        content_type: io.string,
        size: io.number,
        url: io.string,
      }),
    }),
  ]);

export const UnknownMetadataIO = MetadataIO(io.unknown);

export type UnknownMetadata = io.TypeOf<typeof UnknownMetadataIO>;
