import React, { FC, useMemo, useEffect, useCallback, useRef } from 'react';
import { AppLayout } from 'components/AppLayout';
import { Footer } from 'components/Footer';
import { useParams, Redirect, useHistory } from 'react-router';
import { observer } from 'mobx-react-lite';
import { appState } from 'state';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { useTranslation } from 'react-i18next';
import {
  TextField,
  CircularProgress,
  Box,
  makeStyles,
  MenuItem,
  Button,
  FormLabel,
  Typography,
  FormHelperText,
} from '@material-ui/core';
import { DashboardHeader } from '../components/DashboardHeader';
import { useMetadataTypeData } from 'hooks/metadataTypeData';
import { useComponents } from '../components/DataComponents';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { createTagStateObservableFactory } from 'state/modules';
import AddIcon from '@material-ui/icons/AddCircle';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import { UnknownMetadata, MetadataType } from 'codecs';

const THUMBNAIL_RATIO_WIDTH = 333;
const THUMBNAIL_RATIO_HEIGHT = 74;

const DEFAULT_VALUES = {
  description: '',
  title: '',
  route: '',
  client_route: '',
  type: '',
  component: '',
  tags: [] as string[],
  data: '',
  thumbnail: { file: null as File | null, image: null as string | null },
  attachment: { file: null as File | null, title: '' },
};

const useStyles = makeStyles((theme) => ({
  inputContainer: {
    width: '100%',
    display: 'flex',
    flexWrap: 'wrap',
    padding: theme.spacing(2),
    '& > *': {
      margin: theme.spacing(2),
      flex: '1 0 34%',
      minWidth: 250,
    },
  },
  thumbnail: {
    height: 74,
  },
  deleteButton: {
    marginLeft: theme.spacing(1),
  },
}));

export const EditPage: FC = observer(() => {
  const { id } = useParams();
  const { refresh } = appState.metadata;
  const { data, isLoading, fetch: fetchAllMetadata } = appState.metadata.allMetadata;
  const { fetch: updateMetadata, isLoading: isUpdatingMetadata } = appState.metadata.updateMetadata;
  const {
    fetch: updateThumbnail,
    isLoading: isUpdatingThumbnail,
  } = appState.metadata.updateThumbnail;
  const {
    fetch: deleteThumbnail,
    isLoading: isDeletingThumbnail,
  } = appState.metadata.deleteThumbnail;
  const {
    fetch: updateAttachment,
    isLoading: isUpdatingAttachment,
  } = appState.metadata.updateAttachment;
  const { t } = useTranslation();
  const classes = useStyles();
  const { metadataTypeData } = useMetadataTypeData();
  const { components } = useComponents();
  const {
    data: adminTags,
    isLoading: isAdminTagsLoading,
    fetch: fetchAdminTags,
  } = appState.adminTags;
  const history = useHistory();
  const tagInputRef = useRef<HTMLElement | null>(null);

  const editing = !!id;

  useEffect(() => {
    if (editing && !data && !isLoading) {
      fetchAllMetadata({});
    }
  }, [editing]);

  useEffect(() => {
    if (!adminTags && !isAdminTagsLoading) {
      fetchAdminTags({});
    }
  }, []);

  const editedMetadata = useMemo(() => id && data?.find((metadata) => metadata._id === id), [
    data,
    id,
  ]);

  const parseType = useCallback(
    (type: string) => {
      return metadataTypeData.find((typeData) => typeData.type === type)?.type || '';
    },
    [metadataTypeData],
  );

  const isSendingForm =
    isUpdatingMetadata || isUpdatingThumbnail || isDeletingThumbnail || isUpdatingAttachment;

  if (editing && !!data && !editedMetadata) {
    return <Redirect to="/dashboard" />;
  }

  return (
    <AppLayout
      header={
        <DashboardHeader
          title={
            editing
              ? t('metadataEdit', { name: editedMetadata?.title || '' })
              : t('newMetadataCreation')
          }
        />
      }
      footer={<Footer />}
    >
      <>
        {isLoading ? (
          <Box textAlign="center">
            <CircularProgress />
          </Box>
        ) : (
          <>
            <Formik
              initialValues={{
                ...(editedMetadata || DEFAULT_VALUES),
                type: parseType(editedMetadata?.type || DEFAULT_VALUES.type),
                tags: editedMetadata?.tags?.map((tag) => tag.title) || DEFAULT_VALUES.tags,
                data: !!editedMetadata?.data
                  ? JSON.stringify(editedMetadata.data, null, 2)
                  : DEFAULT_VALUES.data,
                thumbnail: editedMetadata?.thumbnail
                  ? {
                      file: null,
                      image: `${process.env.REACT_APP_API_URL}${editedMetadata.thumbnail.url}`,
                    }
                  : DEFAULT_VALUES.thumbnail,
                attachment: editedMetadata?.attachment
                  ? {
                      file: null,
                      title: editedMetadata.attachment.title,
                    }
                  : DEFAULT_VALUES.attachment,
              }}
              onSubmit={(formData) => {
                if (!isSendingForm) {
                  const tagsAndPromises = formData.tags.map((formTag) => {
                    const existingTag = adminTags.find(
                      (tag) => formTag.toLocaleLowerCase() === tag.title.toLocaleLowerCase(),
                    );

                    return (
                      existingTag ??
                      createTagStateObservableFactory().fetch({
                        body: { title: formTag },
                      })
                    );
                  });

                  Promise.all(tagsAndPromises)
                    .then((tags) =>
                      updateMetadata({
                        body: {
                          client_route: formData.client_route,
                          component: formData.component,
                          description: formData.description,
                          route: formData.route,
                          title: formData.title,
                          type: formData.type as MetadataType,
                          data: formData.data ? JSON.parse(formData.data) : null,
                          tags: tags.map((tag) => tag._id),
                        },
                        params: { id },
                      }),
                    )
                    .then((data: UnknownMetadata | undefined) => {
                      const promises: Promise<unknown>[] = [];

                      if (editing || data) {
                        const dataId = editing ? id : data._id;

                        if (!editing) {
                          history.replace(`/dashboard/edit/${dataId}`);
                        }

                        if (formData.thumbnail.file) {
                          const fileData = new FormData();
                          fileData.append('file', formData.thumbnail.file);

                          promises.push(
                            updateThumbnail({
                              body: fileData,
                              params: { metadataId: dataId },
                            }),
                          );
                        } else if (editedMetadata.thumbnail) {
                          promises.push(deleteThumbnail({ params: { metadataId: dataId } }));
                        }

                        if (formData.attachment.file) {
                          const fileData = new FormData();
                          fileData.append('attachmentFile', formData.attachment.file);
                          fileData.append('attachmentTitle', formData.attachment.title);

                          promises.push(
                            updateAttachment({
                              body: fileData,
                              params: { metadataId: dataId },
                            }),
                          );
                        }
                      }
                      return Promise.all(promises);
                    })
                    .then(() => {
                      refresh();
                    });
                }
              }}
              validationSchema={Yup.object().shape({
                description: Yup.string(),
                title: Yup.string().required(),
                route: Yup.string().required(),
                client_route: Yup.string().required(),
                type: Yup.string().required(),
                data: Yup.string().test('json', t('invalidJSON'), (value) => {
                  if (value) {
                    try {
                      JSON.parse(value);
                    } catch {
                      return false;
                    }
                  }

                  return true;
                }),
                component: Yup.string().required(),
                thumbnail: Yup.object().test('thumbnail validation', 'incorrect ratio', (value) => {
                  if (value?.image) {
                    const image = new Image();
                    image.src = value.image;

                    if (
                      (image.height * THUMBNAIL_RATIO_WIDTH) / THUMBNAIL_RATIO_HEIGHT !==
                      image.width
                    ) {
                      return false;
                    }
                  }

                  return true;
                }),
              })}
            >
              {({
                handleBlur,
                handleChange,
                handleSubmit,
                values,
                touched,
                errors,
                setFieldTouched,
                setFieldValue,
              }) => (
                <>
                  <form
                    noValidate={true}
                    onSubmit={handleSubmit}
                    onKeyPress={(e) => {
                      if (
                        e.key === 'Enter' &&
                        tagInputRef.current?.contains(document.activeElement)
                      ) {
                        e.preventDefault();
                      }
                    }}
                  >
                    <Box className={classes.inputContainer}>
                      <TextField
                        type="text"
                        variant="outlined"
                        name="title"
                        label={t('title')}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={values.title}
                        error={touched.title && !!errors.title}
                      />
                      <TextField
                        type="text"
                        multiline={true}
                        variant="outlined"
                        name="description"
                        label={t('description')}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={values.description}
                        error={touched.description && !!errors.description}
                      />
                      <TextField
                        type="text"
                        variant="outlined"
                        name="route"
                        label={t('route')}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={values.route}
                        error={touched.route && !!errors.route}
                      />
                      <TextField
                        type="text"
                        variant="outlined"
                        name="client_route"
                        label={t('client_route')}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={values.client_route}
                        error={touched.client_route && !!errors.client_route}
                      />
                      <TextField
                        label={t('type')}
                        name="type"
                        value={values.type}
                        variant="outlined"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        error={touched.type && !!errors.type}
                        select={true}
                      >
                        {metadataTypeData
                          .filter((typeData) => !!typeData.type)
                          .map((typeData) => (
                            <MenuItem key={typeData.type} value={typeData.type}>
                              {typeData.title}
                            </MenuItem>
                          ))}
                      </TextField>
                      <TextField
                        label={t('component')}
                        name="component"
                        value={values.component}
                        variant="outlined"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        error={touched.component && !!errors.component}
                        select={true}
                      >
                        {Object.keys(components).map((component) => (
                          <MenuItem key={component} value={component}>
                            {components[component].title}
                          </MenuItem>
                        ))}
                      </TextField>
                      <Autocomplete
                        disabled={!adminTags}
                        multiple={true}
                        options={adminTags?.map((tag) => tag.title)}
                        freeSolo={true}
                        onChange={(_, v) => {
                          setFieldValue('tags', v);
                          setFieldTouched('tags', true);
                        }}
                        onBlur={() => setFieldTouched('tags', true)}
                        value={values.tags}
                        disableCloseOnSelect={false}
                        innerRef={tagInputRef}
                        renderInput={(params) => (
                          <TextField
                            label={t('tags')}
                            {...params}
                            error={touched.tags && !!errors.tags}
                            fullWidth={true}
                            variant="outlined"
                            placeholder={t('chooseOrCreateTags')}
                          />
                        )}
                      />
                      <TextField
                        type="data"
                        multiline={true}
                        variant="outlined"
                        name="data"
                        label={t('data')}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={values.data}
                        error={touched.data && !!errors.data}
                      />
                      <div>
                        <FormLabel>{t('thumbnail')}</FormLabel>
                        {!values.thumbnail?.image && <Typography>{t('noThumbnail')}</Typography>}
                        <div>
                          {!!values.thumbnail?.image && (
                            <img src={values.thumbnail.image} className={classes.thumbnail} />
                          )}
                          <div>
                            <FormHelperText error={!!errors.thumbnail}>
                              {t('thumbnailRatioHelperText', {
                                ratio: `${THUMBNAIL_RATIO_WIDTH}:${THUMBNAIL_RATIO_HEIGHT}`,
                              })}
                            </FormHelperText>
                          </div>
                        </div>
                        <Button
                          startIcon={values.thumbnail.image ? <EditIcon /> : <AddIcon />}
                          variant="contained"
                          component="label"
                        >
                          {values.thumbnail.image ? t('change') : t('add')}
                          <input
                            name="thumbnail"
                            type="file"
                            onChange={({ currentTarget }) => {
                              const file = currentTarget.files?.[0];

                              if (!file) {
                                return;
                              }

                              const fileReader = new FileReader();
                              fileReader.onload = () => {
                                setFieldValue('thumbnail', {
                                  file,
                                  image: fileReader.result,
                                });
                              };
                              fileReader.readAsDataURL(file);
                            }}
                            onBlur={handleBlur}
                            hidden
                          />
                        </Button>
                        {!!values.thumbnail.image && (
                          <Button
                            className={classes.deleteButton}
                            startIcon={<DeleteIcon />}
                            variant="contained"
                            onClick={() => setFieldValue('thumbnail', DEFAULT_VALUES.thumbnail)}
                          >
                            {t('delete')}
                          </Button>
                        )}
                      </div>
                      {(values.component === 'attachmentComponent' || values.attachment.title) && (
                        <div>
                          <FormLabel>{t('attachment')}</FormLabel>
                          <Typography>{values.attachment.title || t('noAttachment')}</Typography>
                          <Button
                            startIcon={values.attachment.title ? <EditIcon /> : <AddIcon />}
                            variant="contained"
                            component="label"
                          >
                            {values.attachment.title ? t('change') : t('add')}
                            <input
                              name="attachment"
                              type="file"
                              onChange={({ currentTarget }) => {
                                const file = currentTarget.files?.[0];

                                if (!file) {
                                  return;
                                }

                                setFieldValue('attachment', {
                                  file,
                                  title: file.name,
                                });
                              }}
                              onBlur={handleBlur}
                              hidden
                            />
                          </Button>
                        </div>
                      )}
                    </Box>
                    <Box textAlign="center">
                      <Button
                        disabled={isSendingForm}
                        variant="contained"
                        color="primary"
                        type="submit"
                      >
                        {isSendingForm ? <CircularProgress size={20} /> : t('save')}
                      </Button>
                    </Box>
                  </form>
                </>
              )}
            </Formik>
          </>
        )}
      </>
    </AppLayout>
  );
});
