import React from 'react';
import { AppLayout } from 'components/AppLayout';
import { FiltersBar } from 'components/FiltersBar';
import { Tiles } from 'components/Tiles';
import { Footer } from 'components/Footer';

export const Home = () => {
  return (
    <AppLayout footer={<Footer />}>
      <FiltersBar />
      <Tiles />
    </AppLayout>
  );
};
