import React, { useEffect, FC, useContext, useMemo } from 'react';
import { SortedWasteTopBar } from './components/TopBar';
import { SortedWasteDetail } from './components/Detail';
import { Map } from 'components/Map';
import { observer } from 'mobx-react-lite';
import { appState } from 'state';
import { MapWrapper } from 'components/MapComponents';
import { sortedWasteIcon } from 'components/MapIcons';
import { MetadataIO } from 'codecs';
import * as io from 'io-ts';
import { AppLayout } from 'components/AppLayout';
import { DashboardHeader } from 'pages/Dashboard/components/DashboardHeader';
import { SortedWasteFiltersProvider, SortedWasteFiltersContext } from './sortedWasteFiltersContext';

export const SortedWasteMapMetadataIO = MetadataIO(
  io.interface({
    districtsRoute: io.string,
    measurementsRoute: io.string,
  }),
);

interface SortedWasteMapProps {
  metadata: io.TypeOf<typeof SortedWasteMapMetadataIO>;
}

const SortedWasteMapInner = observer((props: SortedWasteMapProps) => {
  const { sortedWaste, districts } = appState;
  const { data } = sortedWaste;
  const { filterFunction } = useContext(SortedWasteFiltersContext);
  const { metadata } = props;

  const filteredData = useMemo(() => {
    return filterFunction(data);
  }, [data, filterFunction]);

  useEffect(() => {
    sortedWaste.fetch({ params: { route: metadata.route } });
    districts.fetch({ params: { route: metadata.data.districtsRoute } });
  }, []);

  return (
    <AppLayout
      header={
        <DashboardHeader
          title={metadata.title}
          customTopBar={<SortedWasteTopBar stationsCount={filteredData?.features?.length || 0} />}
        />
      }
    >
      <MapWrapper isError={!!sortedWaste.error} isLoading={sortedWaste.isLoading}>
        {!!filteredData?.features && (
          <Map
            data={filteredData}
            customIcons={{
              images: [['sorted-waste', sortedWasteIcon]],
              markerIcon: 'sorted-waste',
            }}
            RenderFeatureProp={({ properties }) => {
              return (
                <SortedWasteDetail
                  properties={properties}
                  measurementsRoute={metadata.data.measurementsRoute}
                />
              );
            }}
          />
        )}
      </MapWrapper>
    </AppLayout>
  );
});

export const SortedWasteMap: FC<SortedWasteMapProps> = (props) => {
  return (
    <SortedWasteFiltersProvider>
      <SortedWasteMapInner {...props} />
    </SortedWasteFiltersProvider>
  );
};
