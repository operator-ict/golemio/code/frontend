import React, { FC } from 'react';
import { Button } from '@material-ui/core';
import { ActionWithRulesProps } from 'react-querybuilder';
import { useCommonQueryStyles } from './commonQueryBuilderStyles';
import DeleteIcon from '@material-ui/icons/DeleteOutline';
import AddIcon from '@material-ui/icons/Add';

const parseLabel = (label: string) => {
  if (label === 'x') {
    return <DeleteIcon />;
  }

  if (label.includes('+')) {
    const parts = label.split('+');
    return (
      <>
        {parts[0]}
        <AddIcon />
        {parts[1]}
      </>
    );
  }

  return label;
};

export const QueryBuilderButton: FC<ActionWithRulesProps> = ({ handleOnClick, label, title }) => {
  const classes = useCommonQueryStyles({});

  return (
    <Button
      type="button"
      variant="outlined"
      onClick={handleOnClick}
      title={title}
      size="small"
      className={classes.marginRight}
    >
      {parseLabel(label)}
    </Button>
  );
};
