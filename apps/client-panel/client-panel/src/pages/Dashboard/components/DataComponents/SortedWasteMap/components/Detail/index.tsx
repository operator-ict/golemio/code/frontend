import React from 'react';
import cls from './detail.module.scss';
import { observer } from 'mobx-react-lite';
import { appState } from 'state';
import { SortedWasteProperties } from 'codecs';
import { ContentWrapper } from 'components/MapComponents';
import { Typography } from '@material-ui/core';
import { ContainerList } from './ContainerList';

interface DetailProps {
  properties: SortedWasteProperties;
  measurementsRoute: string;
}

export const SortedWasteDetail = observer((props: DetailProps) => {
  const { properties, measurementsRoute } = props;
  const dictionary = appState.districts.dictionary;

  if (!properties) {
    return null;
  }

  const getDistrictLabel = (key: string) => dictionary[key] || key;

  return (
    <ContentWrapper>
      <Typography variant="h4" component="h2">
        {properties.name}
        {properties.district && ` - ${getDistrictLabel(properties.district)}`}
      </Typography>
      <Typography color="textSecondary" className={cls.subtitle}>
        {properties.station_number}
      </Typography>
      {properties.containers && (
        <div className={cls.detail}>
          <ContainerList containers={properties.containers} measurementsRoute={measurementsRoute} />
        </div>
      )}
    </ContentWrapper>
  );
});
