import React, { FC, useState, useEffect, useCallback } from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
  Box,
  Paper,
  Typography,
  makeStyles,
  CircularProgress,
} from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import { Save } from '@material-ui/icons';
import { MarkdownPreview } from 'components/MarkdownPreview';
import MDEditor, { commands } from '@uiw/react-md-editor';
import { DataExportMetadata } from 'codecs/DataExportIO';
import { observer } from 'mobx-react-lite';
import { appState } from 'state';

const useStyles = makeStyles((theme) => ({
  container: {
    flex: '1 0 0',
    padding: theme.spacing(2),
    '&:first-child': {
      marginRight: theme.spacing(1),
    },
    '&:last-child': {
      marginLeft: theme.spacing(1),
    },
  },
}));

export const DataExportEditDialog: FC<{
  open: boolean;
  handleClose: () => void;
  initialMarkdown: string;
  metadata: DataExportMetadata;
}> = observer(({ open, initialMarkdown, handleClose, metadata }) => {
  const { t } = useTranslation();
  const classes = useStyles({});
  const [markdown, setMarkdown] = useState(initialMarkdown);
  const { fetch, isLoading, error } = appState.metadata.updateMetadata;

  useEffect(() => {
    setMarkdown(initialMarkdown);
  }, [initialMarkdown]);

  const updateDescription = useCallback(() => {
    fetch({
      params: { id: metadata._id },
      body: {
        ...metadata,
        tags: metadata.tags.map((tag) => tag._id),
        data: { columnDescription: markdown },
      },
    }).then(() => {
      if (!error) {
        handleClose();
      }
    });
  }, [metadata, markdown, handleClose, error]);

  return (
    <Dialog fullWidth={true} open={open} maxWidth={false}>
      <DialogTitle>{t('editColumnDescription')}</DialogTitle>
      <DialogContent>
        <Box display="flex" alignItems="stretch">
          <Paper className={classes.container}>
            <Typography variant="h3">{t('columnDescription')}</Typography>
            <MDEditor
              preview="edit"
              value={markdown}
              onChange={setMarkdown}
              height={500}
              textareaProps={{ disabled: isLoading }}
              commands={[
                commands.bold,
                commands.italic,
                commands.strikethrough,
                commands.hr,
                commands.title,
                commands.divider,
                commands.link,
                commands.quote,
                commands.code,
                commands.divider,
                commands.unorderedListCommand,
                commands.orderedListCommand,
                commands.checkedListCommand,
              ]}
            />
          </Paper>
          <Paper className={classes.container}>
            <Typography variant="h3">{t('preview')}</Typography>
            <MarkdownPreview source={markdown} />
          </Paper>
        </Box>
      </DialogContent>
      <DialogActions>
        <Button
          variant="contained"
          type="button"
          onClick={() => {
            setMarkdown(initialMarkdown);
            handleClose();
          }}
        >
          {t('cancel')}
        </Button>
        <Button
          variant="contained"
          color="primary"
          type="button"
          startIcon={<Save />}
          disabled={isLoading}
          onClick={updateDescription}
        >
          {isLoading ? <CircularProgress size={20} /> : t('save')}
        </Button>
      </DialogActions>
    </Dialog>
  );
});
