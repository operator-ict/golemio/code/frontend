import React, { FC } from 'react';
import { MapComponent, MapComponentMetadataIO } from './MapComponent';
import { Typography } from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import {
  ParkingsMap,
  ParkingsMapMetadataIO,
} from 'pages/Dashboard/components/DataComponents/ParkingMap';
import {
  SortedWasteMap,
  SortedWasteMapMetadataIO,
} from 'pages/Dashboard/components/DataComponents/SortedWasteMap';
import { UnknownMetadata } from 'codecs';
import * as io from 'io-ts';
import { isRight } from 'fp-ts/lib/Either';
import {
  IframeComponent,
  IframeComponentMetadataIO,
} from 'pages/Dashboard/components/DataComponents/IframeComponent';
import { AppLayout } from 'components/AppLayout';
import { DashboardHeader } from 'pages/Dashboard/components/DashboardHeader';
import { DataExport } from 'pages/Dashboard/components/DataComponents/DataExport';
import {
  PowerBiComponent,
  PowerBiMetadataIO,
} from 'pages/Dashboard/components/DataComponents/PowerBiComponent';
import { DataExportMetadataIO } from 'codecs/DataExportIO';
import {
  AttachmentComponent,
  AttachmentComponentMetadataIO,
} from 'pages/Dashboard/components/DataComponents/AttachmentComponent';

export const useComponents = () => {
  const { t } = useTranslation();

  const components: {
    [key: string]: {
      Component: React.ComponentType<{ metadata: UnknownMetadata }>;
      codec: io.Mixed;
      title: string;
    };
  } = {
    dataExport: {
      Component: DataExport,
      codec: DataExportMetadataIO,
      title: t('components::dataExport'),
    },
    powerbiComponent: {
      Component: PowerBiComponent,
      codec: PowerBiMetadataIO,
      title: t('components::powerBi'),
    },
    iframeComponent: {
      Component: IframeComponent,
      codec: IframeComponentMetadataIO,
      title: t('components::iframe'),
    },
    attachmentComponent: {
      Component: AttachmentComponent,
      codec: AttachmentComponentMetadataIO,
      title: t('components::attachment'),
    },
    mapComponent: {
      Component: MapComponent,
      codec: MapComponentMetadataIO,
      title: t('components::map'),
    },
    parkingsMap: {
      Component: ParkingsMap,
      codec: ParkingsMapMetadataIO,
      title: t('components::parkingsMap'),
    },
    sortedWasteMap: {
      Component: SortedWasteMap,
      codec: SortedWasteMapMetadataIO,
      title: t('components::sortedWasteMap'),
    },
  };

  return { components };
};

const ErrorComponent: FC<{ errorKey: string; metadata: UnknownMetadata }> = ({
  errorKey,
  metadata,
}) => {
  const { t } = useTranslation();

  return (
    <AppLayout header={<DashboardHeader title={metadata.title} />}>
      <Typography color="error">{t(errorKey)}</Typography>
    </AppLayout>
  );
};

interface DataComponentProps {
  metadata: UnknownMetadata;
}

export const DataComponent = (props: DataComponentProps) => {
  const { metadata } = props;
  const { components } = useComponents();
  const component = components[metadata.component];

  if (!component) {
    return <ErrorComponent errorKey="invalidComponent" metadata={metadata} />;
  }

  const { Component, codec } = component;

  if (!isRight(codec.decode(metadata))) {
    return <ErrorComponent errorKey="invalidMetadata" metadata={metadata} />;
  }

  return <Component metadata={metadata} />;
};
