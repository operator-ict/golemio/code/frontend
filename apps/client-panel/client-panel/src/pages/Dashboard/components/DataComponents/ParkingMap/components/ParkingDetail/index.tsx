import React from 'react';
import moment from 'moment';
import { useTranslation } from 'react-i18next';
import { ContentTitle } from 'components';
import { ParkingProperties } from 'codecs';

import { ContentWrapper, SectionInfoContainer, SectionInfo } from 'components/MapComponents';
import { ChartOccupancyWeekly } from '../ChartOccupancyWeekly';
import { Typography } from '@material-ui/core';

interface ParkingDetailProps {
  properties: ParkingProperties;
}

export const ParkingDetail = (props: ParkingDetailProps) => {
  const { properties } = props;
  const { t } = useTranslation();
  return (
    <>
      <ContentWrapper>
        <Typography variant="h4" component="h2">
          {properties.name}
        </Typography>
        {properties.address && (
          <Typography color="textSecondary">{properties.address.address_formatted}</Typography>
        )}
        <SectionInfoContainer>
          <SectionInfo>
            <ContentTitle>{t('page::parkings::spotDetail::capacity')}</ContentTitle>
            <p>{properties.total_num_of_places}</p>
          </SectionInfo>
          <SectionInfo>
            <ContentTitle>{t('page::parkings::spotDetail::occupancy')}</ContentTitle>
            <p>{properties.num_of_taken_places}</p>
          </SectionInfo>
          <SectionInfo>
            <ContentTitle>{t('page::parkings::spotDetail::freeSpots')}</ContentTitle>
            <p>{properties.num_of_free_places}</p>
          </SectionInfo>
          <SectionInfo>
            <ContentTitle>{t('page::parkings::spotDetail::lastUpdate')}</ContentTitle>
            <p>{moment(properties.last_updated).fromNow()}</p>
          </SectionInfo>
        </SectionInfoContainer>
      </ContentWrapper>
      <ChartOccupancyWeekly properties={properties} />
    </>
  );
};
