import React, { ChangeEvent } from 'react';
import { BarChart, Bar, XAxis, YAxis, Tooltip, ResponsiveContainer, ReferenceLine } from 'recharts';
import { useTranslation } from 'react-i18next';
import cls from './chart.module.scss';
import { ParkingProperties } from 'codecs';
import { Select, MenuItem, Typography } from '@material-ui/core';

const days = [
  {
    value: 1,
    label: 'Monday',
  },
  {
    value: 2,
    label: 'Tuesday',
  },
  {
    value: 3,
    label: 'Wednesday',
  },
  {
    value: 4,
    label: 'Thursday',
  },
  {
    value: 5,
    label: 'Friday',
  },
  {
    value: 6,
    label: 'Saturday',
  },
  {
    value: 0,
    label: 'Sunday',
  },
];

const mapDayData = (data: { [k: string]: number | null }, key: string) => {
  const hours = [
    '00',
    '01',
    '02',
    '03',
    '04',
    '05',
    '06',
    '07',
    '08',
    '09',
    '10',
    '11',
    '12',
    '13',
    '14',
    '15',
    '16',
    '17',
    '18',
    '19',
    '20',
    '21',
    '22',
    '23',
  ];

  const mappedData = hours.map((item) => ({
    hour: item,
    [key]: data ? data[item] : null,
  }));

  return mappedData;
};

interface ChartProps {
  properties: ParkingProperties;
}

export const ChartOccupancyWeekly = (props: ChartProps) => {
  const { properties } = props;
  const { average_occupancy, num_of_taken_places } = properties;
  const { t } = useTranslation();
  const [activeWeekDay, setActiveWeekDay] = React.useState(new Date().getDay());

  const occupiedString = t('page::parkings::spotDetail::occupied');
  const todayAverageOccupancy =
    average_occupancy && mapDayData(average_occupancy[activeWeekDay], occupiedString);

  interface TooltipProps {
    active?: boolean;
    payload?: Array<{ name: string; value: number }>;
    label?: string;
  }

  const CustomTooltip = ({ active, payload, label }: TooltipProps) => {
    if (active && payload) {
      return (
        <div className={cls.customTooltip}>
          <Typography variant="h4" className={cls.title}>
            {label}:00
          </Typography>
          {payload.map((item) => (
            <div key={cls.name} className={cls.item}>
              <p className={cls.name}>{item.name}:</p>
              <p className={cls.value}>{Math.floor(item.value)}</p>
            </div>
          ))}
        </div>
      );
    }

    return null;
  };

  const handleChange = (e: ChangeEvent<{ name?: string; value: unknown }>) => {
    setActiveWeekDay(e.target.value as number);
  };

  return (
    <div className={cls.container}>
      <div className={cls.daySelectWrapper}>
        <div>
          <Typography variant="h4" component="h3" className={cls.white}>
            {t('page::parkings::spotDetail::averageOccupancy')}
          </Typography>
        </div>
        <Select
          value={activeWeekDay}
          onChange={handleChange}
          MenuProps={{ disablePortal: true }}
          className={cls.daySelect}
          classes={{ select: cls.daySelect, icon: cls.daySelectIcon }}
        >
          {days.map((day) => (
            <MenuItem key={day.value} value={day.value}>
              {t(day.label)}
            </MenuItem>
          ))}
        </Select>
      </div>
      <div style={{ overflow: 'hidden' }}>
        {todayAverageOccupancy ? (
          <div className={cls.wrapper}>
            <ResponsiveContainer height={180} width="100%">
              <BarChart
                data={todayAverageOccupancy}
                margin={{
                  top: 5,
                  right: 30,
                  left: 20,
                  bottom: 5,
                }}
              >
                {/* <CartesianGrid strokeDasharray="1 1" /> */}
                <XAxis dataKey="hour" />
                <YAxis
                  orientation="right"
                  padding={{
                    top: 0,
                    bottom: 0,
                  }}
                  tickLine={false}
                />
                <Tooltip content={<CustomTooltip />} />
                <Bar dataKey={occupiedString} fill="white" strokeWidth={0} />
                <ReferenceLine
                  label={t('page::parkings::spotDetail::now') as string}
                  stroke="red"
                  strokeDasharray="3 3"
                  y={num_of_taken_places}
                />
              </BarChart>
            </ResponsiveContainer>
          </div>
        ) : (
          <h3 className={cls.noData}>{t('page::parkings::spotDetail::noData')}</h3>
        )}
      </div>
    </div>
  );
};
