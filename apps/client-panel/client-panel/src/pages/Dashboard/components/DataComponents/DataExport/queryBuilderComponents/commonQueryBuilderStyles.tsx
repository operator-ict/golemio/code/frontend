import { makeStyles } from '@material-ui/core';

export const useCommonQueryStyles = makeStyles((theme) => ({
  marginRight: {
    '&:not(last-child)': {
      marginRight: theme.spacing(1),
    },
  },
}));
