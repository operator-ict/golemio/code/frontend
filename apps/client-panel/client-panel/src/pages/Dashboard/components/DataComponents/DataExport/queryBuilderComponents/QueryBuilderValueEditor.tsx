import React, { FC, useCallback, ChangeEvent } from 'react';
import { ValueEditorProps } from 'react-querybuilder';
import {
  TextField,
  Select,
  MenuItem,
  Checkbox,
  RadioGroup,
  FormControlLabel,
  Radio,
} from '@material-ui/core';
import { useCommonQueryStyles } from './commonQueryBuilderStyles';
import classnames from 'classnames';

export const QueryBuilderValueEditor: FC<ValueEditorProps> = ({
  className,
  handleOnChange,
  inputType,
  operator,
  title,
  type,
  value,
  values,
}) => {
  const classes = useCommonQueryStyles({});
  const finalClassName = classnames(className, classes.marginRight);

  const onChange = useCallback(
    (
      e: ChangeEvent<{
        name?: string;
        value: unknown;
      }>,
    ) => handleOnChange(e.target.value),
    [handleOnChange],
  );

  if (['null', 'notNull'].includes(operator)) {
    return null;
  }

  switch (type) {
    case 'text':
      return (
        <TextField
          className={finalClassName}
          title={title}
          onChange={onChange}
          value={value}
          type={inputType}
        />
      );
    case 'select':
      return (
        <Select className={finalClassName} title={title} onChange={onChange}>
          {values.map((option) => (
            <MenuItem value={option.name} key={option.name}>
              {option.label}
            </MenuItem>
          ))}
        </Select>
      );
    case 'checkbox':
      return (
        <Checkbox
          className={finalClassName}
          title={title}
          checked={!!value}
          onChange={(e) => handleOnChange(e.target.checked)}
        />
      );
    case 'radio':
      return (
        <RadioGroup value={value} onChange={onChange} row={true}>
          {values.map((option) => (
            <FormControlLabel
              key={option.name}
              label={option.label}
              labelPlacement="start"
              control={<Radio value={option.name} />}
            />
          ))}
        </RadioGroup>
      );
    default:
      return null;
  }
};
