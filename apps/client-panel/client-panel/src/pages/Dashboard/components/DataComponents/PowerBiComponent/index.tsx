import React, { FC, useEffect, useRef, useState } from 'react';
import { MetadataIO } from 'codecs';
import * as io from 'io-ts';
import { observer } from 'mobx-react-lite';
import { AppLayout } from 'components/AppLayout';
import { DashboardHeader } from 'pages/Dashboard/components/DashboardHeader';
import { ContainerVertical } from 'components';
import { appState } from 'state';
import { IReportEmbedConfiguration, models } from 'powerbi-client';
import { makeStyles, IconButton } from '@material-ui/core';
import { Fullscreen } from '@material-ui/icons';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles(() => ({
  relative: {
    position: 'relative',
  },
  reportContainer: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    '& > iframe': {
      border: 'none',
    },
  },
}));

const PowerBiDataIO = io.interface({
  report_id: io.string,
  workspace_id: io.string,
});

export const PowerBiMetadataIO = MetadataIO(PowerBiDataIO);

type PowerBiMetadata = io.TypeOf<typeof PowerBiMetadataIO>;

export const PowerBiComponent: FC<{ metadata: PowerBiMetadata }> = observer(({ metadata }) => {
  const powerBiTokenState = useRef(appState.createPowerBiTokenState());
  const { data, fetch } = powerBiTokenState.current;
  const classes = useStyles();
  const reportContainerRef = useRef(null);
  const [report, setReport] = useState(null);
  const { t } = useTranslation();

  useEffect(() => {
    fetch({ params: { metadataId: metadata._id } });
  }, [metadata._id]);

  useEffect(() => {
    if (!data || !reportContainerRef.current) {
      return;
    }

    const config: IReportEmbedConfiguration = {
      accessToken: data.accessToken,
      embedUrl: data.embedUrl[0].embedUrl,
      id: data.embedUrl[0].reportId,
      tokenType: models.TokenType.Embed,
      type: 'report',
      settings: {
        panes: {
          filters: {
            visible: false,
          },
          pageNavigation: {
            visible: false,
          },
        },
      },
    };

    const report = window.powerbi.embed(reportContainerRef.current, config);
    setReport(report);
  }, [reportContainerRef.current, data]);

  return (
    <AppLayout
      header={
        <DashboardHeader
          title={metadata.title}
          customTopBar={
            <IconButton
              title={t('showFullscreen')}
              color="primary"
              type="button"
              disabled={!report}
              onClick={() => report?.fullscreen()}
            >
              <Fullscreen />
            </IconButton>
          }
        />
      }
    >
      <ContainerVertical className={classes.relative}>
        <div className={classes.reportContainer} ref={reportContainerRef} />
      </ContainerVertical>
    </AppLayout>
  );
});
