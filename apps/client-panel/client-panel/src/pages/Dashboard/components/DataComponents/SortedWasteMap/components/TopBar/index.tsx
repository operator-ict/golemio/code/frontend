import React, { FC, useContext } from 'react';
import { useTranslation } from 'react-i18next';
import { FilterWrapper, FilterSelect, FilterMultiSelect, FilterRange } from 'components';

import cls from './topbar.module.scss';
import { SortedWasteFiltersContext } from '../../sortedWasteFiltersContext';
import { withStyles, Tooltip, Typography, Select, MenuItem } from '@material-ui/core';
import { observer } from 'mobx-react-lite';
import { appState } from 'state';

const CustomTooltip = withStyles({
  tooltip: {
    fontSize: 15,
  },
})(Tooltip);
interface Props {
  stationsCount: number;
}

export const SortedWasteTopBar: FC<Props> = observer(({ stationsCount }) => {
  const { t } = useTranslation();
  const districtList = appState.sortedWaste.districtList;
  const dictionary = appState.districts.dictionary;

  const { filterDescription, resetFilters, setFilterOptions, filterOptions } = useContext(
    SortedWasteFiltersContext,
  );

  const fullnessEnabled = filterOptions.sensor === 'sensor_only';

  const FullnessWrapper = !fullnessEnabled
    ? ({ children }) => (
        <CustomTooltip title={t('page::sortedWaste::filters::forSensorOnly')}>
          <div>{children}</div>
        </CustomTooltip>
      )
    : ({ children }) => <>{children}</>;

  return (
    <div className={cls.container}>
      <div className={cls.filterList}>
        <span>
          {t('page::sortedWaste::filters::shownStations', { count: stationsCount })}
          {filterDescription}.
        </span>
      </div>

      <FilterWrapper resetFilters={resetFilters}>
        <FilterSelect
          title={t(`page::sortedWaste::filters::containers`)}
          value={filterOptions.sensor}
          optionList={[
            {
              label: t('page::sortedWaste::filters::all'),
              value: 'all',
            },
            {
              label: t('page::sortedWaste::filters::sensorOnly'),
              value: 'sensor_only',
            },
          ]}
          onChange={(value) => setFilterOptions('sensor', value as 'all' | 'sensor_only')}
        />
        <FilterMultiSelect
          title={t(`page::sortedWaste::filters::wasteTypes`)}
          values={filterOptions.types}
          onChange={(values) => setFilterOptions('types', values)}
          optionList={[
            {
              label: t('page::sortedWaste::filters::all'),
              value: '',
              type: 'radio',
            },
            ...[1, 2, 3, 4, 5, 6, 7, 8].map((item) => ({
              label: t(`page::sortedWaste::trashType::${item}`),
              value: String(item),
            })),
          ]}
        />
        <FullnessWrapper>
          <FilterRange
            title={t(`page::sortedWaste::filters::fullness`)}
            values={[filterOptions.fullness.min, filterOptions.fullness.max]}
            max={100}
            min={0}
            unit="%"
            disabled={!fullnessEnabled}
            onChange={(min, max) => setFilterOptions('fullness', { min, max })}
          />
        </FullnessWrapper>
        <div>
          <Typography variant="h4" component="h2">
            {t(`page::sortedWaste::filters::area`)}
          </Typography>
          <Select
            MenuProps={{ className: 'ignoreClickAway' }}
            value={filterOptions.area.value || 'all'}
            onChange={(e) => {
              const inputValue = e.target.value as string;
              const value = inputValue === 'all' ? '' : inputValue;
              const label = value ? dictionary[value] : '';

              setFilterOptions('area', {
                value,
                label,
              });
            }}
            placeholder={t('page::sortedWaste::filters::chooseDistrict')}
            style={{ width: '100%', maxWidth: '100%' }}
          >
            {<MenuItem value="all">{t('page::sortedWaste::filters::all')}</MenuItem>}
            {districtList.map((district) => {
              return (
                <MenuItem value={district} key={district}>
                  {dictionary[district]}
                </MenuItem>
              );
            })}
          </Select>
        </div>
      </FilterWrapper>
    </div>
  );
});
