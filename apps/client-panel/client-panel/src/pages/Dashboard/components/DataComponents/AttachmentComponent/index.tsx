import React, { FC, useMemo, useState, useEffect } from 'react';
import * as io from 'io-ts';
import { MetadataIO } from 'codecs';
import { ContainerVertical } from 'components';
import { DashboardHeader } from 'pages/Dashboard/components/DashboardHeader';
import { AppLayout } from 'components/AppLayout';
import PDFObject from 'pdfobject';
import { PDFObject as PDFObjectComponent } from 'react-pdfobject';
import { Box, Button, makeStyles, Typography, CircularProgress } from '@material-ui/core';
import { Trans, useTranslation } from 'react-i18next';
import { GetApp } from '@material-ui/icons';
import { observer } from 'mobx-react-lite';
import { appState } from 'state';

const useStyles = makeStyles(() => ({
  pdfContainer: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
    '& > *': {
      flex: 1,
    },
  },
}));

export const AttachmentComponentMetadataIO = MetadataIO(io.null);

export const AttachmentComponent: FC<{
  metadata: io.TypeOf<typeof AttachmentComponentMetadataIO>;
}> = observer(({ metadata }) => {
  const { t } = useTranslation();
  const classes = useStyles({});
  const { fetch, isLoading, error } = appState.metadata.attachment;
  const [blob, setBlob] = useState<Blob | null>(null);
  const [dataUrl, setDataUrl] = useState<string | null>(null);

  const { title, type, metadataError } = useMemo(() => {
    if (!metadata.attachment) {
      return { title: null, url: null, type: null, metadataError: true };
    }

    return {
      title: metadata.attachment.title,
      url: metadata.attachment.url,
      metadataError: false,
      type: metadata.attachment.content_type,
    };
  }, [metadata]);

  useEffect(() => {
    setDataUrl(null);

    fetch({ params: { metadataId: metadata._id } }).then((blob) => {
      setBlob(blob as Blob);
    });
  }, [fetch, metadata]);

  useEffect(() => {
    if (!blob) {
      return;
    }

    const file = new File([blob], title, { type });
    const fileUrl = URL.createObjectURL(file);
    setDataUrl(fileUrl);

    return () => {
      URL.revokeObjectURL(fileUrl);
    };
  }, [blob, title, type]);

  const isPDF = type === 'application/pdf';
  const showContent = isPDF && PDFObject.supportsPDFs;

  return (
    <AppLayout header={<DashboardHeader title={metadata.title} />}>
      <ContainerVertical>
        {isLoading && (
          <Box textAlign="center">
            <CircularProgress />
          </Box>
        )}
        {!isLoading && (
          <>
            {metadataError ||
              (!!error && (
                <Typography align="center" color="error">
                  {t('attachmentError')}
                </Typography>
              ))}
            {!metadataError && !error && !!dataUrl && (
              <>
                {showContent ? (
                  <PDFObjectComponent
                    url={dataUrl}
                    containerProps={{ className: classes.pdfContainer }}
                  />
                ) : (
                  <Box textAlign="center">
                    <Typography>
                      <Trans
                        i18nKey="failedToShowFile"
                        components={[<strong />]}
                        values={{ name: title }}
                      />
                    </Typography>
                    <Button
                      startIcon={<GetApp />}
                      variant="contained"
                      color="primary"
                      href={dataUrl}
                      download={title}
                    >
                      {t('download')}
                    </Button>
                  </Box>
                )}
              </>
            )}
          </>
        )}
      </ContainerVertical>
    </AppLayout>
  );
});
