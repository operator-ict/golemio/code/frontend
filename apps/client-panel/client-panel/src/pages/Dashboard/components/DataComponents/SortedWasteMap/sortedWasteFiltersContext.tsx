import React, { createContext, FC, ReactNode, useMemo, useState, useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import { SortedWasteCollection } from 'codecs';
import { isNumber } from 'util';

type FilterOptions = {
  sensor: 'sensor_only' | 'all';
  fullness: {
    min: number;
    max: number;
  };
  area: { value: string; label: string };
  types: string[];
};

const DEFAULT_FILTER_OPTIONS: FilterOptions = {
  sensor: 'sensor_only',
  fullness: {
    min: 0,
    max: 100,
  },
  area: { label: '', value: '' },
  types: [],
};

type SetFilterOptions = <T extends keyof FilterOptions>(name: T, value: FilterOptions[T]) => void;

type ContextState = {
  filterOptions: FilterOptions;
  setFilterOptions: SetFilterOptions;
  resetFilters: () => void;
  filterDescription: string;
  filterFunction: (data: SortedWasteCollection) => SortedWasteCollection;
};

export const SortedWasteFiltersContext = createContext<ContextState>({
  filterOptions: DEFAULT_FILTER_OPTIONS,
  setFilterOptions: () => {},
  resetFilters: () => {},
  filterDescription: '',
  filterFunction: (data) => data,
});

interface Props {
  children: ReactNode;
}

export const SortedWasteFiltersProvider: FC<Props> = ({ children }) => {
  const [filterOptions, setFilterOptionsState] = useState(DEFAULT_FILTER_OPTIONS);
  const { t } = useTranslation();

  const setFilterOptions: SetFilterOptions = useCallback((name, value) => {
    setFilterOptionsState((prevState) => {
      const newState = {
        ...prevState,
        [name]: value,
      };

      if (name === 'sensor' && value === 'all') {
        newState.fullness = {
          min: 0,
          max: 100,
        };
      }

      return newState;
    });
  }, []);

  const resetFilters = useCallback(() => {
    setFilterOptionsState(DEFAULT_FILTER_OPTIONS);
  }, []);

  const filterDescription = useMemo(() => {
    const { sensor, types, fullness, area } = filterOptions;
    const { min, max } = fullness;
    const filterDescriptionParts: string[] = [];

    // sensor
    if (sensor === 'sensor_only') {
      filterDescriptionParts.push(t('page::sortedWaste::filters::withSensor').toLowerCase());
    }

    // trash types
    if (types.length > 0) {
      const typesDescription = types
        .map((option) => t(`page::sortedWaste::trashType::${option}`))
        .join(', ');

      filterDescriptionParts.push(
        t('page::sortedWaste::filters::containing', { types: typesDescription }).toLowerCase(),
      );
    }

    // fullness
    if ((min > 0 || max < 100) && min === max) {
      filterDescriptionParts.push(`${t('page::sortedWaste::filters::percentFull', fullness)}`);
    } else if (min > 0 && max < 100) {
      filterDescriptionParts.push(`${t('page::sortedWaste::filters::between', fullness)}`);
    } else if (min > 0) {
      filterDescriptionParts.push(`${t('page::sortedWaste::filters::min', fullness)}`);
    } else if (max < 100) {
      filterDescriptionParts.push(`${t('page::sortedWaste::filters::max', fullness)}`);
    }

    // area
    if (area.value !== '') {
      filterDescriptionParts.push(
        `${t('page::sortedWaste::filters::inArea', { area: area.label })}`,
      );
    }

    if (filterDescriptionParts.length === 0) {
      return '';
    } else {
      return ' ' + filterDescriptionParts.join(', ');
    }
  }, [filterOptions, t]);

  const filterFunction = useCallback(
    (data: SortedWasteCollection) => {
      return {
        ...data,
        features: data?.features?.reduce((features, feature) => {
          const { area, sensor, fullness, types } = filterOptions;
          const { min, max } = fullness;

          // area
          if (area.value && feature.properties.district !== area.value) {
            return features;
          }

          const containers = feature.properties.containers?.filter((container) => {
            // sensors
            if (sensor === 'sensor_only' && !container.sensor_code) {
              return false;
            }

            // types
            if (types.length && !types.includes(`${container.trash_type.id}`)) {
              return false;
            }

            // fullness
            const lastMeasurement = container.last_measurement?.percent_calculated;
            if (min > 0 || max < 100) {
              if (!isNumber(lastMeasurement) || lastMeasurement < min || lastMeasurement > max) {
                return false;
              }
            }

            return true;
          });

          if (containers?.length) {
            features.push({ ...feature, containers });
          }

          return features;
        }, []),
      };
    },
    [filterOptions],
  );

  const val = useMemo(() => {
    return {
      filterOptions,
      setFilterOptions,
      resetFilters,
      filterDescription,
      filterFunction,
    };
  }, [filterOptions, setFilterOptionsState, resetFilters, filterDescription, filterFunction]);

  return (
    <SortedWasteFiltersContext.Provider value={val}>{children}</SortedWasteFiltersContext.Provider>
  );
};
