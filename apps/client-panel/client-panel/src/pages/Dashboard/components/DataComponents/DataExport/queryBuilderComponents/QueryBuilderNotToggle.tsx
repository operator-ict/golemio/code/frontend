import React, { FC } from 'react';
import { NotToggleProps } from 'react-querybuilder';
import { FormControlLabel, Checkbox } from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import { useCommonQueryStyles } from './commonQueryBuilderStyles';

export const QueryBuilderNotToggle: FC<NotToggleProps> = ({ handleOnChange, checked, title }) => {
  const { t } = useTranslation();
  const classes = useCommonQueryStyles({});

  return (
    <FormControlLabel
      className={classes.marginRight}
      labelPlacement="start"
      title={title}
      label={t('invert')}
      control={
        <Checkbox checked={checked} onChange={({ target }) => handleOnChange(target.checked)} />
      }
    />
  );
};
