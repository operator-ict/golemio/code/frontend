import React from 'react';
import { observer } from 'mobx-react-lite';
import * as io from 'io-ts';
import { MetadataIO } from 'codecs';
import { ContainerVertical } from 'components';
import { DashboardHeader } from 'pages/Dashboard/components/DashboardHeader';
import { AppLayout } from 'components/AppLayout';

export const IframeComponentMetadataIO = MetadataIO(
  io.interface({
    url: io.string,
  }),
);

interface IframeComponentProps {
  metadata: io.TypeOf<typeof IframeComponentMetadataIO>;
}

export const IframeComponent = observer((props: IframeComponentProps) => {
  const { metadata } = props;

  return (
    <AppLayout header={<DashboardHeader title={metadata.title} />}>
      <ContainerVertical>
        <iframe
          frameBorder="0"
          allowFullScreen={true}
          src={metadata.data.url}
          style={{ flex: '1 0 auto' }}
        />
      </ContainerVertical>
    </AppLayout>
  );
});
