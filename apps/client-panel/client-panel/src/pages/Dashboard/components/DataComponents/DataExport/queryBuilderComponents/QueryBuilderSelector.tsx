import React, { FC, useCallback, useMemo } from 'react';
import { ValueSelectorProps } from 'react-querybuilder';
import { Select, MenuItem } from '@material-ui/core';
import { useCommonQueryStyles } from './commonQueryBuilderStyles';
import { useTranslation } from 'react-i18next';

const LABELS_TO_TRANSLATE = ['AND', 'OR'];

export const QueryBuilderSelector: FC<ValueSelectorProps> = ({
  handleOnChange,
  options,
  title,
  value,
  className,
}) => {
  const classes = useCommonQueryStyles({});
  const { t } = useTranslation();

  const isCombinatorSelector = useMemo(() => {
    return className.includes('ruleGroup-combinators');
  }, [className]);

  const parseOptionLabel = useCallback(
    (label: string) => {
      if (isCombinatorSelector && LABELS_TO_TRANSLATE.includes(label)) {
        return t(label);
      } else {
        return label;
      }
    },
    [t, isCombinatorSelector],
  );

  return (
    <Select
      variant="standard"
      onChange={({ target }) => handleOnChange(target.value)}
      title={title}
      value={value}
      className={classes.marginRight}
    >
      {options.map((option) => (
        <MenuItem value={option.name} key={option.name}>
          {parseOptionLabel(option.label)}
        </MenuItem>
      ))}
    </Select>
  );
};
