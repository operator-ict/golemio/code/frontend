import React, { FC, ReactNode } from 'react';
import { makeStyles, Typography, Button } from '@material-ui/core';
import ForwardIcon from '@material-ui/icons/Forward';
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  wrapper: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexWrap: 'wrap',
    padding: theme.spacing(4),
    '& > *': {
      flex: '1 0 auto',
    },
    '& > *:last-child': {
      textAlign: 'right',
    },
  },
  linkIcon: {
    transform: 'rotate(180deg)',
  },
  title: {
    display: 'flex',
    alignItems: 'center',
    maxWidth: '100%',
  },
  link: {
    display: 'flex',
    alignItems: 'center',
    marginRight: theme.spacing(4),
  },
}));

export const DashboardHeader: FC<{ title: string; customTopBar?: ReactNode }> = ({
  title,
  customTopBar,
}) => {
  const classes = useStyles({});

  return (
    <div className={classes.wrapper}>
      <div className={classes.title}>
        <Button component={Link} to="/dashboard" className={classes.link}>
          <ForwardIcon color="primary" fontSize="large" className={classes.linkIcon} />
        </Button>
        <Typography variant="h1" color="primary">
          {title.toLocaleUpperCase()}
        </Typography>
      </div>
      <div>{customTopBar}</div>
    </div>
  );
};
