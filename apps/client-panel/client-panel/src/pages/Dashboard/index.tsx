import React, { useEffect, useState } from 'react';
import { Switch, Route, Redirect, useLocation } from 'react-router-dom';
import { Home } from './Home';
import { observer } from 'mobx-react-lite';
import { appState } from 'state';
import { DataComponent } from 'pages/Dashboard/components/DataComponents';
import { EditPage } from 'pages/Dashboard/EditPage';

enum Status {
  WAITING,
  LOADING,
  READY,
}

export const Dashboard = observer(() => {
  const { user, setUrlAfterLogin } = appState.auth;
  const { data, fetch: fetchMetadata, isLoading } = appState.metadata;
  const [state, setState] = useState<Status>(Status.WAITING);
  const { fetch: fetchTags } = appState.dashboardFilters;
  const location = useLocation();

  useEffect(() => {
    if (!user) {
      setUrlAfterLogin(location.pathname + location.search + location.hash);
      return;
    }

    setUrlAfterLogin(null);

    fetchMetadata({ params: { userId: user.id } });
    fetchTags({ params: { userId: user.id } });
  }, []);

  useEffect(() => {
    if (isLoading && state !== Status.LOADING) {
      setState(Status.LOADING);
    }

    if (state === Status.LOADING && !isLoading) {
      setState(Status.READY);
    }
  }, [state, isLoading]);

  if (!user) {
    return <Redirect to="/auth" />;
  }

  return (
    <Switch>
      <Route component={Home} exact path="/dashboard" strict />
      <Route component={EditPage} exact path="/dashboard/edit/:id" strict />
      <Route component={EditPage} exact path="/dashboard/new" strict />
      {(data || []).map((metadata) => (
        <Route
          key={metadata.client_route}
          render={() => <DataComponent metadata={metadata} />}
          exact
          strict
          path={`/dashboard${metadata.client_route}`}
        />
      ))}
      {state === Status.READY && <Redirect to="/dashboard" />}
    </Switch>
  );
});
