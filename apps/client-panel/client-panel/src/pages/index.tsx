import * as React from 'react';
import { Switch, Route, Redirect, useLocation } from 'react-router-dom';

import { AuthRoutes } from './Auth';
import { Dashboard } from './Dashboard';
import PageTermsAndConditions from './TermsAndConditions';
import { AuthStateContext } from '@dataplatform/common-state/modules';
import { appState } from 'state';
import { useAnalytics } from 'hooks/analytics';

export const AppPages = () => {
  const { pathname, search } = useLocation();
  const { pageview } = useAnalytics();

  React.useEffect(() => {
    pageview(`${pathname}${search}`);
  }, [pathname, search]);

  return (
    <React.Suspense fallback={<div />}>
      <AuthStateContext.Provider value={appState}>
        <Switch>
          <Route component={PageTermsAndConditions} exact path="/terms" strict />
          <Route component={Dashboard} path="/dashboard" />
          <Route component={AuthRoutes} path="/auth" />
          <Redirect to="/auth" />
        </Switch>
      </AuthStateContext.Provider>
    </React.Suspense>
  );
};
