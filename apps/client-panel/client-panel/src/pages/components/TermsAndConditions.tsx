import React from 'react';
import { useTranslation } from 'react-i18next';

const TermsAndConditions = () => {
  const { t } = useTranslation();
  function createMarkup() {
    return { __html: t('termsAndConditions') };
  }
  return <div dangerouslySetInnerHTML={createMarkup()} />;
};

export default TermsAndConditions;
