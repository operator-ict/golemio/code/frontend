import React, { useEffect, useState, useCallback } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Trans, useTranslation } from 'react-i18next';
import * as validation from 'validation';

import { WidgetContent, FormInputGroup, FInput, Button } from 'components';

import SimpleContainer from '../components/SimpleContainer';
import { observer } from 'mobx-react-lite';
import { useAuthStateContext } from '@dataplatform/common-state/modules';
import { Formik } from 'formik';

enum VerificationState {
  PasswordForm,
  Success,
  UnknownError,
  NotFound,
  Expired,
  Loading,
}

export const PageAccountConfirmation = observer(
  (props: RouteComponentProps<{ confirmationToken: string }>) => {
    const {
      match: {
        params: { confirmationToken },
      },
      history,
    } = props;

    const { t } = useTranslation();
    const appState = useAuthStateContext();
    const auth = appState.auth;

    const [hasSubmitted, setSubmit] = useState(false);
    const [success, setSuccess] = useState(false);

    const onSuccess = useCallback((success) => {
      if (success) {
        setSuccess(true);
        setTimeout(() => {
          history.push('/auth/sign-in');
        }, 4000);
      }
    }, []);

    useEffect(() => {
      auth.fetchInvitationState(confirmationToken);
    }, []);

    // try to verify automatically when password is not needed
    useEffect(() => {
      if (auth.invitationState.data?.need_password === false) {
        auth.accountConfirmation(confirmationToken).then(onSuccess);
      }
    }, [auth.invitationState.data]);

    const handlePasswordConfirmation = (data: { password: string }) => {
      auth.accountConfirmation(confirmationToken, data.password).then(onSuccess);
    };

    let state: VerificationState;

    if (success) {
      state = VerificationState.Success;
    } else if (auth.invitationState.isLoading || auth.accountConfirmationState.isLoading) {
      state = VerificationState.Loading;
    } else if (auth.invitationState.data?.already_expired) {
      state = VerificationState.Expired;
    } else if (auth.invitationState.error?.data?.error_status === 404) {
      state = VerificationState.NotFound;
    } else if (auth.invitationState.data?.need_password && !auth.accountConfirmationState.error) {
      state = VerificationState.PasswordForm;
    } else {
      state = VerificationState.UnknownError;
    }

    return (
      <React.Fragment>
        <SimpleContainer>
          {state === VerificationState.Success && (
            <WidgetContent style={{ textAlign: 'center', padding: '4rem' }}>
              <h2>{t('page::accountConfirmation::success::title')}</h2>
              <p>{t('page::accountConfirmation::success::asterix')}</p>
            </WidgetContent>
          )}
          {state === VerificationState.Loading && (
            <WidgetContent style={{ textAlign: 'center', padding: '4rem' }}>
              <h2>{t('page::accountConfirmation::pending::title')}</h2>
            </WidgetContent>
          )}
          {state === VerificationState.Expired && (
            <WidgetContent style={{ textAlign: 'center', padding: '4rem' }}>
              <h2>{t('page::accountConfirmation::expired::title')}</h2>
              <p>{t('page::accountConfirmation::expired::description')}</p>
            </WidgetContent>
          )}
          {state === VerificationState.NotFound && (
            <WidgetContent style={{ textAlign: 'center', padding: '4rem' }}>
              <h2>{t('page::accountConfirmation::notFound::title')}</h2>
              <p>{t('page::accountConfirmation::notFound::description')}</p>
            </WidgetContent>
          )}
          {state === VerificationState.UnknownError && (
            <WidgetContent style={{ textAlign: 'center', padding: '4rem' }}>
              <h2>{t('page::accountConfirmation::error::title')}</h2>
              <p>{t('page::accountConfirmation::error::asterix')}</p>
            </WidgetContent>
          )}
          {state === VerificationState.PasswordForm && (
            <WidgetContent style={{ textAlign: 'center', padding: '4rem' }}>
              <h2>{t('page::accountConfirmation::password::title')}</h2>
              <p>{t('page::accountConfirmation::password::description')}</p>
              <br />
              <Formik
                initialValues={{ password: '' }}
                onSubmit={handlePasswordConfirmation}
                validateOnBlur={hasSubmitted}
                validateOnChange={false}
                validationSchema={validation.createSchema({
                  password: validation.password,
                })}
              >
                {({ handleSubmit }) => (
                  <form
                    onSubmit={(e) => {
                      setSubmit(true);
                      handleSubmit(e);
                    }}
                  >
                    <FormInputGroup>
                      <FInput label={t('newPassword')} name="password" type="password" />
                      <Button
                        isLoading={auth.accountConfirmationState.isLoading}
                        primary
                        type="submit"
                      >
                        {t('page::accountConfirmation::password::submitButton')}
                      </Button>
                    </FormInputGroup>
                  </form>
                )}
              </Formik>
            </WidgetContent>
          )}
        </SimpleContainer>

        <p>
          <Trans i18nKey="auth::wantToLogIn">
            {'Want to log in? '}
            <Link to="/auth/sign-in">Go to login page.</Link>
          </Trans>
        </p>
      </React.Fragment>
    );
  },
);
