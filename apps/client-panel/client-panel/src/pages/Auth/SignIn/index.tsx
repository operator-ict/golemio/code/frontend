import React, { useCallback } from 'react';
import { Link as RouterLink, RouteComponentProps } from 'react-router-dom';
import { Formik } from 'formik';
import { useTranslation } from 'react-i18next';
import * as validation from 'validation';
import { withRecaptcha } from 'components';
import { observer } from 'mobx-react-lite';
import { useAuthStateContext } from '@dataplatform/common-state/modules';
import { TextField, Button, Typography, Link, Divider } from '@material-ui/core';
import { AuthContainer } from 'pages/Auth/components/AuthContainer';
import { useFormStyles } from 'hooks/formStyles';

export const SignIn = withRecaptcha<RouteComponentProps>(
  observer(({ history, trySubmit }) => {
    const { t } = useTranslation();
    const formClasses = useFormStyles({});

    const appState = useAuthStateContext();
    const auth = appState.auth;
    const signingIn = auth.signInState.isLoading;

    const handleLogin = useCallback(
      (data: { email: string; password: string }) =>
        trySubmit((recaptchaResponse) => {
          auth
            .signIn({ ...data, 'g-recaptcha-response': recaptchaResponse })
            .then((data: unknown) => {
              if (data) {
                history.push(auth.urlAfterLogin || '/dashboard');
              }
            });
        }),
      [auth, trySubmit, history],
    );

    return (
      <AuthContainer>
        <Formik
          initialValues={{ email: '', password: '' }}
          onSubmit={handleLogin}
          validationSchema={validation.createSchema({
            email: validation.email,
            password: validation.password,
          })}
        >
          {({ handleSubmit, handleBlur, handleChange, touched, errors }) => (
            <form noValidate={true} onSubmit={handleSubmit} className={formClasses.formWrapper}>
              <Typography variant="h1">{t('page::signin::title')}</Typography>
              <div>
                <TextField
                  className={formClasses.bottomMargin}
                  type="email"
                  fullWidth={true}
                  variant="outlined"
                  name="email"
                  label={t('email')}
                  placeholder={t('form::emailPlaceholder')}
                  onBlur={handleBlur}
                  onChange={handleChange}
                  error={touched.email && !!errors.email}
                />
                <TextField
                  className={formClasses.bottomMargin}
                  type="password"
                  fullWidth={true}
                  variant="outlined"
                  name="password"
                  label={t('password')}
                  onBlur={handleBlur}
                  onChange={handleChange}
                  error={touched.password && !!errors.password}
                />
              </div>
              <div>
                <Button
                  className={formClasses.bottomMargin}
                  disabled={signingIn}
                  variant="contained"
                  fullWidth={true}
                  color="primary"
                  type="submit"
                >
                  {t('signIn')}
                </Button>
                <Divider className={formClasses.bottomMargin} />
                <Button
                  className={formClasses.bottomMargin}
                  variant="contained"
                  fullWidth={true}
                  component={RouterLink}
                  to="/auth/request-access"
                >
                  {t('accessRequest')}
                </Button>
                <Link
                  underline="always"
                  color="textSecondary"
                  component={RouterLink}
                  to="/auth/password-reset"
                >
                  {t('forgottenPassword')}
                </Link>
              </div>
            </form>
          )}
        </Formik>
      </AuthContainer>
    );
  }),
);
