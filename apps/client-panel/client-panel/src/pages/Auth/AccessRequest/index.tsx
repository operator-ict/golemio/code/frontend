import React, { FC, useState, useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import { withRecaptcha } from 'components';
import { observer } from 'mobx-react-lite';
import { Button, TextField, Typography } from '@material-ui/core';
import { createAccessRequestState } from 'state/modules';
import { AuthContainer } from 'pages/Auth/components/AuthContainer';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { useFormStyles } from 'hooks/formStyles';

export const AccessRequest: FC = withRecaptcha(
  observer(({ trySubmit }) => {
    const { t } = useTranslation();
    const classes = useFormStyles({});
    const [state] = useState(createAccessRequestState());
    const { isLoading, fetch, wasSuccessful } = state;

    const sendRequest = useCallback(
      (formData: Omit<Parameters<typeof fetch>[0]['body'], 'g-recaptcha-response'>) => {
        if (isLoading) {
          return;
        }

        trySubmit((res) => {
          fetch({ body: { ...formData, 'g-recaptcha-response': res } });
        });
      },
      [trySubmit, isLoading, fetch],
    );

    return (
      <AuthContainer showBackLink={true}>
        <Formik
          initialValues={{ name: '', surname: '', email: '', organization: '' }}
          onSubmit={sendRequest}
          validationSchema={Yup.object().shape({
            name: Yup.string().required(),
            surname: Yup.string().required(),
            email: Yup.string()
              .email()
              .required(),
            organization: Yup.string().required(),
          })}
        >
          {({ handleSubmit, touched, handleBlur, handleChange, errors }) => (
            <form onSubmit={handleSubmit} noValidate={true} className={classes.formWrapper}>
              <Typography className={classes.bottomMargin} variant="h1">
                {t('accessRequest')}
              </Typography>
              {!wasSuccessful && (
                <>
                  <div>
                    <TextField
                      required={true}
                      className={classes.bottomMargin}
                      type="text"
                      fullWidth={true}
                      variant="outlined"
                      name="name"
                      label={t('name')}
                      onBlur={handleBlur}
                      onChange={handleChange}
                      error={touched.name && !!errors.name}
                    />
                    <TextField
                      required={true}
                      className={classes.bottomMargin}
                      type="text"
                      fullWidth={true}
                      variant="outlined"
                      name="surname"
                      label={t('surname')}
                      onBlur={handleBlur}
                      onChange={handleChange}
                      error={touched.surname && !!errors.surname}
                    />
                    <TextField
                      required={true}
                      className={classes.bottomMargin}
                      type="email"
                      fullWidth={true}
                      variant="outlined"
                      name="email"
                      label={t('email')}
                      onBlur={handleBlur}
                      onChange={handleChange}
                      placeholder={t('form::emailPlaceholder')}
                      error={touched.email && !!errors.email}
                    />
                    <TextField
                      required={true}
                      className={classes.bottomMargin}
                      type="text"
                      fullWidth={true}
                      variant="outlined"
                      name="organization"
                      label={t('organization')}
                      onBlur={handleBlur}
                      onChange={handleChange}
                      error={touched.organization && !!errors.organization}
                    />
                  </div>
                  <Button
                    variant="contained"
                    color="primary"
                    disabled={isLoading}
                    fullWidth={true}
                    type="submit"
                  >
                    {t('sendAccessRequest')}
                  </Button>
                </>
              )}
              {wasSuccessful && (
                <>
                  <Typography variant="h4" component="p" align="center">
                    {t('accessRequestSentSuccessfully')}
                  </Typography>
                  <div />
                </>
              )}
            </form>
          )}
        </Formik>
      </AuthContainer>
    );
  }),
);
