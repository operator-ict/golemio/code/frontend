import React, { FC } from 'react';
import { useTranslation } from 'react-i18next';

import { Formik } from 'formik';
import * as validation from 'validation';

import { withRecaptcha } from 'components';

import { useAuthStateContext } from '@dataplatform/common-state/modules';
import { AuthContainer } from 'pages/Auth/components/AuthContainer';
import { Typography, TextField, CircularProgress, Button } from '@material-ui/core';
import { useFormStyles } from 'hooks/formStyles';
import { observer } from 'mobx-react-lite';

export const PagePasswordResetRequest: FC = withRecaptcha(
  observer(({ trySubmit }) => {
    const { t } = useTranslation();
    const appState = useAuthStateContext();
    const [hasRequested, setHasRequested] = React.useState(false);
    const classes = useFormStyles({});

    const { isLoading } = appState.auth.passwordResetRequestState;

    const handleClickReset = (data: { email: string }) => {
      trySubmit((recaptchaResponse) =>
        appState.auth
          .passwordResetRequest({ email: data.email, 'g-recaptcha-response': recaptchaResponse })
          .then((res: unknown) => {
            setHasRequested(!!res);
          }),
      );
    };

    return (
      <AuthContainer showBackLink={true}>
        {hasRequested ? (
          <>
            <h2>{t('page::passwordRequest::titleSuccess')}</h2>
            <p>{t('page::passwordRequest::asterixSuccess')}</p>
          </>
        ) : (
          <Formik
            initialValues={{ email: '' }}
            onSubmit={handleClickReset}
            validationSchema={validation.createSchema({
              email: validation.email,
            })}
          >
            {({ handleSubmit, handleChange, handleBlur, touched, errors }) => (
              <form onSubmit={handleSubmit} noValidate={true} className={classes.formWrapper}>
                <div>
                  <Typography className={classes.bottomMargin} variant="h1">
                    {t('page::passwordRequest::title')}
                  </Typography>
                  <TextField
                    className={classes.bottomMargin}
                    type="email"
                    fullWidth={true}
                    variant="outlined"
                    name="email"
                    label={t('email')}
                    placeholder={t('form::emailPlaceholder')}
                    onBlur={handleBlur}
                    onChange={handleChange}
                    error={touched.email && !!errors.email}
                  />
                </div>
                <Button fullWidth={true} color="primary" type="submit" variant="contained">
                  {isLoading ? (
                    <CircularProgress size={20} />
                  ) : (
                    t('page::passwordRequest::resetButton')
                  )}
                </Button>
              </form>
            )}
          </Formik>
        )}
      </AuthContainer>
    );
  }),
);
