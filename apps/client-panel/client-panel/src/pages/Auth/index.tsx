import * as React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { PageSignUp } from './SignUp';
import { SignIn } from './SignIn';
import { PagePasswordReset } from './PasswordReset';
import { PagePasswordResetRequest } from './PasswordResetRequest';
import NotVerified from './NotVerified';
import Verify from './Verify';
import { PageAccountConfirmation } from './AccountConfirmation';

import cls from './auth.module.scss';

import { observer } from 'mobx-react-lite';
import { appState } from 'state';
import { AppLayout } from 'components/AppLayout';
import { Footer } from 'components/Footer';
import { AccessRequest } from 'pages/Auth/AccessRequest';

interface AuthRoutesProps {
  enableSignUp: boolean;
}

export const AuthRoutes = observer((props: AuthRoutesProps) => {
  const { enableSignUp } = props;
  const { user, urlAfterLogin } = appState.auth;

  if (user && user.verified) {
    return <Redirect to={urlAfterLogin || '/dashboard'} />;
  }

  return (
    <AppLayout footer={<Footer />}>
      <div className={cls.pageWrapper}>
        <Switch>
          {enableSignUp && <Route component={PageSignUp} path="/auth/sign-up" />}
          <Route component={Verify} path="/auth/verify" />
          <Route component={SignIn} path="/auth/sign-in" />
          <Route component={AccessRequest} path="/auth/request-access" />
          <Route component={PagePasswordReset} path="/auth/password-reset/:resetToken" />
          <Route component={PagePasswordResetRequest} path="/auth/password-reset" />
          <Route
            component={PageAccountConfirmation}
            path="/auth/account-confirmation/:confirmationToken"
          />
          <Route component={NotVerified} path="/auth/not-verified" />
          <Redirect to="/auth/sign-in" />
        </Switch>
      </div>
    </AppLayout>
  );
});
