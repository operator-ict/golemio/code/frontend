import React from 'react';
import { hot } from 'react-hot-loader/root';

import { AppPages as Pages } from '../pages';
import { Lang } from '@dataplatform/common-components/components';
import { SnackContainer } from '../components/SnackContainer';

import './App.module.scss';
import { StylesProvider, ThemeProvider } from '@material-ui/styles';
import { theme } from '../theme';
import { CssBaseline } from '@material-ui/core';
import { BrowserRouter } from 'react-router-dom';
import config from '../config';

export const App = () => {
  return (
    <StylesProvider injectFirst>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Lang>
          <SnackContainer />
          <BrowserRouter basename={config.basename}>
            <Pages />
          </BrowserRouter>
        </Lang>
      </ThemeProvider>
    </StylesProvider>
  );
};

export default hot(App);
