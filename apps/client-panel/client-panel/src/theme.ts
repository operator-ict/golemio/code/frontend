import { createMuiTheme } from '@material-ui/core';
import { grey } from '@material-ui/core/colors';
import createBreakpoints from '@material-ui/core/styles/createBreakpoints';

export const COLOR_BACKGROUND_DARK = '#252529';
export const COLOR_BACKGROUND_DARK_HIGHLIGHT = '#545454';
const MAIN_COLOR = '#090942';

const defaultBreakpoints = createBreakpoints({});

export const theme = createMuiTheme({
  palette: {
    primary: { main: MAIN_COLOR },
    secondary: grey,
    text: {
      secondary: '#9EA0A5',
    },
  },
  typography: {
    h1: {
      fontSize: 26,
      fontWeight: 'bold',
      letterSpacing: 1.8,
    },
    h2: {
      fontSize: 24,
      fontWeight: 'bold',
      textTransform: 'uppercase',
    },
    h3: {
      fontSize: 20,
      fontWeight: 'bold',
      textTransform: 'uppercase',
      color: MAIN_COLOR,
    },
    h4: {
      fontSize: 18,
    },
  },
  breakpoints: {
    values: {
      ...defaultBreakpoints.values,
      md: 1100,
    },
  },
});
