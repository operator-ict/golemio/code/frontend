import { useTranslation } from 'react-i18next';
import { useMemo, ComponentType } from 'react';
import { MetadataType } from 'codecs';

import { ReactComponent as AllIcon } from './all.svg';
import { ReactComponent as DashboardIcon } from './dashboard.svg';
import { ReactComponent as ApplicationIcon } from './application.svg';
import { ReactComponent as ExportIcon } from './export.svg';
import { ReactComponent as AnalysisIcon } from './analysis.svg';

export const useMetadataTypeData = () => {
  const { t } = useTranslation();

  const metadataTypeData = useMemo(
    (): Array<{
      type: MetadataType;
      Icon: ComponentType;
      title: string;
    }> => [
      { type: null, Icon: AllIcon, title: t('types::all') },
      {
        type: 'dashboard',
        Icon: DashboardIcon,
        title: t('types::dashboards'),
      },
      {
        type: 'analysis',
        Icon: AnalysisIcon,
        title: t('types::analyses'),
      },
      {
        type: 'export',
        Icon: ExportIcon,
        title: t('types::dataExport'),
      },
      {
        type: 'application',
        Icon: ApplicationIcon,
        title: t('types::application'),
      },
    ],
    [t],
  );

  return { metadataTypeData };
};
