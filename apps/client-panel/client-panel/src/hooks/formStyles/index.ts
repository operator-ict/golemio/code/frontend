import { makeStyles } from '@material-ui/core';

export const useFormStyles = makeStyles((theme) => ({
  bottomMargin: {
    marginBottom: theme.spacing(2),
  },
  formWrapper: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    flex: '1',
  },
}));
