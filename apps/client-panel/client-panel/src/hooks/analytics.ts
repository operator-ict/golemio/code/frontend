import ReactGA from 'react-ga';

const key = process.env.REACT_APP_GA_KEY;

if (key) {
  ReactGA.initialize(key);
}

export const useAnalytics = () => {
  return {
    pageview: key ? (page: string) => ReactGA.pageview(page) : () => {},
  };
};
