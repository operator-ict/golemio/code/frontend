# Client Panel for Golemio

Client panel for the Golemio Data Platform System.

Developed by http://operatorict.cz

## Local installation

### Prerequisites

- node.js
- npm
- yarn

### Installation

1. Install all prerequisites
2. Install all dependencies using command: `yarn install` from the monorepo's root directory.
3. Create `.env` file in `apps/client-panel/src`

## Usage

## NPM Scripts

from the monorepo's root directory.


Local start

For client-panel `npm run-script start-local-client-panel`


Local build

For client-panel `npm run-script build-local-client-panel`


Dev start

For client-panel `npm run-script start-dev-client-panel`


Dev build

For client-panel `npm run-script build-dev-client-panel`


Prod start

For client-panel `npm run-script start-prod-client-panel`


Prod build

For client-panel `npm run-script build-prod-client-panel`

Local tests

For client-panel `e2e-local-client-panel`
For client-panel `e2e-local-watch-client-panel`

Dev tests
For client-panel `e2e-dev-client-panel`

Prod tests
For client-panel `e2e-prod-client-panel`

## Contribution guidelines

Please read [CONTRIBUTING.md](CONTRIBUTING.md).

## Problems?

Contact benak@operatorict.cz or vycpalek@operatorict.cz
