/**
 * This Localization component initialises i18next
 * i18next: https://www.i18next.com/
 * react bindings for i18next: https://react.i18next.com/
 * i18next with plurals: http://docs.translatehouse.org/projects/localization-guide/en/latest/l10n/pluralforms.html?id=l10n/pluralforms
 *
 */

import * as React from 'react';
import i18n from 'i18next';
import backend from 'i18next-xhr-backend';
import detector from 'i18next-browser-languagedetector';
import { initReactI18next } from 'react-i18next';
import moment from 'moment';
import 'moment/locale/cs';
import { observer } from 'mobx-react-lite';
import { appState } from 'state';

const LANG_DEFAULT = process.env.DEFAULT_LNG || 'cs';
const isDev = process.env.NODE_ENV !== 'production';

const init = (callback) => {
  i18n
    .use(detector)
    /**
     * Asyncly loads translations
     * Translations are located at /public/locales
     * All files in /public are static and locales folder files are accessible from
     * http://yourserver.com/locales/{lang}/translation.json
     */
    .use(backend)
    .use(initReactI18next) // passes i18n down to react-i18next
    .init(
      {
        /**
         * default language is comming from redux store
         */
        // lng,
        /**
         * if there is no lng prop or no translation file, i18next will use a fallback language
         */
        fallbackLng: LANG_DEFAULT,
        interpolation: {
          escapeValue: false, // react already safes from xss
        },
        keySeparator: '::',
        nsSeparator: '@@',
        /**
         * Page will be rendered after translations are loaded
         * check https://react.i18next.com/components/withnamespaces
         * to disable this default behaviour and add your custom one
         */
        react: {
          useSuspense: false,
          wait: true,
        },
        debug: isDev,
        backend: {
          loadPath: `${process.env.PUBLIC_URL}/locales/{{lng}}/{{ns}}.json`,
        },
      },
      callback,
    );
};

export const Lang: React.FC = observer(({ children }) => {
  const lng = appState.settings.lng;
  const [langLoaded, setLangLoaded] = React.useState(false);
  const [isInitDone, setInitDone] = React.useState(false);

  React.useEffect(() => {
    init(() => setInitDone(true));
  }, []);

  React.useEffect(() => {
    setLangLoaded(false);
    i18n.changeLanguage(lng);
    moment.locale(lng);
    i18n.loadLanguages(lng, () => setLangLoaded(true));
  }, [lng]);

  return <>{isInitDone && langLoaded && children}</>;
});
