import React, { useCallback, FC } from 'react';
import { Notification, NotificationType } from '@dataplatform/common-state/modules';
import { useTranslation } from 'react-i18next';
import { css } from '@emotion/core';

import { useNotificationsState } from '@dataplatform/common-components/components/NotificationsStateProvider';

import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ErrorIcon from '@material-ui/icons/Error';
// import InfoIcon from '@material-ui/icons/Info';
import CloseIcon from '@material-ui/icons/Close';
// import WarningIcon from '@material-ui/icons/Warning';
import { IconButton, Snackbar, Slide, Box, SnackbarContent } from '@material-ui/core';
import { green, red } from '@material-ui/core/colors';

const variantIcon = {
  success: CheckCircleIcon,
  //   warning: WarningIcon,
  error: ErrorIcon,
  //   info: InfoIcon
};

interface AlertBoxProps {
  data: Notification;
}

export const Snack = ({ data }: AlertBoxProps) => {
  const { t } = useTranslation(undefined, { useSuspense: false });
  const notifications = useNotificationsState();

  const handleNotificationHoverStart = useCallback((event) => {
    notifications.stopNotificationDecay(event.currentTarget.dataset.nid);
  }, []);
  const handleNotificationHoverEnd = useCallback((event) => {
    notifications.startNotificationDecay(event.currentTarget.dataset.nid);
  }, []);
  const handleNotificationDismiss = useCallback((event) => {
    notifications.removeNotification(event.currentTarget.dataset.nid);
  }, []);
  const Icon = variantIcon[data.type];

  return (
    <Snackbar
      open
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'right',
      }}
      TransitionComponent={Transition}
      data-nid={data.id}
      onMouseEnter={handleNotificationHoverStart}
      onMouseLeave={handleNotificationHoverEnd}
    >
      <SnackbarContent
        css={css`
          background-color: ${data.type === NotificationType.error && red[500]};
          background-color: ${data.type === NotificationType.success && green[500]};
        `}
        message={
          <div
            css={css`
              display: flex;
              align-items: center;
            `}
          >
            {!!Icon && <Icon />}
            <Box pl={!!Icon ? 2 : 0}>
              {typeof data.title === 'string' ? <p>{t(data.title)}</p> : data.title}
            </Box>
          </div>
        }
        action={[
          <IconButton
            key="close"
            aria-label="close"
            color="inherit"
            onClick={handleNotificationDismiss}
            data-nid={data.id}
          >
            <CloseIcon />
          </IconButton>,
        ]}
      />
    </Snackbar>
  );
};

const Transition: FC = (props) => <Slide {...props} direction="left" />;
