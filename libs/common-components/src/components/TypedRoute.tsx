import {
  Route as ReactRouterRoute,
  RouteProps as RRRouteProps,
  RouteComponentProps,
} from 'react-router-dom';
import { Route as RouteType, PathPart, ParamsFromPathArray } from 'typesafe-react-router';
import React from 'react';

interface RouteProps<RouteParts extends PathPart<any>[]>
  extends Omit<RRRouteProps, 'to' | 'render' | 'component' | 'children'> {
  route: RouteType<RouteParts>;
  render?: (
    props: RouteComponentProps<Record<ParamsFromPathArray<RouteParts>[number], string>>,
  ) => React.ReactNode;
}
export const TypedRoute = <RouteParts extends Array<any>>({
  route: _r,
  ...props
}: RouteProps<RouteParts>) => {
  return React.createElement(ReactRouterRoute, props);
};
