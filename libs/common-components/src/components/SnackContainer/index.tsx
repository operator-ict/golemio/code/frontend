import React from 'react';
import { useNotificationsState } from '@dataplatform/common-components/components/NotificationsStateProvider';

import Snackbar from '@material-ui/core/Snackbar';

import { Snack } from '../Snack';

import { observer } from 'mobx-react-lite';

export const SnackContainer = observer(() => {
  const notifications = useNotificationsState();
  const snacks = notifications.notifications;

  return (
    <>
      {snacks.map((data) => (
        <Snack key={data.id} data={data} />
      ))}
    </>
  );
});
