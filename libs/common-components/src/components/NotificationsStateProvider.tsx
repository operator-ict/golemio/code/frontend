import React, { useContext, createContext, FC } from 'react';
import { NotificationsState } from '@dataplatform/common-state/modules';

const NotificationsContext = createContext<NotificationsState>(null);

export const NotificationsStateProvider: FC<{
  state: NotificationsState;
}> = ({ state, children }) => {
  return <NotificationsContext.Provider value={state}>{children}</NotificationsContext.Provider>;
};

export const useNotificationsState = () => {
  const state = useContext(NotificationsContext);
  if (!state) {
    throw new Error('Wrap your app in NotificationsStateProvider');
  }
  return state;
};
