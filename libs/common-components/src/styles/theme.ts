import { createMuiTheme } from '@material-ui/core/styles';

// A custom theme for this app
export const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#2196F3',
    },
    secondary: {
      main: '#42f551',
    },
    text: {
      secondary: '#8C8C8C',
    },
    grey: {
      A100: '#EEEEEE',
      A200: '#C8C8C8',
    },
    error: {
      main: '#FF0000',
    },
    background: {
      default: '#fbfbfd',
    },
  },
});
