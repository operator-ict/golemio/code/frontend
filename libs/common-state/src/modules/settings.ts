import { observable, action, autorun } from 'mobx';
import { SyncTrunk } from 'mobx-sync';

type Language = 'cs' | 'en';
const languages = ['en', 'cs'];

const defaultLanguage = (process.env.DEFAULT_LANG || 'cs') as Language;

export const settings = observable<{ lng: Language; setLng: (code: Language) => void }>(
  {
    lng: defaultLanguage,
    setLng(code: string) {
      this.lng = code;
    },
  },
  { setLng: action.bound },
);

const trunk = new SyncTrunk(settings, {
  storageKey: 'state/modules/settings',
  storage: localStorage,
});
trunk.init();

autorun(() => {
  if (!languages.includes(settings.lng)) {
    settings.lng = defaultLanguage;
  }
});
