import * as io from 'io-ts';

export function maybe<RT extends io.Any>(
  type: RT,
  name?: string,
): io.UnionType<[RT, typeof io.null], io.TypeOf<RT> | null> {
  return io.union([type, io.null], name);
}
