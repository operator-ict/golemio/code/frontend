
# Frontend monorepo for Golemio

NRWL/NX monorepo for the Golemio Data Platform System.

Project specific information is in README in every project.

NRWL/NX monorepo enforces single version policy.
Every project in the monorepo should (and must) be used with the same version of packages in package.json.

For more info visit https://github.com/nrwl/nx

Developed by http://operatorict.cz

## Local installation

### Prerequisites

- node.js
- npm
- yarn

### Installation

1. Install all prerequisites
2. Install all dependencies using command: `yarn install` from the monorepo's root directory.
3. Create `.env` files in `apps/client-panel/src` and `apps/client-api-key-management/src`

## Usage

### NPM Scripts

Build scripts are for publish purposes and start scripts are for development.

Start scripts for developing. `yarn start-**`

Build scripts for build only. `yarn build-**`

Local scripts for localhost environment developing. `yarn start-local-**`

Dev scripts for dev server environment developing. `yarn start-dev-**`

Prod scripts for prod server environment developing. `yarn start-prod-**`

#### Testing

Tests for local environment
`e2e-local-client-panel`
`e2e-local-client-api-key-management`

Tests for local environment with change watching:
`e2e-local-watch-client-panel`
`e2e-local-watch-client-api-key-management`

Tests for dev environment
`e2e-dev-client-panel`
`e2e-dev-client-api-key-management`

Tests for prod environment
`e2e-prod-client-panel`
`e2e-prod-client-api-key-management`

## Problems?

Contact benak@operatorict.cz or vycpalek@operatorict.cz
